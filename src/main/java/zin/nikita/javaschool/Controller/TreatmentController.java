package zin.nikita.javaschool.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import zin.nikita.javaschool.GSON.TreatmentGSON;
import zin.nikita.javaschool.Service.TreatmentService;

import java.util.List;

@Controller
public class TreatmentController {

    private TreatmentService treatmentService;

    private static Logger log = LoggerFactory.getLogger(TreatmentController.class);

    @Autowired
    private void setTreatmentService(final TreatmentService treatmentService) {
        this.treatmentService = treatmentService;
    }

    @GetMapping(value = "/getTreatmentsOnDay")
    public @ResponseBody List<TreatmentGSON> getTreatmentsOnDay() {
        log.info("elPanel trying to get treatments");
        return treatmentService.findTreatmentsGsonOnCurrentDay();
    }
}
