package zin.nikita.javaschool.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import zin.nikita.javaschool.DTO.PatientDTO;
import zin.nikita.javaschool.PDF.PatientPDF;
import zin.nikita.javaschool.Service.PatientService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class PDFController {

    private PatientService patientService;

    @Autowired
    private void setPatientService(final PatientService patientService) {
        this.patientService = patientService;
    }

    @PostMapping(value = "/doctor/pdf")
    public ModelAndView pdf(@ModelAttribute("patientDTO") final PatientDTO patientDTO) {
        PatientPDF patient = getPatientPDF(getInsurance(patientDTO));
        return new ModelAndView("pdfDocument", "modelObject", patient);
    }

    private PatientPDF getPatientPDF(final String insurance) {
        return patientService.findPatientForPDFpage(insurance);
    }

    private String getInsurance(final PatientDTO patientDTO) {
        List<String> strings = new ArrayList<>(Arrays.asList(patientDTO.getInsurance().split(" ")));
        for (String string : strings) {
            if (string.matches("^[0-9]+$")) {
                patientDTO.setInsurance(string);
            }
        }
        return patientDTO.getInsurance();
    }
}