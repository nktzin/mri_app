package zin.nikita.javaschool.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import zin.nikita.javaschool.DTO.UserDTO;
import zin.nikita.javaschool.Service.UserService;
import zin.nikita.javaschool.Validation.LoginExistsException;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    private UserService userService;

    @Autowired
    private void setUserService(final UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/registration")
    public ModelAndView getRegistration() {
        return new ModelAndView("admin/registration");
    }

    @PostMapping(value = "/registration")
    public ModelAndView registration(@Valid @ModelAttribute("user") final UserDTO userDTO,
                                     final BindingResult result, final Errors errors) {
        if (!userDTO.getPassword().equals(userDTO.getConfirmPassword())) {
            errors.rejectValue("password", "message.passwordError");
        }
        if (result.hasErrors()) {
            return new ModelAndView("admin/registration", "user", userDTO);
        }
        UserDTO registered = new UserDTO();
        if (!result.hasErrors()) {
            registered = createUser(userDTO);
        }
        if (registered == null) {
            errors.rejectValue("login", "message.regError");
        }
        if (result.hasErrors()) {
            return new ModelAndView("admin/registration", "user", userDTO);
        } else {
            return new ModelAndView("admin/successRegister", "user", userDTO);
        }
    }

    private UserDTO createUser(final UserDTO userDTO) {
        UserDTO registered;
        try {
            registered = userService.createFirstUser(userDTO);
        } catch (LoginExistsException ex) {
            return null;
        }
        return registered;
    }
}
