package zin.nikita.javaschool.Controller;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.DTO.DoctorDTO;
import zin.nikita.javaschool.DTO.NurseDTO;
import zin.nikita.javaschool.DTO.PatientDTO;
import zin.nikita.javaschool.DTO.TreatmentDTO;
import zin.nikita.javaschool.Entity.Enums.RoleEnum;
import zin.nikita.javaschool.Entity.User;
import zin.nikita.javaschool.Service.AdminService;
import zin.nikita.javaschool.Service.DoctorService;
import zin.nikita.javaschool.Service.NurseService;
import zin.nikita.javaschool.Service.TreatmentService;
import zin.nikita.javaschool.Service.UserService;

import java.util.ArrayList;
import java.util.List;

@Controller
public class LoginController {

    private static String loginPage = "login";

    private UserService userService;

    private DoctorService doctorService;

    private NurseService nurseService;

    private TreatmentService treatmentService;

    private AdminService adminService;

    @Autowired
    private void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    private void setDoctorService(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @Autowired
    private void setNurseService(NurseService nurseService) {
        this.nurseService = nurseService;
    }

    @Autowired
    private void setTreatmentService(TreatmentService treatmentService) {
        this.treatmentService = treatmentService;
    }

    @Autowired
    private void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping(value = "/logout")
    public ModelAndView logout() {
        return new ModelAndView(loginPage);
    }

    @GetMapping(value = "/login")
    public ModelAndView getLoginPage() {
        return new ModelAndView(loginPage);
    }

    @GetMapping(value = "/")
    public ModelAndView getPage() {
        return returnModelAndView();
    }

    @GetMapping(value = "/home")
    public ModelAndView getHome() {
        return returnModelAndView();
    }

    @GetMapping(value = "/loginFailure")
    public String loginError() {
        return "loginFailure";
    }

    @GetMapping(value = "/error")
    public String getError() {
        return "error404";
    }

    private ModelAndView returnModelAndView() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_ANONYMOUS"));
        if (auth == null) {
            return new ModelAndView(loginPage);
        }
        if (auth.getAuthorities().contains(authorities.get(0))) {
            return new ModelAndView(loginPage);
        }

        Object obj = auth.getPrincipal();
        String username = "";

        if (obj instanceof UserDetails) {
            username = ((UserDetails) obj).getUsername();
        }
        User user = userService.findUserByLogin(username);

        if (user.getRole().equals(RoleEnum.DOCTOR)) {
            return getDoctorPage(username);
        } else if (user.getRole().equals(RoleEnum.NURSE)) {
            return getNursePage(username);
        } else {
            return getAdminPage(username);
        }
    }

    private ModelAndView getDoctorPage(String username) {
        DoctorDTO doctorDTO = doctorService.findDoctorDTOByLogin(username);
        int numberOfPatients = doctorService.findNumberOfPatients(doctorDTO.getLogin());
        List<PatientDTO> infoAboutPatients = doctorService.findInfoAboutPatients(doctorDTO.getLogin());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("doctor/doctor");
        modelAndView.addObject("doctorDTO", doctorDTO);
        modelAndView.addObject("numberOfPatients", numberOfPatients);
        modelAndView.addObject("infoAboutPatients", infoAboutPatients);
        return modelAndView;
    }

    private ModelAndView getAdminPage(String username) {
        AdminDTO adminDTO = adminService.findAdminDTOByLogin(username);
        return new ModelAndView("admin/admin", "adminDTO", adminDTO);
    }

    private ModelAndView getNursePage(String username) {
        NurseDTO nurseDTO = nurseService.findNurseByLogin(username);
        List<Integer> pages = new ArrayList<>();
        DateTime local = new DateTime();
        int numberOfPages = treatmentService.findAllPlannedTreatments(0, local);
        if (numberOfPages == 0) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("nurse/nurse");
            modelAndView.addObject(nurseDTO);
            return modelAndView;
        } else {
            numberOfPages = getNumberOfPages(numberOfPages);
            for (int i = 1; i < numberOfPages + 1; i++) {
                pages.add(i);
            }
            List<TreatmentDTO> treatmentList = treatmentService.findAllTreatmentsByPage(1, 10);
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("nurse/nurse");
            modelAndView.addObject(nurseDTO);
            modelAndView.addObject("pages", pages);
            modelAndView.addObject("treatmentList", treatmentList);
            return modelAndView;
        }
    }

    private Integer getNumberOfPages(final Integer numberOfPages) {
        if (numberOfPages % 10 == 0) {
            return numberOfPages / 10;
        } else {
            return numberOfPages / 10 + 1;
        }
    }
}
