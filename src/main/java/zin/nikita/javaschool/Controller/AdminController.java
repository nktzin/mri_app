package zin.nikita.javaschool.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.DTO.DoctorDTO;
import zin.nikita.javaschool.DTO.EmployeeDTO;
import zin.nikita.javaschool.DTO.NurseDTO;
import zin.nikita.javaschool.DTO.UserDTO;
import zin.nikita.javaschool.Entity.Enums.RoleEnum;
import zin.nikita.javaschool.Service.AdminService;
import zin.nikita.javaschool.Service.DoctorService;
import zin.nikita.javaschool.Service.NurseService;
import zin.nikita.javaschool.Service.UserService;
import zin.nikita.javaschool.Validation.LoginExistsException;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class AdminController {

    private static final Logger log = LoggerFactory.getLogger(AdminController.class);

    private static String adminPage = "admin/admin";
    private static String adminDtoModelName = "adminDTO";
    private static String registerUserPage = "admin/registerUser";

    @Resource(name = "sessionAdminDTO")
    private AdminDTO adminDTO;
    private AdminService adminService;
    private UserService userService;
    private DoctorService doctorService;
    private NurseService nurseService;

    @Autowired
    private void setAdminService(final AdminService adminService) {
        this.adminService = adminService;
    }

    @Autowired
    private void setUserService(final UserService userService) {
        this.userService = userService;
    }

    @Autowired
    private void setDoctorService(final DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @Autowired
    private void setNurseService(final NurseService nurseService) {
        this.nurseService = nurseService;
    }

    /////////////////////////////////////////////////////Common Mapping/////////////////////////////////////////////////
    @GetMapping(value = "/admin")
    public ModelAndView getAdminPage() {
        adminDTO = getCurrentUser();
        if (adminDTO == null) {
            return new ModelAndView(adminPage);
        }
        log.info("User: {} is get on page", adminDTO.getLogin());
        return new ModelAndView(adminPage, adminDtoModelName, adminDTO);
    }

    @GetMapping(value = "/admin/registration")
    public ModelAndView getRegistration() {
        return new ModelAndView(registerUserPage, adminDtoModelName, adminDTO);
    }

    @GetMapping(value = "/admin/employees")
    public ModelAndView getEmployees() {
        List<DoctorDTO> doctorList = doctorService.findAllDoctorsDTO();
        List<NurseDTO> nurseList = nurseService.findAllNursesDTO();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(adminPage);
        modelAndView.addObject(adminDTO);
        modelAndView.addObject("doctorDTOList", doctorList);
        modelAndView.addObject("nurseDTOList", nurseList);
        return modelAndView;
    }

    @PostMapping(value = "/admin/fireEmployee")
    public ModelAndView fireEmployee(@ModelAttribute("employeeDTO") final EmployeeDTO employeeDTO) {
        String login = getLogin(employeeDTO);
        if (login != null) {
            String role = userService.deleteUserByLogin(employeeDTO.getEmployeeLogin());
            log.info("User {} is deleted", login);
            switch (role) {
                case "DOCTOR":
                    doctorService.deleteDoctorByLogin(employeeDTO.getEmployeeLogin());
                    log.info("doctor deleted with login: {}", login);
                    break;
                case "NURSE":
                    nurseService.deleteNurseByLogin(employeeDTO.getEmployeeLogin());
                    log.info("nurse deleted with login: {}", login);
                    break;
                default:
                    adminService.deleteAdminByLogin(employeeDTO.getEmployeeLogin());
                    log.info("admin deleted with login: {}", login);
                    break;
            }
        }
        return new ModelAndView(adminPage, adminDtoModelName, adminDTO);
    }

    ///////////////////////////////////////////////Registration Mapping/////////////////////////////////////////////////

    @PostMapping(value = "/admin/registration")
    public ModelAndView registration(@Valid @ModelAttribute("user") final UserDTO userDTO,
                                     final BindingResult result, final Errors errors) {
        if (userDTO.getRole().equals(RoleEnum.DOCTOR) && userDTO.getSpecialization().equals("")) {
            errors.rejectValue("specialization", "Specialization field is empty");
            return new ModelAndView(registerUserPage, "user", userDTO);
        }
        if (!userDTO.getPassword().equals(userDTO.getConfirmPassword())) {
            errors.rejectValue("password", "message.passwordError");
        }
        if (result.hasErrors()) {
            return new ModelAndView(registerUserPage, "user", userDTO);
        }
        UserDTO registered = new UserDTO();
        if (!result.hasErrors()) {
            registered = createUser(userDTO);
        }
        if (result.hasErrors() || registered == null) {
            log.info("Invalid data, return registration page");
            return new ModelAndView(registerUserPage, "user", userDTO);
        } else {
            createProfile(registered, adminDTO);
            return new ModelAndView(adminPage, adminDtoModelName, adminDTO);
        }
    }

    ///////////////////////////////////////////Auxiliary methods area///////////////////////////////////////////////////

    private AdminDTO getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth == null) {
            return null;
        }

        Object obj = auth.getPrincipal();
        String username = "";

        if (obj instanceof UserDetails) {
            username = ((UserDetails) obj).getUsername();
        }

        return adminService.findAdminDTOByLogin(username);
    }

    private UserDTO createUser(final UserDTO userDTO) {
        UserDTO registered;
        try {
            registered = userService.createUser(userDTO);
        } catch (LoginExistsException ex) {
            log.warn("User with login: {} is already exist", userDTO.getLogin());
            return null;
        }
        return registered;
    }

    private void createProfile(final UserDTO userDTO, final AdminDTO adminDTO) {
        if (userDTO.getRole().equals(RoleEnum.DOCTOR)) {
            DoctorDTO doctorDTO = new DoctorDTO();
            doctorDTO.setFirstName(userDTO.getFirstName());
            doctorDTO.setLastName(userDTO.getLastName());
            doctorDTO.setSpecialization(userDTO.getSpecialization());
            doctorDTO.setEnabled(true);
            doctorDTO.setLogin(userDTO.getLogin());
            doctorService.createDoctor(doctorDTO, adminDTO);
        }
        if (userDTO.getRole().equals(RoleEnum.NURSE)) {
            NurseDTO nurseDTO = new NurseDTO();
            nurseDTO.setFirstName(userDTO.getFirstName());
            nurseDTO.setLastName(userDTO.getLastName());
            nurseDTO.setEnabled(true);
            nurseDTO.setLogin(userDTO.getLogin());
            nurseService.createNurse(nurseDTO, adminDTO);
        }
    }

    private String getLogin(EmployeeDTO employeeDTO) {
        String login = null;
        List<String> strings = new ArrayList<>(Arrays.asList(employeeDTO.getEmployeeInfo().split(" ")));
        for (String string : strings) {
            if (string.matches("^[a-zA-Z0-9]+$")) {
                employeeDTO.setEmployeeLogin(string);
                login = string;
            }
        }
        return login;
    }
}
