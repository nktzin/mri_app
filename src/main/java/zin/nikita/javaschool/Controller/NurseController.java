package zin.nikita.javaschool.Controller;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import zin.nikita.javaschool.DTO.NurseDTO;
import zin.nikita.javaschool.DTO.PatientDTO;
import zin.nikita.javaschool.DTO.TreatmentDTO;
import zin.nikita.javaschool.Entity.Patient;
import zin.nikita.javaschool.JMS.Sender;
import zin.nikita.javaschool.Service.NurseService;
import zin.nikita.javaschool.Service.PatientService;
import zin.nikita.javaschool.Service.TreatmentService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class NurseController {

    private static String nursePage = "nurse/nurse";
    private static String patientTreatsPage = "nurse/patientTreats";
    private static String pagesModelName = "pages";


    @Resource(name = "sessionNurseDTO")
    NurseDTO nurseDTO;
    @Resource(name = "requestPatientDTO")
    PatientDTO patientDTO;
    private NurseService nurseService;
    private TreatmentService treatmentService;
    private PatientService patientService;
    private Sender sender;

    @Autowired
    private void setNurseService(final NurseService nurseService) {
        this.nurseService = nurseService;
    }

    @Autowired
    private void setTreatmentService(final TreatmentService treatmentService) {
        this.treatmentService = treatmentService;
    }

    @Autowired
    private void setPatientService(final PatientService patientService) {
        this.patientService = patientService;
    }

    @Autowired
    private void setSender(final Sender sender) {
        this.sender = sender;
    }

    @GetMapping(value = "/nurse")
    public ModelAndView getNursePage(@RequestParam(name = "page", required = false) final Integer pageNumber) {
        nurseDTO = getCurrentUser();
        List<Integer> pages = new ArrayList<>();
        DateTime local = new DateTime();
        int numberOfPages = treatmentService.findAllPlannedTreatments(0, local);
        if (numberOfPages == 0) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(nursePage);
            modelAndView.addObject(nurseDTO);
            return modelAndView;
        } else {
            numberOfPages = getNumberOfPages(numberOfPages);
            for (int i = 1; i < numberOfPages + 1; i++) {
                pages.add(i);
            }
            List<TreatmentDTO> treatmentList = treatmentService.findAllTreatmentsByPage(pageNumber, 10);
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(nursePage);
            modelAndView.addObject(nurseDTO);
            modelAndView.addObject(pagesModelName, pages);
            modelAndView.addObject("treatmentList", treatmentList);
            return modelAndView;
        }
    }

    @GetMapping(value = "/nurse/today")
    public ModelAndView getTodaysTreatments(@RequestParam(name = "page", required = false) final Integer pageNumber) {
        DateTime local = new DateTime();
        List<Integer> pages = new ArrayList<>();
        int numberOfPages = treatmentService.findAllPlannedTreatments(1, local);
        if (numberOfPages == 0) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("nurse/todayTreats");
            modelAndView.addObject(nurseDTO);
            return modelAndView;
        } else {
            numberOfPages = getNumberOfPages(numberOfPages);
            for (int i = 1; i < numberOfPages + 1; i++) {
                pages.add(i);
            }
            List<TreatmentDTO> currentTreatments = treatmentService
                    .findAllTreatmentsOnCurrentDay(pageNumber, 10, local);
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("nurse/todayTreats");
            modelAndView.addObject(pagesModelName, pages);
            modelAndView.addObject(nurseDTO);
            modelAndView.addObject("currentTreatments", currentTreatments);
            return modelAndView;
        }
    }

    @GetMapping(value = "/nurse/nexthour")
    public ModelAndView getNextHourTreatments(@RequestParam(name = "page", required = false) final Integer pageNumber) {
        DateTime local = new DateTime();
        List<Integer> pages = new ArrayList<>();
        int numberOfPages = treatmentService.findAllPlannedTreatments(2, local);
        if (numberOfPages == 0) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("nurse/nextHourTreats");
            modelAndView.addObject(nurseDTO);
            return modelAndView;
        } else {
            numberOfPages = getNumberOfPages(numberOfPages);
            for (int i = 1; i < numberOfPages + 1; i++) {
                pages.add(i);
            }
            List<TreatmentDTO> treatmentsOnNextHour = treatmentService
                    .findAllTreatmentsOnNextHour(pageNumber, 10, local);
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("nurse/nextHourTreats");
            modelAndView.addObject(pagesModelName, pages);
            modelAndView.addObject(nurseDTO);
            modelAndView.addObject("treatmentsOnNextHour", treatmentsOnNextHour);
            return modelAndView;
        }
    }

    @GetMapping(value = "/nurse/canceled")
    public ModelAndView getCanceledTreatments(@RequestParam(name = "page", required = false) final Integer pageNumber) {
        List<Integer> pages = new ArrayList<>();
        int numberOfPages = treatmentService.findAllCanceledTreatments();
        if (numberOfPages == 0) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(nursePage);
            modelAndView.addObject(nurseDTO);
            return modelAndView;
        } else {
            numberOfPages = getNumberOfPages(numberOfPages);
            for (int i = 1; i < numberOfPages + 1; i++) {
                pages.add(i);
            }
            List<TreatmentDTO> canceledList = treatmentService
                    .findAllCanceledOrDoneTreatmentsByPage(0, pageNumber, 10);
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(nursePage);
            modelAndView.addObject(nurseDTO);
            modelAndView.addObject(pagesModelName, pages);
            modelAndView.addObject("canceledList", canceledList);
            return modelAndView;
        }
    }

    @GetMapping(value = "/nurse/done")
    public ModelAndView getDoneTreatments(@RequestParam(name = "page", required = false) final Integer pageNumber) {
        List<Integer> pages = new ArrayList<>();
        int numberOfPages = treatmentService.findAllDoneTreatments();
        if (numberOfPages == 0) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(nursePage);
            modelAndView.addObject(nurseDTO);
            return modelAndView;
        } else {
            numberOfPages = getNumberOfPages(numberOfPages);
            for (int i = 1; i < numberOfPages + 1; i++) {
                pages.add(i);
            }
            List<TreatmentDTO> doneList = treatmentService
                    .findAllCanceledOrDoneTreatmentsByPage(1, pageNumber, 10);
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(nursePage);
            modelAndView.addObject(nurseDTO);
            modelAndView.addObject(pagesModelName, pages);
            modelAndView.addObject("doneList", doneList);
            return modelAndView;
        }
    }

    @GetMapping(value = "/nurse/patient")
    public ModelAndView getPatients() {
        List<Patient> patientList = patientService.findAllPatients();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(nursePage);
        modelAndView.addObject(nurseDTO);
        modelAndView.addObject("patientList", patientList);
        return modelAndView;
    }

    @GetMapping(value = "/nurse/patient/treatments")
    public ModelAndView getTreatments(@RequestParam(name = "page", required = false) final Integer pageNumber,
                                      @ModelAttribute("patientDTO") final PatientDTO patient) {
        List<TreatmentDTO> patientTreatments;
        List<Integer> pages = new ArrayList<>();
        int numberOfPages = patientDTO.getTreatments().size();
        if (numberOfPages == 0) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(patientTreatsPage);
            modelAndView.addObject(nurseDTO);
            return modelAndView;
        } else {
            numberOfPages = getNumberOfPages(patientDTO.getTreatments().size());
            for (int i = 1; i < numberOfPages + 1; i++) {
                pages.add(i);
            }
            patientTreatments = getTreatmentsOnPage(pageNumber, 10, patientDTO.getTreatments());
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(patientTreatsPage);
            modelAndView.addObject(patientDTO);
            modelAndView.addObject(pagesModelName, pages);
            modelAndView.addObject(nurseDTO);
            modelAndView.addObject("patientTreatments", patientTreatments);
            return modelAndView;
        }
    }

    @PostMapping(value = "/nurse/patient/treatments")
    public ModelAndView getPatientTreatments(@ModelAttribute("patientDTO") final PatientDTO patient) {

        List<TreatmentDTO> patientTreatments = new ArrayList<>();
        List<Integer> pages = new ArrayList<>();
        int count = 0;
        patientDTO = findPatientTreatments(getInsurance(patient));
        int numberOfPages = getNumberOfPages(patientDTO.getTreatments().size());
        if (numberOfPages != 0) {
            for (TreatmentDTO treatment : patientDTO.getTreatments()) {
                if (count < 10) {
                    patientTreatments.add(treatment);
                    count++;
                } else {
                    break;
                }
            }
            for (int i = 1; i < numberOfPages + 1; i++) {
                pages.add(i);
            }
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(patientTreatsPage);
        modelAndView.addObject(patientDTO);
        modelAndView.addObject(pagesModelName, pages);
        modelAndView.addObject("patientTreatments", patientTreatments);
        modelAndView.addObject(nurseDTO);
        return modelAndView;
    }

    @PostMapping(value = "/nurse/doneOrCanceled")
    public ModelAndView treatmentDone(@ModelAttribute("treatmentDTO") TreatmentDTO treatmentDTO) {
        String[] date = treatmentDTO.getDate().split(" ");
        if (treatmentDTO.getDone() != null) {
            treatmentService.moveToCompleted(treatmentDTO.getIdTreatment());
            if (DateTime.now().toString("dd.MM.yyyy").equals(date[0])) {
                sender.sendMessage("Treatment done!");
            }
            return returnPage(treatmentDTO.getPage());
        } else {
            treatmentService.moveToCanceled(treatmentDTO);
            if (DateTime.now().toString("dd.MM.yyyy").equals(date[0])) {
                sender.sendMessage("Treatment canceled!");
            }
            return returnPage(treatmentDTO.getPage());
        }
    }

    private ModelAndView returnPage(String page) {
        ModelAndView modelAndView = new ModelAndView();
        switch (page) {
            case "nurse":
                modelAndView = getNursePage(1);
                break;
            case "today":
                modelAndView = getTodaysTreatments(1);
                break;
            case "nextHour":
                modelAndView = getNextHourTreatments(1);
                break;
            case "patientTreats":
                patientDTO = patientService.findAllPatientTreatments(patientDTO.getInsurance());
                modelAndView = getTreatments(1, patientDTO);
                break;
        }
        return modelAndView;
    }


    ///////////////////////////////////////////Auxiliary methods area///////////////////////////////////////////////////

    private NurseDTO getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth == null) {
            return null;
        }

        Object obj = auth.getPrincipal();
        String username = "";

        if (obj instanceof UserDetails) {
            username = ((UserDetails) obj).getUsername();
        }

        return nurseService.findNurseByLogin(username);
    }

    private PatientDTO findPatientTreatments(final String insurance) {
        return patientService.findAllPatientTreatments(insurance);
    }

    private Integer getNumberOfPages(final Integer numberOfPages) {
        if (numberOfPages % 10 == 0) {
            return numberOfPages / 10;
        } else {
            return numberOfPages / 10 + 1;
        }
    }

    private List<TreatmentDTO> getTreatmentsOnPage(final Integer pageNumber,
                                                   final int pageSize,
                                                   final List<TreatmentDTO> treatments) {
        int firstResult = 0;
        int maxResult = 9;
        if (pageNumber == null || pageNumber == 1) {
            return getCurrentTreatments(firstResult, maxResult, treatments);
        } else {
            firstResult = pageSize * (pageNumber - 1);
            maxResult = firstResult + 9;
            return getCurrentTreatments(firstResult, maxResult, treatments);
        }
    }

    private List<TreatmentDTO> getCurrentTreatments(final int firstResult,
                                                    final int maxResult,
                                                    final List<TreatmentDTO> treatments) {
        List<TreatmentDTO> treatmentsOnPage = new ArrayList<>();
        for (int i = firstResult; i < maxResult; i++) {
            if (i == treatments.size()) {
                break;
            } else {
                treatmentsOnPage.add(treatments.get(i));
            }
        }
        return treatmentsOnPage;
    }

    private String getInsurance(final PatientDTO patientDTO) {
        List<String> strings = new ArrayList<>(Arrays.asList(patientDTO.getInsurance().split(" ")));
        for (String string : strings) {
            if (string.matches("^[0-9]+$")) {
                patientDTO.setInsurance(string);
            }
        }
        return patientDTO.getInsurance();
    }
}
