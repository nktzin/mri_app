package zin.nikita.javaschool.Controller;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import zin.nikita.javaschool.DTO.DoctorDTO;
import zin.nikita.javaschool.DTO.PatientDTO;
import zin.nikita.javaschool.DTO.PrescriptionDTO;
import zin.nikita.javaschool.DTO.TreatmentDTO;
import zin.nikita.javaschool.Entity.Enums.DayTimePattern;
import zin.nikita.javaschool.Entity.Enums.StatusOfPatient;
import zin.nikita.javaschool.Entity.Enums.WeekTimePattern;
import zin.nikita.javaschool.Entity.Patient;
import zin.nikita.javaschool.Entity.Prescription;
import zin.nikita.javaschool.Entity.ProcAndMed;
import zin.nikita.javaschool.JMS.Sender;
import zin.nikita.javaschool.Service.DoctorService;
import zin.nikita.javaschool.Service.PatientService;
import zin.nikita.javaschool.Service.PrescriptionService;
import zin.nikita.javaschool.Service.ProcAndMedService;
import zin.nikita.javaschool.Service.TreatmentService;
import zin.nikita.javaschool.Validation.InsuranceExistsException;
import zin.nikita.javaschool.Validation.StartDateIsBeforeNowException;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
public class DoctorController {

    private static final Logger log = LoggerFactory.getLogger(DoctorController.class);

    private static String doctorPage = "doctor/doctor";
    private static String prescriptionPage = "doctor/prescription";
    private static String registerPatientPage = "doctor/registerPatient";

    private static String dateModelName = "date";
    private static String pagesModelName = "pages";
    private static String patientDtoModelName = "patientDTO";

    private static String dd_MMM = "dd MMM";
    private static String dd_MM_yyyy = "dd.MM.yyyy";


    @Resource(name = "sessionDoctorDTO")
    DoctorDTO doctorDTO;
    @Resource(name = "sessionPatientDTO")
    PatientDTO patientDTO;

    private DoctorService doctorService;
    private PatientService patientService;
    private PrescriptionService prescriptionService;
    private ProcAndMedService procAndMedService;
    private TreatmentService treatmentService;
    private Sender sender;

    @Autowired
    private void setDoctorService(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @Autowired
    private void setPatientService(PatientService patientService) {
        this.patientService = patientService;
    }

    @Autowired
    private void setPrescriptionService(PrescriptionService prescriptionService) {
        this.prescriptionService = prescriptionService;
    }

    @Autowired
    private void setProcAndMedService(ProcAndMedService procAndMedService) {
        this.procAndMedService = procAndMedService;
    }

    @Autowired
    private void setTreatmentService(TreatmentService treatmentService) {
        this.treatmentService = treatmentService;
    }

    @Autowired
    private void setSender(Sender sender) {
        this.sender = sender;
    }

    /*
     ******************************Common Mapping******************************
     */

    @GetMapping(value = "/doctor")
    public ModelAndView getDoctorPage() {
        doctorDTO = getCurrentUser();
        if (doctorDTO == null) {
            return new ModelAndView(doctorPage);
        }
        log.info("User {} is get on page", doctorDTO.getLogin());
        return getModelForDoctorPage(doctorDTO, "");
    }

    /*
     ******************************End scope Common Mapping******************************
     */

    /*
     ******************************Mapping from doctor page******************************
     */

    @GetMapping(value = "/doctor/registerPatient")
    public ModelAndView getPatientRegistration() {
        return new ModelAndView(registerPatientPage,
                "doctorDTO", doctorDTO);
    }

    @GetMapping(value = "/doctor/treatments")
    public ModelAndView getAllTreatments(@RequestParam(name = "page", required = false) final Integer pageNumber) {
        List<Integer> pages = new ArrayList<>();
        DateTime local = new DateTime();
        int numberOfPages = treatmentService.findAllPlannedTreatments(0, local);
        if (numberOfPages == 0) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(doctorPage);
            modelAndView.addObject(doctorDTO);
            return modelAndView;
        } else {
            if (numberOfPages % 10 == 0) {
                numberOfPages = numberOfPages / 10;
            } else {
                numberOfPages = numberOfPages / 10 + 1;
            }
            for (int i = 1; i < numberOfPages + 1; i++) {
                pages.add(i);
            }
            List<TreatmentDTO> treatmentList = treatmentService.findAllTreatmentsByPage(pageNumber, 10);
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(doctorPage);
            modelAndView.addObject(doctorDTO);
            modelAndView.addObject(pagesModelName, pages);
            modelAndView.addObject("treatmentList", treatmentList);
            return modelAndView;
        }
    }

    @GetMapping(value = "/doctor/findPatient")
    public ModelAndView findPatient() {
        List<Patient> patientList = patientService.findAllPatients();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("doctor/findPatient");
        modelAndView.addObject(doctorDTO);
        modelAndView.addObject(patientList);
        return modelAndView;
    }

    @GetMapping(value = "/doctor/createPDF")
    public ModelAndView createPDF() {
        List<Patient> patientList = patientService.findAllPatients();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("doctor/createPDF");
        modelAndView.addObject(doctorDTO);
        modelAndView.addObject(patientList);
        return modelAndView;
    }

    @GetMapping(value = "/doctor/patientDischarge")
    public ModelAndView discharge() {
        List<Patient> patientList = patientService.findAllPatients();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("doctor/patientDischarge");
        modelAndView.addObject(doctorDTO);
        modelAndView.addObject(patientList);
        return modelAndView;
    }

    @GetMapping(value = "/doctor/patient")
    public ModelAndView getPatientsForEditPrescription() {
        List<Patient> patientList = patientService.findAllPatients();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(doctorPage);
        modelAndView.addObject(doctorDTO);
        modelAndView.addObject(patientList);
        return modelAndView;
    }

    /*
     ******************************End scope doctor page mapping******************************
     */

    /*
     ******************************Mapping from findPatient page******************************
     */

    @PostMapping(value = "/doctor/toPrescription")
    public ModelAndView createPrescription(@ModelAttribute("patientDTO") final PatientDTO patient) {
        patientDTO = getPatientDTO(getInsurance(patient));
        String date = DateTime.now().toString(dd_MMM);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(prescriptionPage);
        modelAndView.addObject(patientDTO);
        modelAndView.addObject(doctorDTO);
        modelAndView.addObject(dateModelName, date);
        return modelAndView;
    }

    /*
     ******************************End scope findPatient page mapping******************************
     */

    /*
     ******************************Mapping from registerPatient page******************************
     */

    @GetMapping(value = "/doctor/prescription")
    public ModelAndView getPrescription(@ModelAttribute("patientDTO") final PatientDTO patientDTO) {
        ModelAndView modelAndView = new ModelAndView();
        String date = DateTime.now().toString(dd_MMM);
        modelAndView.setViewName(prescriptionPage);
        modelAndView.addObject(doctorDTO);
        modelAndView.addObject(this.patientDTO);
        modelAndView.addObject(dateModelName, date);
        return modelAndView;
    }

    @GetMapping(value = "/doctor/prescription/afterDischarge")
    public ModelAndView getPrescriptionAfterDischarge() {
        patientDTO.setStatusOfPatient(StatusOfPatient.TREATMENT);
        patientService.updateStatusOfPatient(patientDTO);
        String date = DateTime.now().toString(dd_MMM);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(prescriptionPage);
        modelAndView.addObject(doctorDTO);
        modelAndView.addObject(patientDTO);
        modelAndView.addObject(dateModelName, date);
        return modelAndView;
    }

    @PostMapping(value = "/doctor/registerPatient")
    public ModelAndView registerPatient(@ModelAttribute("patientDTO") final PatientDTO patientDTO,
                                        final BindingResult result, final Errors errors) {

        PatientDTO registered = null;
        String successMsg;
        if (!result.hasErrors()) {
            registered = createPatient(patientDTO, doctorDTO);
        }
        if (registered == null) {
            errors.rejectValue("insurance", "message.insuranceExist");
            patientDTO.setHasErrors(1);
        }
        if (registered != null && registered.getStatusOfPatient().equals(StatusOfPatient.DISCHARGED)) {
            errors.rejectValue("statusOfPatient", "patient discharged");
            patientDTO.setHasErrors(1);
            this.patientDTO = patientDTO;
        }
        if (result.hasErrors()) {
            log.info("Invalid data in patient form, return registration page");
            return new ModelAndView(registerPatientPage, patientDtoModelName, this.patientDTO);
        } else {
            patientService.addNurses(patientDTO);
            this.patientDTO = patientDTO;
            successMsg = "Пациент успешно зарегестрирован";
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(registerPatientPage);
            modelAndView.addObject(this.patientDTO);
            modelAndView.addObject("successMsg", successMsg);
            modelAndView.addObject(doctorDTO);
            return modelAndView;
        }
    }

    /*
     ******************************End scope registerPatient page mapping**********************
     */

    /*
     ******************************Mapping from prescription page******************************
     */

    @PostMapping(value = "/doctor/prescription/create")
    public ModelAndView createPrescription(@ModelAttribute("prescriptionDTO") final PrescriptionDTO prescriptionDTO,
                                           final BindingResult result, final Errors errors) {
        Set<Long> treatmentsId;
        PrescriptionDTO prescription;
        try {
            doAllConvertingWork(prescriptionDTO);
        } catch (StartDateIsBeforeNowException ex) {
            log.warn("Start date is invalid. Exception: {}", ex);
            errors.rejectValue("startTime", "startDate is before now");
        }
        if (prescriptionDTO.getMedicamentName() == null && prescriptionDTO.getProcedureName() == null) {
            errors.rejectValue("procedureName", "No procedure or medication selected");
        }
        if (result.hasErrors()) {
            String date = DateTime.now().toString(dd_MMM);
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(prescriptionPage);
            modelAndView.addObject(dateModelName, date);
            modelAndView.addObject(doctorDTO);
            modelAndView.addObject(patientDTO);
            return modelAndView;
        }
        DateTime now = DateTime.now();
        prescriptionDTO.setPatientInsurance(patientDTO.getInsurance());
        prescriptionDTO.setIdDoctor(doctorDTO.getIdDoctor());
        prescription = generatePrescription(prescriptionDTO);
        prescriptionService.addDoctor(prescription);
        prescriptionService.addPatient(prescription);
        treatmentsId = createTreatments(getNumberOfTreatments(prescriptionDTO), prescriptionDTO);
        treatmentService.addPatient(treatmentsId, prescription);
        treatmentService.addNurse(treatmentsId);
        treatmentService.addPrescription(treatmentsId, prescription);
        if (prescription.getStartTime().toString(dd_MM_yyyy).equals(now.toString(dd_MM_yyyy))) {
            sender.sendMessage("New treatments was created!");
        }
        return getModelForDoctorPage(doctorDTO, "Назначение успешно создано!");
    }

    @GetMapping(value = "/doctor/findAllProcedures")
    public ModelAndView findAllProcedures() {
        List<ProcAndMed> allProcedures = procAndMedService.findAllProcedures();
        String date = DateTime.now().toString(dd_MMM);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(prescriptionPage);
        modelAndView.addObject("resultAllProcedures", allProcedures);
        modelAndView.addObject(patientDTO);
        modelAndView.addObject(doctorDTO);
        modelAndView.addObject(dateModelName, date);
        return modelAndView;
    }

    @GetMapping(value = "/doctor/findAllMedicaments")
    public ModelAndView findAllMedicaments() {
        List<ProcAndMed> allMedicaments = procAndMedService.findAllMedicaments();
        String date = DateTime.now().toString(dd_MMM);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(prescriptionPage);
        modelAndView.addObject("resultAllMedicaments", allMedicaments);
        modelAndView.addObject(patientDTO);
        modelAndView.addObject(doctorDTO);
        modelAndView.addObject(dateModelName, date);
        return modelAndView;
    }

    /*
     ******************************End scope prescription page mapping******************************
     */

    /*
     ******************************Mapping from patientDischarge page******************************
     */

    @PostMapping(value = "/doctor/dischargePatient")
    public ModelAndView dischargePatient(@ModelAttribute("PatientDTO") final PatientDTO patientDTO) {
        PatientDTO discharged = discharge(patientDTO);
        if (discharged == null) {
            return new ModelAndView("doctor/patientDischarge", patientDtoModelName, patientDTO);
        } else {
            ModelAndView modelAndView = getModelForDoctorPage(doctorDTO, "Пациент успешно выписан!");
            modelAndView.addObject(discharged);
            return modelAndView;
        }
    }

    /*
     ******************************Mapping from edit prescription page********************************
     */

    @GetMapping(value = "/doctor/patient/patientPrescriptions")
    public ModelAndView getPatientPrescriptions(@ModelAttribute("patientDTO") final PatientDTO patient) {
        List<PrescriptionDTO> patientPrescriptions = new ArrayList<>();
        List<Integer> pages = new ArrayList<>();
        int count = 0;
        int numberOfPages = patientDTO.getPrescriptionDTOS().size();
        if (numberOfPages % 10 == 0) {
            numberOfPages = numberOfPages / 10;
        } else {
            numberOfPages = numberOfPages / 10 + 1;
        }
        if (numberOfPages != 0) {
            patientPrescriptions = getPatientPrescriptions(patientDTO, numberOfPages, pages, count);
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("doctor/patientPrescriptions");
        modelAndView.addObject(patientDTO);
        modelAndView.addObject("patientPrescriptions", patientPrescriptions);
        modelAndView.addObject(pagesModelName, pages);
        modelAndView.addObject(doctorDTO);
        return modelAndView;
    }

    @PostMapping(value = "/doctor/patient/patientPrescriptions")
    public ModelAndView getPatientPrescription(@ModelAttribute("patientDTO") final PatientDTO patient) {
        List<PrescriptionDTO> patientPrescriptions = new ArrayList<>();
        List<Integer> pages = new ArrayList<>();
        int count = 0;
        patientDTO = findPatientPrescriptions(getInsurance(patient));
        int numberOfPages = patientDTO.getPrescriptionDTOS().size();
        if (numberOfPages % 10 == 0) {
            numberOfPages = numberOfPages / 10;
        } else {
            numberOfPages = numberOfPages / 10 + 1;
        }
        if (numberOfPages != 0) {
            patientPrescriptions = getPatientPrescriptions(patientDTO, numberOfPages, pages, count);
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("doctor/patientPrescriptions");
        modelAndView.addObject(patientDTO);
        modelAndView.addObject("patientPrescriptions", patientPrescriptions);
        modelAndView.addObject(pagesModelName, pages);
        modelAndView.addObject(doctorDTO);
        return modelAndView;
    }

    @PostMapping(value = "/doctor/editOrCanceled")
    public ModelAndView editOrCanceled(@ModelAttribute("prescriptionDTO") final PrescriptionDTO prescriptionDTO) {
        if (prescriptionDTO.getEdit() != null) {
            PrescriptionDTO prescription = prescriptionService.findPrescriptionDTOById(prescriptionDTO.getIdPrescription());
            String date = DateTime.now().toString(dd_MMM);
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("doctor/editPrescription");
            modelAndView.addObject(patientDTO);
            modelAndView.addObject("prescriptionDTO", prescription);
            modelAndView.addObject(dateModelName, date);
            return modelAndView;
        } else {
            prescriptionService.canceledPrescription(prescriptionDTO.getIdPrescription());
            return getModelForDoctorPage(doctorDTO, "Назначение успешно отменено!");
        }
    }

    @PostMapping(value = "/doctor/editPrescription")
    public ModelAndView editPrescription(@ModelAttribute("prescriptionDTO") final PrescriptionDTO prescriptionDTO,
                                         final BindingResult result, final Errors errors) {
        Set<Long> treatmentsId;
        if (prescriptionDTO.getDose() != null
                && prescriptionDTO.getToDayTimePatternConvert() == null
                && prescriptionDTO.getToWeekTimePatternConvert() == null
                && prescriptionDTO.getDatefilter() == null) {
            editDose(prescriptionDTO.getIdPrescription(), prescriptionDTO.getDose());
        } else {
            try {
                doAllConvertingWork(prescriptionDTO);
            } catch (StartDateIsBeforeNowException ex) {
                errors.rejectValue("startTime", "wrong date");
            }
            if (result.hasErrors()) {
                PrescriptionDTO prescription = prescriptionService.findPrescriptionDTOById(prescriptionDTO.getIdPrescription());
                return getModelAndViewForEditPrescription(prescription);
            }
            DateTime now = DateTime.now();
            changePrescription(prescriptionDTO);
            treatmentsId = createTreatments(getNumberOfTreatments(prescriptionDTO), prescriptionDTO);
            treatmentService.addPatient(treatmentsId, prescriptionDTO);
            treatmentService.addNurse(treatmentsId);
            treatmentService.addPrescription(treatmentsId, prescriptionDTO);
            if (prescriptionDTO.getStartTime().toString(dd_MM_yyyy).equals(now.toString(dd_MM_yyyy))) {
                sender.sendMessage("New treatments was created!");
            }
        }
        return getModelForDoctorPage(doctorDTO, "Назначение успешно изменено!");
    }

    ///////////////////////////////////////////Auxiliary methods area///////////////////////////////////////////////////

    private DoctorDTO getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth == null) {
            return null;
        }

        Object obj = auth.getPrincipal();
        String username = "";

        if (obj instanceof UserDetails) {
            username = ((UserDetails) obj).getUsername();
        }

        return doctorService.findDoctorDTOByLogin(username);
    }

    private PatientDTO createPatient(final PatientDTO patientDTO, final DoctorDTO doctorDTO) {
        PatientDTO registered;
        try {
            registered = patientService.createPatient(patientDTO, doctorDTO);
        } catch (InsuranceExistsException ex) {
            log.warn("Trying to create patient with existing insurance");
            Patient patient = patientService.findPatientByInsurance(patientDTO.getInsurance());
            if (patient.getStatusOfPatient().equals(StatusOfPatient.TREATMENT)) {
                return null;
            } else {
                patientDTO.setStatusOfPatient(patient.getStatusOfPatient());
                return patientDTO;
            }
        }
        return registered;
    }

    private PatientDTO getPatientDTO(final String insurance) {
        Patient patient = patientService.findPatientByInsurance(insurance);
        patientDTO.setFirstName(patient.getFirstName());
        patientDTO.setLastName(patient.getLastName());
        patientDTO.setInsurance(patient.getInsurance());
        patientDTO.setDiagnosis(patient.getDiagnosis());
        patientDTO.setDoctor(patient.getDoctor());
        patientDTO.setPrescriptionDTOS(patientService.findPatientPrescriptions(insurance));
        return patientDTO;
    }

    private void doAllConvertingWork(final PrescriptionDTO prescriptionDTO)
            throws StartDateIsBeforeNowException {
        convertStringToDayTimePattern(prescriptionDTO);
        convertStringToWeekTimePattern(prescriptionDTO);
        convertStringDateFilterToActualDate(prescriptionDTO);
    }

    private void convertStringDateFilterToActualDate(final PrescriptionDTO prescriptionDTO)
            throws StartDateIsBeforeNowException {
        List<String> dateFilterList = new ArrayList<>(Arrays.asList(prescriptionDTO.getDatefilter().split(" ")));
        Prescription prescription = null;
        for (String list : dateFilterList) {
            if (list.equals("-")) {
                dateFilterList.remove(list);
            }
        }
        if (prescriptionDTO.getIdPrescription() != 0) {
            prescription = prescriptionService.findPrescriptionById(prescriptionDTO.getIdPrescription());
        }

        setDateTimeToPrescription(prescription, dateFilterList, prescriptionDTO);
    }

    private void setDateTimeToPrescription(final Prescription prescription,
                                           final List<String> dateFilterList,
                                           final PrescriptionDTO prescriptionDTO) throws StartDateIsBeforeNowException {
        DateTime now = DateTime.now();
        DateTimeFormatter dtf = DateTimeFormat.forPattern(dd_MM_yyyy);
        if (prescription == null
                && (dtf.parseDateTime(dateFilterList.get(0)).isAfter(now)
                || dateFilterList.get(0).equals(now.toString(dd_MM_yyyy)))) {
            setTime(prescriptionDTO, dtf, dateFilterList);
        } else if (prescription != null) {
            if (prescription.getStartTime().isBefore(now) && dtf.parseDateTime(dateFilterList.get(0)).equals(prescription.getStartTime())) {
                setTime(prescriptionDTO, dtf, dateFilterList);
            } else if ((dtf.parseDateTime(dateFilterList.get(0)).isAfter(now)
                    || dtf.parseDateTime(dateFilterList.get(0)).equals(now))
                    && (prescription.getStartTime().isAfter(now) || prescription.getStartTime().equals(now))) {
                setTime(prescriptionDTO, dtf, dateFilterList);
            } else {
                log.info("Client entered incorrect start date of prescription");
                throw new StartDateIsBeforeNowException(dateFilterList.get(0) + " wrong start date");
            }
        } else {
            log.info("Client entered incorrect start date of prescription");
            throw new StartDateIsBeforeNowException(dateFilterList.get(0) + " wrong start date");
        }
    }

    private void setTime(final PrescriptionDTO prescriptionDTO,
                         final DateTimeFormatter dtf,
                         final List<String> dateFilterList) {
        prescriptionDTO.setStartTime(dtf.parseDateTime(dateFilterList.get(0)));
        prescriptionDTO.setEndTime(dtf.parseDateTime(dateFilterList.get(1)).withTime(21, 59, 59, 999));
    }

    private void convertStringToDayTimePattern(final PrescriptionDTO prescriptionDTO) {
        switch (prescriptionDTO.getToDayTimePatternConvert()) {
            case "1":
                prescriptionDTO.setDayTimePattern(DayTimePattern.ONCE_A_DAY);
                break;
            case "2":
                prescriptionDTO.setDayTimePattern(DayTimePattern.TWICE_A_DAY);
                break;
            case "3":
                prescriptionDTO.setDayTimePattern(DayTimePattern.THREE_TIMES_A_DAY);
                break;
            case "4":
                prescriptionDTO.setDayTimePattern(DayTimePattern.FOUR_TIMES_A_DAY);
                break;
            case "5":
                prescriptionDTO.setDayTimePattern(DayTimePattern.FIVE_TIMES_A_DAY);
                break;
        }
    }

    private void convertStringToWeekTimePattern(final PrescriptionDTO prescriptionDTO) {
        switch (prescriptionDTO.getToWeekTimePatternConvert()) {
            case "1":
                prescriptionDTO.setWeekTimePattern(WeekTimePattern.ONCE_A_WEEK);
                break;
            case "2":
                prescriptionDTO.setWeekTimePattern(WeekTimePattern.TWICE_A_WEEK);
                break;
            case "3":
                prescriptionDTO.setWeekTimePattern(WeekTimePattern.THREE_TIMES_A_WEEK);
                break;
            case "4":
                prescriptionDTO.setWeekTimePattern(WeekTimePattern.FOUR_TIMES_A_WEEK);
                break;
            case "5":
                prescriptionDTO.setWeekTimePattern(WeekTimePattern.FIVE_TIMES_A_WEEK);
                break;
            case "6":
                prescriptionDTO.setWeekTimePattern(WeekTimePattern.SIX_TIMES_A_WEEK);
                break;
            case "7":
                prescriptionDTO.setWeekTimePattern(WeekTimePattern.SEVEN_TIMES_A_WEEK);
                break;
        }
    }

    private PrescriptionDTO generatePrescription(final PrescriptionDTO prescriptionDTO) {
        return prescriptionService.createPrescription(prescriptionDTO);
    }

    private int getNumberOfTreatments(final PrescriptionDTO prescriptionDTO) {
        return Days.daysBetween(prescriptionDTO.getStartTime().toLocalDate(),
                prescriptionDTO.getEndTime().toLocalDate()).getDays() + 1;
    }

    private Set<Long> createTreatments(final int numberOfTreatments, final PrescriptionDTO prescriptionDTO) {
        Set<Long> treatmentsId;
        switch (prescriptionDTO.getDayTimePattern()) {
            case ONCE_A_DAY:
                treatmentsId = treatmentService
                        .createTreatment(numberOfTreatments, prescriptionDTO);
                break;
            case TWICE_A_DAY:
                treatmentsId = treatmentService
                        .createTreatmentWithTwoDayPattern(numberOfTreatments, prescriptionDTO);
                break;
            case THREE_TIMES_A_DAY:
                treatmentsId = treatmentService
                        .createTreatmentWithThreeDayPattern(numberOfTreatments, prescriptionDTO);
                break;
            case FOUR_TIMES_A_DAY:
                treatmentsId = treatmentService
                        .createTreatmentWithFourDayPattern(numberOfTreatments, prescriptionDTO);
                break;
            case FIVE_TIMES_A_DAY:
                treatmentsId = treatmentService
                        .createTreatmentWithFiveDayPattern(numberOfTreatments, prescriptionDTO);
                break;
            default:
                treatmentsId = treatmentService
                        .createTreatment(numberOfTreatments, prescriptionDTO);
                break;
        }
        return treatmentsId;
    }

    private PatientDTO discharge(PatientDTO patientDTO) {
        getInsurance(patientDTO);
        PatientDTO discharged = patientService.dischargePatient(patientDTO);
        if (discharged.isDischarged()) {
            return patientDTO;
        } else {
            return null;
        }
    }

    private PatientDTO findPatientPrescriptions(final String insurance) {
        return patientService.findAllPatientPrescriptions(insurance);
    }

    private void editDose(final long idPrescriptions, final String dose) {
        prescriptionService.editDose(idPrescriptions, dose);
    }

    private void changePrescription(final PrescriptionDTO prescriptionDTO) {
        DateTime dateTime = new DateTime().withTime(23, 59, 59, 999);
        prescriptionService.changePrescription(prescriptionDTO, dateTime);
    }

    private ModelAndView getModelAndViewForEditPrescription(final PrescriptionDTO prescriptionDTO) {
        ModelAndView modelAndView = new ModelAndView();
        String date = DateTime.now().toString(dd_MMM);
        modelAndView.setViewName("doctor/editPrescription");
        modelAndView.addObject(patientDTO);
        modelAndView.addObject(prescriptionDTO);
        modelAndView.addObject(dateModelName, date);
        return modelAndView;
    }

    private String getInsurance(final PatientDTO patientDTO) {
        List<String> strings = new ArrayList<>(Arrays.asList(patientDTO.getInsurance().split(" ")));
        for (String string : strings) {
            if (string.matches("^[0-9]+$")) {
                patientDTO.setInsurance(string);
            }
        }
        return patientDTO.getInsurance();
    }

    private List<PrescriptionDTO> getPatientPrescriptions(final PatientDTO patientDTO,
                                                          final int numberOfPages,
                                                          final List<Integer> pages,
                                                          int count) {
        List<PrescriptionDTO> patientPrescriptions = new ArrayList<>();
        for (PrescriptionDTO prescription : patientDTO.getPrescriptionDTOS()) {
            if (count < 10) {
                patientPrescriptions.add(prescription);
                count++;
            } else {
                break;
            }
        }
        for (int i = 1; i < numberOfPages + 1; i++) {
            pages.add(i);
        }
        return patientPrescriptions;
    }

    private ModelAndView getModelForDoctorPage(final DoctorDTO doctorDTO, final String successMsg) {
        int numberOfPatients = doctorService.findNumberOfPatients(doctorDTO.getLogin());
        List<PatientDTO> infoAboutPatients = doctorService.findInfoAboutPatients(doctorDTO.getLogin());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(doctorPage);
        modelAndView.addObject("doctorDTO", doctorDTO);
        modelAndView.addObject("numberOfPatients", numberOfPatients);
        modelAndView.addObject("infoAboutPatients", infoAboutPatients);
        modelAndView.addObject("successMsg", successMsg);
        return modelAndView;
    }
}
