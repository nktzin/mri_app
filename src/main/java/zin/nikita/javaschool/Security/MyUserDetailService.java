package zin.nikita.javaschool.Security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import zin.nikita.javaschool.Entity.User;
import zin.nikita.javaschool.Service.UserService;

import java.util.ArrayList;
import java.util.List;

@Service
public class MyUserDetailService implements UserDetailsService {

    private UserService userService;

    private static final Logger log = LoggerFactory.getLogger(MyUserDetailService.class);

    @Autowired
    private void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(final String login) throws UsernameNotFoundException {
        User user = userService.findUserByLogin(login);
        if (user == null) {
            log.error("No user found with login: {}", login);
            throw new UsernameNotFoundException("No user found with login: " + login);
        }
        return new org.springframework.security.core.userdetails.User(user.getLogin(),
                user.getPassword(), getAuthorities(user.getRole().toString()));
    }

    private static List<GrantedAuthority> getAuthorities(final String role) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
        return authorities;
    }
}
