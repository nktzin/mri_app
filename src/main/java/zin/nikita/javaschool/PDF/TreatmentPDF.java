package zin.nikita.javaschool.PDF;

import zin.nikita.javaschool.Entity.Enums.StatusOfTreatment;

public class TreatmentPDF {

    private long id;

    private String date;

    private StatusOfTreatment statusOfTreatment;

    private String cause;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public StatusOfTreatment getStatusOfTreatment() {
        return statusOfTreatment;
    }

    public void setStatusOfTreatment(StatusOfTreatment statusOfTreatment) {
        this.statusOfTreatment = statusOfTreatment;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }
}
