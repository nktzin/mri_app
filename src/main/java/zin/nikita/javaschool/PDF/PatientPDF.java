package zin.nikita.javaschool.PDF;

import zin.nikita.javaschool.Entity.Enums.StatusOfPatient;

import java.util.HashMap;
import java.util.List;

public class PatientPDF {

    private String firstName;

    private String lastName;

    private String insurance;

    private String doctorFirstName;

    private String doctorLastName;

    private String doctorSpecialization;

    private long prescriptionId;

    private List<TreatmentPDF> treatments;

    private HashMap<Long, List<TreatmentPDF>> treatmentsMap;

    private HashMap<Long, List<PrescriptionPDF>> prescriptionsMap;

    private StatusOfPatient statusOfPatient;

    private boolean isProcedure;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public String getDoctorSpecialization() {
        return doctorSpecialization;
    }

    public void setDoctorSpecialization(String doctorSpecialization) {
        this.doctorSpecialization = doctorSpecialization;
    }

    public long getPrescriptionId() {
        return prescriptionId;
    }

    public void setPrescriptionId(long prescriptionId) {
        this.prescriptionId = prescriptionId;
    }

    public List<TreatmentPDF> getTreatments() {
        return treatments;
    }

    public void setTreatments(List<TreatmentPDF> treatments) {
        this.treatments = treatments;
    }

    public HashMap<Long, List<TreatmentPDF>> getTreatmentsMap() {
        return treatmentsMap;
    }

    public void setTreatmentsMap(HashMap<Long, List<TreatmentPDF>> treatmentsMap) {
        this.treatmentsMap = treatmentsMap;
    }

    public HashMap<Long, List<PrescriptionPDF>> getPrescriptionsMap() {
        return prescriptionsMap;
    }

    public void setPrescriptionsMap(HashMap<Long, List<PrescriptionPDF>> prescriptionMap) {
        this.prescriptionsMap = prescriptionMap;
    }

    public StatusOfPatient getStatusOfPatient() {
        return statusOfPatient;
    }

    public void setStatusOfPatient(StatusOfPatient statusOfPatient) {
        this.statusOfPatient = statusOfPatient;
    }

    public boolean isProcedure() {
        return isProcedure;
    }

    public void setProcedure(boolean procedure) {
        isProcedure = procedure;
    }
}
