package zin.nikita.javaschool.PDF;

import zin.nikita.javaschool.Entity.Enums.DayTimePattern;
import zin.nikita.javaschool.Entity.Enums.WeekTimePattern;

public class PrescriptionPDF {

    private String prescriptionName;

    private DayTimePattern dayTimePattern;

    private WeekTimePattern weekTimePattern;

    private String startDate;

    private String endDate;

    public String getPrescriptionName() {
        return prescriptionName;
    }

    public void setPrescriptionName(String prescriptionName) {
        this.prescriptionName = prescriptionName;
    }

    public DayTimePattern getDayTimePattern() {
        return dayTimePattern;
    }

    public void setDayTimePattern(DayTimePattern dayTimePattern) {
        this.dayTimePattern = dayTimePattern;
    }

    public WeekTimePattern getWeekTimePattern() {
        return weekTimePattern;
    }

    public void setWeekTimePattern(WeekTimePattern weekTimePattern) {
        this.weekTimePattern = weekTimePattern;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
