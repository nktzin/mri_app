package zin.nikita.javaschool.PDF;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.Map;

public class PDFDocument extends AbstractPdfView {

    @Override
    protected void buildPdfDocument(final Map<String, Object> model,
                                    final Document document,
                                    final PdfWriter pdfWriter,
                                    final HttpServletRequest httpServletRequest,
                                    final HttpServletResponse httpServletResponse) throws DocumentException, IOException {

        BaseFont bf = BaseFont.createFont("/fonts/HelveticaLight.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font title = new Font(bf, 26, Font.BOLD);
        Font head3 = new Font(bf, 16);
        Font mainText = new Font(bf, 14);

        PatientPDF patientPDF = (PatientPDF) model.get("modelObject");
        document.add(new Paragraph("Карта пациента", title));
        setInformationAboutPatient(document, patientPDF, mainText);
        setInformationAboutDoctor(document, patientPDF, mainText);

        document.add(new Paragraph("Назначенное лечение:", head3));
        document.add(new Phrase("Комплекс процедур: ", mainText));
        setInformationAboutPrescription(document, patientPDF, mainText, head3);

    }

    private void setInformationAboutPatient(final Document document,
                                            final PatientPDF patientPDF,
                                            final Font font) throws DocumentException {
        Paragraph patientInfo = new Paragraph();
        patientInfo.setSpacingBefore(20);
        patientInfo.setSpacingAfter(5);
        patientInfo.setAlignment(Element.ALIGN_LEFT);
        Phrase patient = new Phrase(patientPDF.getLastName() + " " + patientPDF.getFirstName()
                + ", № страхового полиса: " + patientPDF.getInsurance(), font);
        patientInfo.add(patient);
        document.add(patientInfo);
    }

    private void setInformationAboutDoctor(final Document document,
                                           final PatientPDF patientPDF,
                                           final Font font) throws DocumentException {
        Paragraph doctorInfo = new Paragraph();
        doctorInfo.setSpacingAfter(5);
        doctorInfo.setAlignment(Element.ALIGN_LEFT);
        Phrase doctor = new Phrase("Лечащий врач: " + patientPDF.getDoctorLastName() + " "
                + patientPDF.getDoctorFirstName() + ", специализация: "
                + patientPDF.getDoctorSpecialization(), font);
        doctorInfo.add(doctor);
        document.add(doctorInfo);
    }

    private void setInformationAboutPrescription(final Document document,
                                                 final PatientPDF patientPDF,
                                                 final Font mainText, final Font head) throws DocumentException {
        Paragraph treatmentsList = new Paragraph("Расписание процедур", head);
        treatmentsList.setSpacingAfter(10);
        treatmentsList.setAlignment(Element.ALIGN_LEFT);
        for (Long prescriptionId : patientPDF.getPrescriptionsMap().keySet()) {
            for (PrescriptionPDF prescriptionPDF : patientPDF.getPrescriptionsMap().get(prescriptionId)) {
                document.add(new Phrase("\"" + prescriptionPDF.getPrescriptionName() + "\"" + ", "
                        + convertDayTimePatternToString(prescriptionPDF)
                        + ", "
                        + convertWeekTimePatternToString(prescriptionPDF)
                        + ". \nПериод лечения: "
                        + prescriptionPDF.getStartDate()
                        + " - "
                        + prescriptionPDF.getEndDate(), mainText));
                document.add(treatmentsList);
                PdfPTable table = createTable(mainText);
                int i = 1;
                patientPDF.getTreatmentsMap().get(prescriptionId).sort(Comparator.comparing(TreatmentPDF::getDate));
                for (TreatmentPDF treatmentPDF : patientPDF.getTreatmentsMap().get(prescriptionId)) {
                    createTreatmentRow(treatmentPDF, i++, mainText, table);
                }
                table.setSpacingAfter(10);
                document.add(table);
            }
        }
    }

    private void createTreatmentRow(final TreatmentPDF treatmentPDF,
                                    final int i,
                                    final Font font,
                                    final PdfPTable table) {
        PdfPCell number = new PdfPCell(new Phrase(Integer.toString(i), font));
        PdfPCell date = new PdfPCell(new Phrase(treatmentPDF.getDate(), font));
        PdfPCell status = new PdfPCell(new Phrase(convertStatusToString(treatmentPDF), font));
        setOrientation(number);
        setOrientation(date);
        setOrientation(status);
        table.addCell(number);
        table.addCell(date);
        table.addCell(status);
    }

    private String convertStatusToString(final TreatmentPDF treatmentPDF) {
        String status = null;
        switch (treatmentPDF.getStatusOfTreatment()) {
            case PLANNED:
                status = "Запланировано";
                break;
            case CANCELED:
                status = "Отменено";
                break;
            case DONE:
                status = "Выполнено";
                break;
        }
        return status;
    }

    private PdfPTable createTable(final Font font) {
        PdfPTable table = new PdfPTable(new float[]{1, 3, 3});
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell header1 = new PdfPCell(new Phrase("№ п/п", font));
        PdfPCell header2 = new PdfPCell(new Phrase("Дата приёма", font));
        PdfPCell header3 = new PdfPCell(new Phrase("Статус", font));
        setOrientation(header1);
        setOrientation(header2);
        setOrientation(header3);
        table.addCell(header1);
        table.addCell(header2);
        table.addCell(header3);
        return table;
    }

    private void setOrientation(final PdfPCell pCell) {
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        pCell.setPaddingTop(5);
        pCell.setPaddingBottom(5);
    }

    private String convertDayTimePatternToString(final PrescriptionPDF prescriptionPDF) {
        String dayTimePattern;
        switch (prescriptionPDF.getDayTimePattern()) {
            case ONCE_A_DAY:
                dayTimePattern = "1 раз в день";
                break;
            case TWICE_A_DAY:
                dayTimePattern = "2 раза в день";
                break;
            case THREE_TIMES_A_DAY:
                dayTimePattern = "3 раза в день";
                break;
            case FOUR_TIMES_A_DAY:
                dayTimePattern = "4 раза в день";
                break;
            case FIVE_TIMES_A_DAY:
                dayTimePattern = "5 раз в день";
                break;
            default:
                dayTimePattern = "1 раз в день";
                break;
        }
        return dayTimePattern;
    }

    private String convertWeekTimePatternToString(final PrescriptionPDF prescriptionPDF) {
        String weekTimePattern;
        switch (prescriptionPDF.getWeekTimePattern()) {
            case ONCE_A_WEEK:
                weekTimePattern = "1 раз в неделю";
                break;
            case TWICE_A_WEEK:
                weekTimePattern = "2 раза в неделю";
                break;
            case THREE_TIMES_A_WEEK:
                weekTimePattern = "3 раза в неделю";
                break;
            case FOUR_TIMES_A_WEEK:
                weekTimePattern = "4 раза в неделю";
                break;
            case FIVE_TIMES_A_WEEK:
                weekTimePattern = "5 раз в неделю";
                break;
            case SIX_TIMES_A_WEEK:
                weekTimePattern = "6 раз в неделю";
                break;
            case SEVEN_TIMES_A_WEEK:
                weekTimePattern = "Каждый день";
                break;
            default:
                weekTimePattern = "1 раз в неделю";
                break;
        }
        return weekTimePattern;
    }
}
