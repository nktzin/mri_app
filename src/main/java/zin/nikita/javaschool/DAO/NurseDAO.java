package zin.nikita.javaschool.DAO;

import zin.nikita.javaschool.Entity.Nurse;

import java.util.List;

public interface NurseDAO {

    void createNurse(Nurse nurse);

    Nurse findNurseById(long id);

    Nurse findNurseByLogin(String login);

    List<Nurse> findAllNurses();

    void updateNurse(Nurse nurse);

    void deleteNurseByLogin(String login);
}
