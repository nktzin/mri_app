package zin.nikita.javaschool.DAO;

import zin.nikita.javaschool.Entity.Admin;

public interface AdminDAO {

    void createAdmin(Admin admin);

    Admin findAdminByLogin(String login);

    void updateAdmin(Admin admin);

    void deleteAdminByLogin(String login);
}
