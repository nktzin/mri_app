package zin.nikita.javaschool.DAO;

import org.joda.time.DateTime;
import zin.nikita.javaschool.Entity.Treatment;

import java.util.List;
import java.util.Set;

public interface TreatmentDAO {

    long createTreatment(Treatment treatment);

    Treatment findTreatmentById(long id);

    List<Treatment> findAllTreatmentsOnCurrentDay(int firstResult, int maxResult, DateTime localTime);

    List<Treatment> findAllTreatmentsOnCurrentDay();

    List<Treatment> findAllTreatmentsOnNextHour(int firstResult, int maxResult, DateTime localTime);

    Integer findAllPlannedTreatments(int flag, DateTime localTime);

    Integer findAllCanceledOrDoneTreatments(int flag);

    List<Treatment> findAllTreatmentsByPage(int firstResult, int maxResult);

    List<Treatment> findAllCanceledOrDoneTreatmentsByPage(int flag, int firstResult, int maxResult);

    List<Treatment> findTreatmentsBySetId(Set<Long> treatmentsId);

    void updateTreatment(Treatment treatment);
}
