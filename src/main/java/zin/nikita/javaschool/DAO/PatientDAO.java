package zin.nikita.javaschool.DAO;

import zin.nikita.javaschool.Entity.Patient;

import java.util.List;

public interface PatientDAO {

    boolean createPatient(Patient patient);

    Patient findPatientByInsurance(String insurance);

    List<Patient> findAllPatients();

    void updatePatient(Patient patient);
}
