package zin.nikita.javaschool.DAO;

import zin.nikita.javaschool.Entity.Prescription;

public interface PrescriptionDAO {

    long createPrescription(Prescription prescription);

    Prescription findPrescriptionById(long id);

    void updatePrescription(Prescription prescription);
}
