package zin.nikita.javaschool.DAO.Impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zin.nikita.javaschool.DAO.NurseDAO;
import zin.nikita.javaschool.Entity.Nurse;
import zin.nikita.javaschool.Entity.Nurse_;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public final class NurseDAOImpl implements NurseDAO {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void createNurse(final Nurse nurse) {
        currentSession().save(nurse);
    }

    @Override
    public Nurse findNurseById(final long id) {
        if (currentSession().get(Nurse.class, id) == null) {
            return null;
        } else {
            return currentSession().get(Nurse.class, id);
        }
    }

    @Override
    public Nurse findNurseByLogin(final String login) {
        CriteriaBuilder cb = currentSession().getCriteriaBuilder();
        CriteriaQuery<Nurse> cq = cb.createQuery(Nurse.class);
        Root<Nurse> nurseRoot = cq.from(Nurse.class);

        Predicate predicateForEnable = cb.equal(nurseRoot.get(Nurse_.enabled), true);
        Predicate predicateForLogin = cb.equal(nurseRoot.get(Nurse_.login), login);
        Predicate predicateForEnableLogin = cb.and(predicateForEnable, predicateForLogin);

        CriteriaQuery<Nurse> query = cq.select(nurseRoot).where(predicateForEnableLogin);

        TypedQuery<Nurse> fullQuery = currentSession().createQuery(query);
        try {
            return fullQuery.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public List<Nurse> findAllNurses() {
        CriteriaBuilder cb = currentSession().getCriteriaBuilder();
        CriteriaQuery<Nurse> cq = cb.createQuery(Nurse.class);
        Root<Nurse> nurseRoot = cq.from(Nurse.class);

        CriteriaQuery<Nurse> query = cq.select(nurseRoot)
                .where(cb.equal(nurseRoot.get(Nurse_.enabled), true));

        TypedQuery<Nurse> fullQuery = currentSession().createQuery(query);

        return fullQuery.getResultList();
    }

    @Override
    public void updateNurse(final Nurse nurse) {
        currentSession().saveOrUpdate(nurse);
    }

    @Override
    public void deleteNurseByLogin(final String login) {
        CriteriaBuilder cb = currentSession().getCriteriaBuilder();
        CriteriaQuery<Nurse> cq = cb.createQuery(Nurse.class);
        Root<Nurse> nurseRoot = cq.from(Nurse.class);
        CriteriaQuery<Nurse> query = cq.select(nurseRoot).where(cb.equal(nurseRoot.get(Nurse_.login), login));

        TypedQuery<Nurse> fullQuery = currentSession().createQuery(query);
        Nurse nurse = fullQuery.getSingleResult();
        nurse.setEnabled(false);
        currentSession().update(nurse);
    }
}
