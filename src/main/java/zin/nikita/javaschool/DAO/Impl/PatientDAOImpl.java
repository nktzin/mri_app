package zin.nikita.javaschool.DAO.Impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zin.nikita.javaschool.DAO.PatientDAO;
import zin.nikita.javaschool.Entity.Enums.StatusOfPatient;
import zin.nikita.javaschool.Entity.Patient;
import zin.nikita.javaschool.Entity.Patient_;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class PatientDAOImpl implements PatientDAO {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public boolean createPatient(final Patient patient) {
        CriteriaBuilder cb = currentSession().getCriteriaBuilder();
        CriteriaQuery<Patient> cq = cb.createQuery(Patient.class);
        Root<Patient> patientRoot = cq.from(Patient.class);
        cq.select(patientRoot).where(cb.equal(patientRoot.get(Patient_.insurance), patient.getInsurance()));
        TypedQuery<Patient> fullQuery = currentSession().createQuery(cq);
        if (fullQuery.getResultList().size() != 0) {
            return false;
        } else {
            currentSession().save(patient);
            return true;
        }
    }

    @Override
    public Patient findPatientByInsurance(final String insurance) {
        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<Patient> criteriaQuery = criteriaBuilder.createQuery(Patient.class);
        Root<Patient> patientRoot = criteriaQuery.from(Patient.class);

        CriteriaQuery<Patient> query =
                criteriaQuery.select(patientRoot)
                        .where(criteriaBuilder.equal(patientRoot.get(Patient_.insurance), insurance));

        try {
            TypedQuery<Patient> fullQuery = currentSession().createQuery(query);
            return fullQuery.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public List<Patient> findAllPatients() {
        CriteriaBuilder cb = currentSession().getCriteriaBuilder();
        CriteriaQuery<Patient> cq = cb.createQuery(Patient.class);
        Root<Patient> patientRoot = cq.from(Patient.class);
        CriteriaQuery<Patient> query = cq.select(patientRoot)
                .where(cb.equal(patientRoot.get(Patient_.statusOfPatient), StatusOfPatient.TREATMENT))
                .orderBy(cb.asc(patientRoot.get(Patient_.firstName)));

        TypedQuery<Patient> fullQuery = currentSession().createQuery(query);
        return fullQuery.getResultList();
    }

    @Override
    public void updatePatient(final Patient patient) {
        currentSession().saveOrUpdate(patient);
    }
}
