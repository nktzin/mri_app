package zin.nikita.javaschool.DAO.Impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zin.nikita.javaschool.DAO.TreatmentDAO;
import zin.nikita.javaschool.Entity.Enums.StatusOfTreatment;
import zin.nikita.javaschool.Entity.Treatment;
import zin.nikita.javaschool.Entity.Treatment_;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Repository
public class TreatmentDAOImpl implements TreatmentDAO {

    private static String dd_MM_yyyy = "dd.MM.yyyy";
    private static String dd_MM_yyyy_HH = "dd.MM.yyyy HH";

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }


    @Override
    public long createTreatment(final Treatment treatment) {
        currentSession().save(treatment);
        return treatment.getIdTreatment();
    }

    @Override
    public Treatment findTreatmentById(final long id) {
        if (currentSession().get(Treatment.class, id) == null) {
            return null;
        } else {
            return currentSession().get(Treatment.class, id);
        }
    }

    @Override
    public Integer findAllCanceledOrDoneTreatments(final int flag) {

        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<Treatment> criteriaQuery = criteriaBuilder.createQuery(Treatment.class);
        Root<Treatment> treatmentRoot = criteriaQuery.from(Treatment.class);

        if (flag == 0) {

            CriteriaQuery<Treatment> query = criteriaQuery.select(treatmentRoot)
                    .where(criteriaBuilder
                            .equal(treatmentRoot.get(Treatment_.statusOfTreatment), StatusOfTreatment.CANCELED));

            TypedQuery<Treatment> fullQuery = currentSession().createQuery(query);
            return fullQuery.getResultList().size();
        } else {
            CriteriaQuery<Treatment> query = criteriaQuery.select(treatmentRoot)
                    .where(criteriaBuilder
                            .equal(treatmentRoot.get(Treatment_.statusOfTreatment), StatusOfTreatment.DONE));

            TypedQuery<Treatment> fullQuery = currentSession().createQuery(query);
            return fullQuery.getResultList().size();
        }
    }

    @Override
    public Integer findAllPlannedTreatments(final int flag, final DateTime localTime) {

        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<Treatment> criteriaQuery = criteriaBuilder.createQuery(Treatment.class);
        Root<Treatment> treatmentRoot = criteriaQuery.from(Treatment.class);

        CriteriaQuery<Treatment> query =
                criteriaQuery.select(treatmentRoot)
                        .where(criteriaBuilder
                                .equal(treatmentRoot
                                        .get(Treatment_.statusOfTreatment), StatusOfTreatment.PLANNED))
                        .orderBy(criteriaBuilder.asc(treatmentRoot.get(Treatment_.dateTime)));

        TypedQuery<Treatment> fullQuery = currentSession().createQuery(query);

        if (flag == 0) {
            return fullQuery.getResultList().size();
        }

        if (flag == 1) {
            return getAllTreatmentsOnCurrentDay(fullQuery.getResultList(), localTime);
        } else {
            return getAllTreatmentsOnNextHour(fullQuery.getResultList(), localTime);
        }
    }

    @Override
    public List<Treatment> findAllTreatmentsOnCurrentDay() {
        List<Treatment> treatmentsOnCurrentDay = new ArrayList<>();

        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<Treatment> criteriaQuery = criteriaBuilder.createQuery(Treatment.class);
        Root<Treatment> treatmentRoot = criteriaQuery.from(Treatment.class);

        CriteriaQuery<Treatment> query =
                criteriaQuery.select(treatmentRoot)
                        .orderBy(criteriaBuilder.asc(treatmentRoot.get(Treatment_.dateTime)));

        TypedQuery<Treatment> fullQuery = currentSession().createQuery(query);

        DateTime dateTime = new DateTime();
        for (Treatment treatment : fullQuery.getResultList()) {
            if (treatment.getDateTime().isAfter(dateTime.withHourOfDay(23))) {
                break;
            }
            if (treatment.getDateTime().toString(dd_MM_yyyy)
                    .equals(dateTime.toString(dd_MM_yyyy))) {
                treatmentsOnCurrentDay.add(treatment);
            }
        }
        return treatmentsOnCurrentDay;
    }

    @Override
    public List<Treatment> findAllTreatmentsOnCurrentDay(int firstResult,
                                                         final int maxResult,
                                                         final DateTime localTime) {
        List<Treatment> treatmentsOnCurrentDay = new ArrayList<>();
        List<Treatment> treatmentsToPage = new ArrayList<>();

        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<Treatment> criteriaQuery = criteriaBuilder.createQuery(Treatment.class);
        Root<Treatment> treatmentRoot = criteriaQuery.from(Treatment.class);

        CriteriaQuery<Treatment> query =
                criteriaQuery.select(treatmentRoot)
                        .where(criteriaBuilder
                                .equal(treatmentRoot.get(Treatment_.statusOfTreatment), StatusOfTreatment.PLANNED))
                        .orderBy(criteriaBuilder.asc(treatmentRoot.get(Treatment_.dateTime)));

        TypedQuery<Treatment> fullQuery = currentSession().createQuery(query);

        for (Treatment treatment : fullQuery.getResultList()) {
            if (treatment.getDateTime().isAfter(localTime.withHourOfDay(23))) {
                break;
            }
            if (treatment.getDateTime().toString(dd_MM_yyyy)
                    .equals(localTime.toString(dd_MM_yyyy))) {
                treatmentsOnCurrentDay.add(treatment);
            }
        }

        if (treatmentsOnCurrentDay.size() != 0) {
            for (int i = firstResult; i < maxResult; i++) {
                if (i == treatmentsOnCurrentDay.size()) {
                    break;
                } else {
                    treatmentsToPage.add(treatmentsOnCurrentDay.get(i));
                }
            }
        }

        return treatmentsToPage;
    }

    @Override
    public List<Treatment> findAllTreatmentsOnNextHour(final int firstResult,
                                                       final int maxResult,
                                                       final DateTime localTime) {
        List<Treatment> treatmentsOnNextHour = new ArrayList<>();
        List<Treatment> treatmentsToPage = new ArrayList<>();

        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<Treatment> criteriaQuery = criteriaBuilder.createQuery(Treatment.class);
        Root<Treatment> treatmentRoot = criteriaQuery.from(Treatment.class);

        CriteriaQuery<Treatment> query =
                criteriaQuery.select(treatmentRoot)
                        .where(criteriaBuilder
                                .equal(treatmentRoot.get(Treatment_.statusOfTreatment), StatusOfTreatment.PLANNED))
                        .orderBy(criteriaBuilder.asc(treatmentRoot.get(Treatment_.dateTime)));

        TypedQuery<Treatment> fullQuery = currentSession().createQuery(query);

        for (Treatment treatment : fullQuery.getResultList()) {
            if (treatment.getDateTime().isAfter(localTime.plusHours(1))) {
                break;
            } else {
                if (treatment.getDateTime().toString(dd_MM_yyyy_HH)
                        .equals(localTime.toString(dd_MM_yyyy_HH)) ||
                        treatment.getDateTime().toString(dd_MM_yyyy_HH)
                                .equals(localTime.plusHours(1).toString(dd_MM_yyyy_HH)))
                    treatmentsOnNextHour.add(treatment);
            }
        }

        if (treatmentsOnNextHour.size() != 0) {
            for (int i = firstResult; i < maxResult - 1; i++) {
                if (i == treatmentsOnNextHour.size()) {
                    break;
                } else {
                    treatmentsToPage.add(treatmentsOnNextHour.get(i));
                }
            }
        }

        return treatmentsToPage;
    }

    @Override
    public List<Treatment> findAllTreatmentsByPage(final int firstResult, final int maxResult) {
        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<Treatment> criteriaQuery = criteriaBuilder.createQuery(Treatment.class);
        Root<Treatment> treatmentRoot = criteriaQuery.from(Treatment.class);

        CriteriaQuery<Treatment> query =
                criteriaQuery.select(treatmentRoot)
                        .where(criteriaBuilder
                                .equal(treatmentRoot.get(Treatment_.statusOfTreatment), StatusOfTreatment.PLANNED))
                        .orderBy(criteriaBuilder.asc(treatmentRoot.get(Treatment_.dateTime)));

        TypedQuery<Treatment> fullQuery = currentSession().createQuery(query);
        fullQuery.setFirstResult(firstResult - 1);
        fullQuery.setMaxResults(maxResult);
        return fullQuery.getResultList();
    }

    @Override
    public List<Treatment> findAllCanceledOrDoneTreatmentsByPage(final int flag,
                                                                 final int firstResult,
                                                                 final int maxResult) {
        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<Treatment> criteriaQuery = criteriaBuilder.createQuery(Treatment.class);
        Root<Treatment> treatmentRoot = criteriaQuery.from(Treatment.class);

        if (flag == 0) {
            CriteriaQuery<Treatment> query = criteriaQuery.select(treatmentRoot)
                    .where(criteriaBuilder
                            .equal(treatmentRoot.get(Treatment_.statusOfTreatment), StatusOfTreatment.CANCELED));

            TypedQuery<Treatment> fullQuery = currentSession().createQuery(query);
            fullQuery.setFirstResult(firstResult - 1);
            fullQuery.setMaxResults(maxResult);
            return fullQuery.getResultList();
        } else {
            CriteriaQuery<Treatment> query = criteriaQuery.select(treatmentRoot)
                    .where(criteriaBuilder
                            .equal(treatmentRoot.get(Treatment_.statusOfTreatment), StatusOfTreatment.DONE));

            TypedQuery<Treatment> fullQuery = currentSession().createQuery(query);
            fullQuery.setFirstResult(firstResult - 1);
            fullQuery.setMaxResults(maxResult);
            return fullQuery.getResultList();
        }
    }

    @Override
    public List<Treatment> findTreatmentsBySetId(final Set<Long> treatmentsId) {
        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<Treatment> criteriaQuery = criteriaBuilder.createQuery(Treatment.class);
        Root<Treatment> treatmentRoot = criteriaQuery.from(Treatment.class);

        List<Treatment> treatmentList = new ArrayList<>();
        for (Long l : treatmentsId) {
            CriteriaQuery<Treatment> query =
                    criteriaQuery.select(treatmentRoot)
                            .where(criteriaBuilder.equal(treatmentRoot.get(Treatment_.idTreatment), l));

            TypedQuery<Treatment> fullQuery = currentSession().createQuery(query);
            treatmentList.add(fullQuery.getSingleResult());
        }
        return treatmentList;
    }

    @Override
    public void updateTreatment(final Treatment treatment) {
        currentSession().saveOrUpdate(treatment);
    }

    ///////////////////////////////////////////Auxiliary methods area///////////////////////////////////////////////////

    private Integer getAllTreatmentsOnCurrentDay(final List<Treatment> treatments,
                                                 final DateTime localTime) {
        List<Treatment> treatmentsOnCurrentDay = new ArrayList<>();
        for (Treatment treatment : treatments) {
            if (treatment.getDateTime().isAfter(localTime.withHourOfDay(23))) {
                break;
            }
            if (treatment.getDateTime().toString(dd_MM_yyyy)
                    .equals(localTime.toString(dd_MM_yyyy))) {
                treatmentsOnCurrentDay.add(treatment);
            }
        }
        return treatmentsOnCurrentDay.size();
    }

    private Integer getAllTreatmentsOnNextHour(final List<Treatment> treatments,
                                               final DateTime localTime) {
        List<Treatment> treatmentsOnNextHour = new ArrayList<>();
        for (Treatment treatment : treatments) {
            if (treatment.getDateTime().isAfter(localTime.plusHours(1))) {
                break;
            }
            if (treatment.getDateTime().toString(dd_MM_yyyy_HH)
                    .equals(localTime.toString(dd_MM_yyyy_HH)) ||
                    treatment.getDateTime().toString(dd_MM_yyyy_HH)
                            .equals(localTime.plusHours(1).toString(dd_MM_yyyy_HH))) {

                treatmentsOnNextHour.add(treatment);
            }
        }
        return treatmentsOnNextHour.size();
    }
}