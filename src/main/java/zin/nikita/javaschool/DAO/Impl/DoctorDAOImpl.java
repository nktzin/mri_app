package zin.nikita.javaschool.DAO.Impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zin.nikita.javaschool.DAO.DoctorDAO;
import zin.nikita.javaschool.Entity.Doctor;
import zin.nikita.javaschool.Entity.Doctor_;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public final class DoctorDAOImpl implements DoctorDAO {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void createDoctor(final Doctor doctor) {
        currentSession().save(doctor);
    }

    @Override
    public Doctor findDoctorById(final long id) {
        if (currentSession().get(Doctor.class, id) == null) {
            return null;
        } else {
            return currentSession().get(Doctor.class, id);
        }
    }

    @Override
    public Doctor findDoctorByLogin(final String login) {
        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<Doctor> criteriaQuery = criteriaBuilder.createQuery(Doctor.class);
        Root<Doctor> doctorRoot = criteriaQuery.from(Doctor.class);

        Predicate predicateForEnable = criteriaBuilder.equal(doctorRoot.get(Doctor_.enabled), true);
        Predicate predicateForLogin = criteriaBuilder.equal(doctorRoot.get(Doctor_.login), login);
        Predicate predicateForEnableLogin = criteriaBuilder.and(predicateForEnable, predicateForLogin);

        CriteriaQuery<Doctor> query = criteriaQuery.select(doctorRoot).where(predicateForEnableLogin);

        TypedQuery<Doctor> fullQuery = currentSession().createQuery(query);
        try {
            return fullQuery.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public List<Doctor> findAllDoctors() {
        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<Doctor> criteriaQuery = criteriaBuilder.createQuery(Doctor.class);
        Root<Doctor> doctorRoot = criteriaQuery.from(Doctor.class);

        CriteriaQuery<Doctor> query = criteriaQuery.select(doctorRoot)
                .where(criteriaBuilder.equal(doctorRoot.get(Doctor_.enabled), true));

        TypedQuery<Doctor> fullQuery = currentSession().createQuery(query);
        return fullQuery.getResultList();
    }

    @Override
    public void updateDoctor(final Doctor doctor) {

        currentSession().saveOrUpdate(doctor);
    }

    @Override
    public void deleteDoctorByLogin(final String login) {
        CriteriaBuilder cb = currentSession().getCriteriaBuilder();
        CriteriaQuery<Doctor> cq = cb.createQuery(Doctor.class);
        Root<Doctor> doctorRoot = cq.from(Doctor.class);
        CriteriaQuery<Doctor> query = cq.select(doctorRoot).where(cb.equal(doctorRoot.get(Doctor_.login), login));

        TypedQuery<Doctor> fullQuery = currentSession().createQuery(query);
        Doctor doctor = fullQuery.getSingleResult();
        doctor.setEnabled(false);
        currentSession().update(doctor);
    }
}
