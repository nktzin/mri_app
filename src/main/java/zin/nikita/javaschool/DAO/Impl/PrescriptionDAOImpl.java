package zin.nikita.javaschool.DAO.Impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zin.nikita.javaschool.DAO.PrescriptionDAO;
import zin.nikita.javaschool.Entity.Prescription;

@Repository
public class PrescriptionDAOImpl implements PrescriptionDAO {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public long createPrescription(final Prescription prescription) {
        currentSession().save(prescription);
        return prescription.getIdPrescription();
    }

    @Override
    public Prescription findPrescriptionById(final long id) {
        if (currentSession().get(Prescription.class, id) == null) {
            return null;
        } else {
            return currentSession().get(Prescription.class, id);
        }
    }

    @Override
    public void updatePrescription(final Prescription prescription) {
        currentSession().saveOrUpdate(prescription);
    }
}
