package zin.nikita.javaschool.DAO.Impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zin.nikita.javaschool.DAO.AdminDAO;
import zin.nikita.javaschool.Entity.Admin;
import zin.nikita.javaschool.Entity.Admin_;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Repository
public final class AdminDAOImpl implements AdminDAO {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void createAdmin(final Admin admin) {
        /*
            First of all, the user would add on "User" table
            with checking on uniqueness and then would add another
            entity (doctor/admin/nurse)
         */
        currentSession().save(admin);
    }

    @Override
    public Admin findAdminByLogin(final String login) {
        CriteriaBuilder cb = currentSession().getCriteriaBuilder();
        CriteriaQuery<Admin> cq = cb.createQuery(Admin.class);
        Root<Admin> adminRoot = cq.from(Admin.class);

        Predicate predicateForEnable = cb.equal(adminRoot.get(Admin_.enabled), true);
        Predicate predicateForLogin = cb.equal(adminRoot.get(Admin_.login), login);
        Predicate predicateForEnableLogin = cb.and(predicateForEnable, predicateForLogin);
        CriteriaQuery<Admin> query = cq.select(adminRoot).where(predicateForEnableLogin);

        TypedQuery<Admin> fullQuery = currentSession().createQuery(query);
        try {
            return fullQuery.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public void updateAdmin(final Admin admin) {
        currentSession().saveOrUpdate(admin);
    }

    @Override
    public void deleteAdminByLogin(final String login) {
        CriteriaBuilder cb = currentSession().getCriteriaBuilder();
        CriteriaQuery<Admin> cq = cb.createQuery(Admin.class);
        Root<Admin> adminRoot = cq.from(Admin.class);
        CriteriaQuery<Admin> query = cq.select(adminRoot).where(cb.equal(adminRoot.get(Admin_.login), login));

        TypedQuery<Admin> fullQuery = currentSession().createQuery(query);
        Admin admin = fullQuery.getSingleResult();
        admin.setEnabled(false);
        currentSession().update(admin);
    }
}
