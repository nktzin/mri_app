package zin.nikita.javaschool.DAO.Impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zin.nikita.javaschool.DAO.ProcAndMedDAO;
import zin.nikita.javaschool.Entity.Enums.ProcedureOrMedicament;
import zin.nikita.javaschool.Entity.ProcAndMed;
import zin.nikita.javaschool.Entity.ProcAndMed_;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class ProcAndMedDAOImpl implements ProcAndMedDAO {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public ProcAndMed findProcedureByName(final String procedureName) {
        return findByName(procedureName);
    }

    @Override
    public ProcAndMed findMedicamentByName(final String medicamentName) {
        return findByName(medicamentName);
    }

    @Override
    public List<ProcAndMed> findAllProcedures() {
        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<ProcAndMed> criteriaQuery = criteriaBuilder.createQuery(ProcAndMed.class);
        Root<ProcAndMed> medRoot = criteriaQuery.from(ProcAndMed.class);

        CriteriaQuery<ProcAndMed> query =
                criteriaQuery.select(medRoot)
                        .where(criteriaBuilder.equal(medRoot.get(ProcAndMed_.procedureOrMedicament), ProcedureOrMedicament.PROCEDURE));

        TypedQuery<ProcAndMed> fullQuery = currentSession().createQuery(query);
        return fullQuery.getResultList();
    }

    @Override
    public List<ProcAndMed> findAllMedicaments() {
        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<ProcAndMed> criteriaQuery = criteriaBuilder.createQuery(ProcAndMed.class);
        Root<ProcAndMed> medRoot = criteriaQuery.from(ProcAndMed.class);

        CriteriaQuery<ProcAndMed> query =
                criteriaQuery.select(medRoot)
                        .where(criteriaBuilder.equal(medRoot.get(ProcAndMed_.procedureOrMedicament), ProcedureOrMedicament.MEDICAMENT));

        TypedQuery<ProcAndMed> fullQuery = currentSession().createQuery(query);
        return fullQuery.getResultList();
    }

    @Override
    public void updateProcAndMed(final ProcAndMed procAndMed) {
        currentSession().update(procAndMed);
    }

    private ProcAndMed findByName(final String name) {
        CriteriaBuilder criteriaBuilder = currentSession().getCriteriaBuilder();
        CriteriaQuery<ProcAndMed> criteriaQuery = criteriaBuilder.createQuery(ProcAndMed.class);
        Root<ProcAndMed> procAndMedRoot = criteriaQuery.from(ProcAndMed.class);
        CriteriaQuery<ProcAndMed> query = criteriaQuery.select(procAndMedRoot)
                .where(criteriaBuilder.equal(procAndMedRoot.get(ProcAndMed_.name), name));
        TypedQuery<ProcAndMed> fullQuery = currentSession().createQuery(query);
        return fullQuery.getSingleResult();
    }
}
