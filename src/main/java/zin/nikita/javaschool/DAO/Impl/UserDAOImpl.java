package zin.nikita.javaschool.DAO.Impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zin.nikita.javaschool.DAO.UserDAO;
import zin.nikita.javaschool.Entity.User;
import zin.nikita.javaschool.Entity.User_;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Repository
public class UserDAOImpl implements UserDAO {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public boolean createUser(final User user) {
        CriteriaBuilder cb = currentSession().getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> rootEntry = cq.from(User.class);
        cq.select(rootEntry).where(cb.equal(rootEntry.get(User_.login), user.getLogin()));
        TypedQuery<User> query = currentSession().createQuery(cq);
        if (query.getResultList().size() != 0) {
            return false;
        } else {
            currentSession().save(user);
            return true;
        }
    }

    @Override
    public User findUserByLogin(final String login) {

        CriteriaBuilder cb = currentSession().getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> userRoot = cq.from(User.class);
        Predicate predicateForEnable = cb.equal(userRoot.get(User_.enabled), true);
        Predicate predicateForLogin = cb.equal(userRoot.get(User_.login), login);
        Predicate predicateForEnableLogin = cb.and(predicateForEnable, predicateForLogin);
        CriteriaQuery<User> query = cq.select(userRoot).where(predicateForEnableLogin);
        try {
            TypedQuery<User> fullQuery = currentSession().createQuery(query);
            return fullQuery.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public String deleteUserByLogin(final String login) {
        CriteriaBuilder cb = currentSession().getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> userRoot = cq.from(User.class);
        CriteriaQuery<User> query = cq.select(userRoot).where(cb.equal(userRoot.get(User_.login), login));

        TypedQuery<User> fullQuery = currentSession().createQuery(query);
        User user = fullQuery.getSingleResult();
        user.setEnabled(false);
        currentSession().update(user);
        return user.getRole().toString();
    }
}
