package zin.nikita.javaschool.DAO;

import zin.nikita.javaschool.Entity.User;

public interface UserDAO {

    boolean createUser(User user);

    User findUserByLogin(String login);

    String deleteUserByLogin(String login);
}
