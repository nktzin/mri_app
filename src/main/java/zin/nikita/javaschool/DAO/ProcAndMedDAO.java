package zin.nikita.javaschool.DAO;

import zin.nikita.javaschool.Entity.ProcAndMed;

import java.util.List;

public interface ProcAndMedDAO {

    ProcAndMed findProcedureByName(String procedureName);

    ProcAndMed findMedicamentByName(String medicamentName);

    List<ProcAndMed> findAllProcedures();

    List<ProcAndMed> findAllMedicaments();

    void updateProcAndMed(ProcAndMed procAndMed);
}
