package zin.nikita.javaschool.DAO;

import zin.nikita.javaschool.Entity.Doctor;

import java.util.List;

public interface DoctorDAO {

    void createDoctor(Doctor doctor);

    Doctor findDoctorById(long id);

    Doctor findDoctorByLogin(String login);

    List<Doctor> findAllDoctors();

    void updateDoctor(Doctor doctor);

    void deleteDoctorByLogin(String login);
}
