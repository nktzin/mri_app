package zin.nikita.javaschool.DTO;

import org.joda.time.DateTime;
import zin.nikita.javaschool.Entity.Enums.DayTimePattern;
import zin.nikita.javaschool.Entity.Enums.WeekTimePattern;


public class PrescriptionDTO {
    /**
     * Values procedureName, medicamentName,
     * toDayTimePatternConvert, toWeekTimePatternConvert,
     * datefilter - are used to convert strings, received
     * from a view to the required data types.
     * procedureName and medicamentName -> ProcAndMed class;
     * toDayTimePatternConvert -> dayTimePattern;
     * toWeekTimePatternConvert -> weekTimePattern;
     * datefilter -> startTime and endTime
     */

    private long idPrescription;
    private long idDoctor;
    private String patientInsurance;
    private String patientFirsName;
    private String patientLastName;
    private String procedureName;
    private String medicamentName;
    private String toDayTimePatternConvert;
    private String toWeekTimePatternConvert;
    private String datefilter;
    private DayTimePattern dayTimePattern;
    private WeekTimePattern weekTimePattern;
    private DateTime startTime;
    private DateTime endTime;
    private String dose;
    private String edit;
    private String cancel;

    public long getIdPrescription() {
        return idPrescription;
    }

    public void setIdPrescription(long idPrescription) {
        this.idPrescription = idPrescription;
    }

    public long getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(long idDoctor) {
        this.idDoctor = idDoctor;
    }

    public String getPatientInsurance() {
        return patientInsurance;
    }

    public void setPatientInsurance(String patientInsurance) {
        this.patientInsurance = patientInsurance;
    }

    public String getPatientFirsName() {
        return patientFirsName;
    }

    public void setPatientFirsName(String patientFirsName) {
        this.patientFirsName = patientFirsName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public String getMedicamentName() {
        return medicamentName;
    }

    public void setMedicamentName(String medicamentName) {
        this.medicamentName = medicamentName;
    }

    public String getToDayTimePatternConvert() {
        return toDayTimePatternConvert;
    }

    public void setToDayTimePatternConvert(String toDayTimePatternConvert) {
        this.toDayTimePatternConvert = toDayTimePatternConvert;
    }

    public String getToWeekTimePatternConvert() {
        return toWeekTimePatternConvert;
    }

    public void setToWeekTimePatternConvert(String toWeekTimePatternConvert) {
        this.toWeekTimePatternConvert = toWeekTimePatternConvert;
    }

    public String getDatefilter() {
        return datefilter;
    }

    public void setDatefilter(String datefilter) {
        this.datefilter = datefilter;
    }

    public DayTimePattern getDayTimePattern() {
        return dayTimePattern;
    }

    public void setDayTimePattern(DayTimePattern dayTimePattern) {
        this.dayTimePattern = dayTimePattern;
    }

    public WeekTimePattern getWeekTimePattern() {
        return weekTimePattern;
    }

    public void setWeekTimePattern(WeekTimePattern weekTimePattern) {
        this.weekTimePattern = weekTimePattern;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public DateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(DateTime endTime) {
        this.endTime = endTime;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getEdit() {
        return edit;
    }

    public void setEdit(String edit) {
        this.edit = edit;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }
}
