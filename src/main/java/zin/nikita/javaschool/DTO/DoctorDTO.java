package zin.nikita.javaschool.DTO;

import zin.nikita.javaschool.Entity.Prescription;

import java.util.HashSet;
import java.util.Set;

public class DoctorDTO {

    private long idDoctor;
    private String login;
    private long patientId;
    private String firstName;
    private String lastName;
    private String specialization;
    private boolean enabled;
    private Set<PatientDTO> patientDTOS = new HashSet<>();
    private Set<Prescription> prescriptions;

    public long getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(long idDoctor) {
        this.idDoctor = idDoctor;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<PatientDTO> getPatientDTOS() {
        return patientDTOS;
    }

    public void setPatientDTOS(Set<PatientDTO> patientDTOS) {
        this.patientDTOS = patientDTOS;
    }

    public Set<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(Set<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }
}
