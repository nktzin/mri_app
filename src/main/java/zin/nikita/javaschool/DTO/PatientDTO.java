package zin.nikita.javaschool.DTO;

import zin.nikita.javaschool.Entity.Doctor;
import zin.nikita.javaschool.Entity.Enums.StatusOfPatient;

import java.util.ArrayList;
import java.util.List;

public class PatientDTO {

    private long idPatient;
    private long idDoctor;
    private String firstName;
    private String lastName;
    private String insurance;
    private String diagnosis;
    private boolean isDischarged;
    private Doctor doctor;
    private List<TreatmentDTO> treatments = new ArrayList<>();
    private List<PrescriptionDTO> prescriptionDTOS = new ArrayList<>();
    private int hasErrors = 0;
    private StatusOfPatient statusOfPatient;
    private String prescriptionName;

    public long getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(long idPatient) {
        this.idPatient = idPatient;
    }

    public long getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(long idDoctor) {
        this.idDoctor = idDoctor;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public boolean isDischarged() {
        return isDischarged;
    }

    public void setDischarged(boolean discharged) {
        isDischarged = discharged;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public List<TreatmentDTO> getTreatments() {
        return treatments;
    }

    public void setTreatments(List<TreatmentDTO> treatments) {
        this.treatments = treatments;
    }

    public List<PrescriptionDTO> getPrescriptionDTOS() {
        return prescriptionDTOS;
    }

    public void setPrescriptionDTOS(List<PrescriptionDTO> prescriptionDTOS) {
        this.prescriptionDTOS = prescriptionDTOS;
    }

    public int getHasErrors() {
        return hasErrors;
    }

    public void setHasErrors(int hasErrors) {
        this.hasErrors = hasErrors;
    }

    public StatusOfPatient getStatusOfPatient() {
        return statusOfPatient;
    }

    public void setStatusOfPatient(StatusOfPatient statusOfPatient) {
        this.statusOfPatient = statusOfPatient;
    }

    public String getPrescriptionName() {
        return prescriptionName;
    }

    public void setPrescriptionName(String prescriptionName) {
        this.prescriptionName = prescriptionName;
    }
}
