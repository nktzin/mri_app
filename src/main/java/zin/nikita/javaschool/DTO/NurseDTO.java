package zin.nikita.javaschool.DTO;

import zin.nikita.javaschool.Entity.Enums.RoleEnum;

import java.util.ArrayList;
import java.util.List;

public class NurseDTO {

    private long idNurse;
    private long idPatient;
    private List<Integer> numberOfPages = new ArrayList<>();
    private String firstName;
    private String lastName;
    private String login;
    private String password;
    private String confirmPassword;
    private RoleEnum role;
    private boolean enabled;

    public long getIdNurse() {
        return idNurse;
    }

    public void setIdNurse(long idNurse) {
        this.idNurse = idNurse;
    }

    public long getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(long idPatient) {
        this.idPatient = idPatient;
    }

    public List<Integer> getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(List<Integer> numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
