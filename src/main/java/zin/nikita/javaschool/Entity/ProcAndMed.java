package zin.nikita.javaschool.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import zin.nikita.javaschool.Entity.Enums.ProcedureOrMedicament;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "procedures_medicaments")
public final class ProcAndMed {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idPAM;

    private String name;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ProcedureOrMedicament procedureOrMedicament;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "procAndMeds")
    @JsonBackReference
    private Set<Prescription> prescriptions = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "procAndMeds")
    @JsonBackReference
    private Set<Treatment> treatments = new HashSet<>();

    private boolean enabled;

    public ProcAndMed() {
    }

    public long getIdPAM() {
        return idPAM;
    }

    public void setIdPAM(final long idPAM) {
        this.idPAM = idPAM;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public ProcedureOrMedicament getProcedureOrMedicament() {
        return procedureOrMedicament;
    }

    public void setProcedureOrMedicament(final ProcedureOrMedicament procedureOrMedicament) {
        this.procedureOrMedicament = procedureOrMedicament;
    }

    public Set<Prescription> getPrescription() {
        return prescriptions;
    }

    public void setPrescription(final Set<Prescription> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public Set<Treatment> getTreatment() {
        return treatments;
    }

    public void setTreatment(final Set<Treatment> treatments) {
        this.treatments = treatments;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProcAndMed that = (ProcAndMed) o;
        return idPAM == that.idPAM
                && enabled == that.enabled
                && Objects.equals(name, that.name)
                && procedureOrMedicament == that.procedureOrMedicament;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPAM, name, procedureOrMedicament, enabled);
    }

    @Override
    public String toString() {
        return "ProcAndMed{"
                + "idPAM=" + idPAM
                + ", name='" + name + '\''
                + ", procedureOrMedicament=" + procedureOrMedicament
                + ", enabled=" + enabled
                + '}';
    }
}
