package zin.nikita.javaschool.Entity.Enums;

public enum RoleEnum {
    USER, NURSE, DOCTOR, ADMIN
}
