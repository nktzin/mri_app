package zin.nikita.javaschool.Entity.Enums;

public enum DayTimePattern {
    ONCE_A_DAY,
    TWICE_A_DAY,
    THREE_TIMES_A_DAY,
    FOUR_TIMES_A_DAY,
    FIVE_TIMES_A_DAY
}
