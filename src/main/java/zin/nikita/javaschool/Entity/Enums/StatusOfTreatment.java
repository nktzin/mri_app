package zin.nikita.javaschool.Entity.Enums;

public enum StatusOfTreatment {
    PLANNED, DONE, CANCELED
}
