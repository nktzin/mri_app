package zin.nikita.javaschool.Entity.Enums;

public enum StatusOfPatient {
    TREATMENT, DISCHARGED
}
