package zin.nikita.javaschool.Entity.Enums;

public enum WeekTimePattern {
    ONCE_A_WEEK,
    TWICE_A_WEEK,
    THREE_TIMES_A_WEEK,
    FOUR_TIMES_A_WEEK,
    FIVE_TIMES_A_WEEK,
    SIX_TIMES_A_WEEK,
    SEVEN_TIMES_A_WEEK
}
