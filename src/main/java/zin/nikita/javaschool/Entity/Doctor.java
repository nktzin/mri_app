package zin.nikita.javaschool.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Builder
@Entity
@Table(name = "Doctors")
@NoArgsConstructor
@AllArgsConstructor
public final class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idDoctor;

    @Column(name = "login_user", nullable = false, unique = true)
    private String login;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String specialization;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "doctor")
    @JsonManagedReference
    private Set<Patient> patients = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "doctor")
    @JsonManagedReference
    private Set<Prescription> prescriptions;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idAdmin", nullable = false)
    @JsonBackReference
    private Admin admin;

    private boolean enabled;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Doctor doctor = (Doctor) o;
        return idDoctor == doctor.idDoctor
                && enabled == doctor.enabled
                && Objects.equals(login, doctor.login)
                && Objects.equals(firstName, doctor.firstName)
                && Objects.equals(lastName, doctor.lastName)
                && Objects.equals(admin, doctor.admin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDoctor, login, firstName, lastName, specialization, enabled);
    }

    @Override
    public String toString() {
        return "Doctor{"
                + "id=" + idDoctor
                + ", login='" + login + '\''
                + ", firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", specialization='" + specialization + '\''
                + ", enabled=" + enabled
                + '}';
    }
}
