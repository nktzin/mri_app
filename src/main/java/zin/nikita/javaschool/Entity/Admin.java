package zin.nikita.javaschool.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Builder
@Entity
@Table(name = "administrators")
@NoArgsConstructor
@AllArgsConstructor
public final class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idAdmin;

    @Column(name = "login_user")
    private String login;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "admin")
    @JsonManagedReference
    private Set<Doctor> doctors;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "admin")
    @JsonManagedReference
    private Set<Nurse> nurses;

    private boolean enabled;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Admin admin = (Admin) o;
        return idAdmin == admin.idAdmin &&
                enabled == admin.enabled &&
                Objects.equals(login, admin.login) &&
                Objects.equals(firstName, admin.firstName) &&
                Objects.equals(lastName, admin.lastName) &&
                Objects.equals(doctors, admin.doctors) &&
                Objects.equals(nurses, admin.nurses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idAdmin, login, firstName, lastName, doctors, nurses, enabled);
    }

    @Override
    public String toString() {
        return "Admin{"
                + "idAdmin=" + idAdmin
                + ", login='" + login + '\''
                + ", firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", enabled=" + enabled
                + '}';
    }
}
