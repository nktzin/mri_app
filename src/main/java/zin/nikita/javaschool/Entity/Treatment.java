package zin.nikita.javaschool.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import zin.nikita.javaschool.Entity.Enums.StatusOfTreatment;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "treatments")
public final class Treatment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idTreatment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPatient")
    @JsonBackReference
    private Patient patient;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPrescription")
    @JsonBackReference
    private Prescription prescription;


    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "treatments")
    @JsonManagedReference
    private List<Nurse> nurseList = new ArrayList<>();

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "date_of_treatment")
    private DateTime dateTime;

    @Column(name = "treatment_status")
    @Enumerated(EnumType.STRING)
    private StatusOfTreatment statusOfTreatment;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "treatment_procAndMeds",
            joinColumns = @JoinColumn(name = "treatment_id"),
            inverseJoinColumns = @JoinColumn(name = "procAndMeds_id"))
    @JsonManagedReference
    private Set<ProcAndMed> procAndMeds = new HashSet<>();

    private String dose;

    private String cause;

    private boolean enabled;

    public Treatment() {
    }

    public long getIdTreatment() {
        return idTreatment;
    }

    public void setIdTreatment(final long idTreatment) {
        this.idTreatment = idTreatment;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(final Patient patient) {
        this.patient = patient;
    }

    public Prescription getPrescription() {
        return prescription;
    }

    public void setPrescription(final Prescription prescription) {
        this.prescription = prescription;
    }

    public List<Nurse> getNurseList() {
        return nurseList;
    }

    public void setNurseList(List<Nurse> nurseList) {
        this.nurseList = nurseList;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(final DateTime dateTime) {
        this.dateTime = dateTime;
    }

    public StatusOfTreatment getStatusOfTreatment() {
        return statusOfTreatment;
    }

    public void setStatusOfTreatment(final StatusOfTreatment statusOfTreatment) {
        this.statusOfTreatment = statusOfTreatment;
    }

    public Set<ProcAndMed> getProcAndMeds() {
        return procAndMeds;
    }

    public void setProcAndMeds(final Set<ProcAndMed> procAndMeds) {
        this.procAndMeds = procAndMeds;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Treatment treatment = (Treatment) o;
        return idTreatment == treatment.idTreatment
                && Objects.equals(dose, treatment.dose)
                && enabled == treatment.enabled
                && Objects.equals(dateTime, treatment.dateTime)
                && statusOfTreatment == treatment.statusOfTreatment;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTreatment, dateTime, statusOfTreatment, enabled, dose);
    }

    @Override
    public String toString() {
        return "Treatment{"
                + "id=" + idTreatment
                + ", dateTime=" + dateTime
                + ", statusOfTreatment=" + statusOfTreatment
                + ", enabled=" + enabled
                + ", dose=" + dose
                + '}';
    }
}
