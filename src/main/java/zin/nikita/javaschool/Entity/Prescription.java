package zin.nikita.javaschool.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import zin.nikita.javaschool.Entity.Enums.DayTimePattern;
import zin.nikita.javaschool.Entity.Enums.WeekTimePattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Builder
@Entity
@Table(name = "prescriptions")
@NoArgsConstructor
@AllArgsConstructor
public final class Prescription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idPrescription;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPatient")
    @JsonBackReference
    private Patient patient;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doctor_id")
    @JsonBackReference
    private Doctor doctor;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "prescription")
    @JsonManagedReference
    private Set<Treatment> treatments = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "prescription_procAndMeds",
            joinColumns = @JoinColumn(name = "prescription_id"),
            inverseJoinColumns = @JoinColumn(name = "procAndMeds_id"))
    @JsonManagedReference
    private Set<ProcAndMed> procAndMeds = new HashSet<>();

    @Enumerated(EnumType.ORDINAL)
    private DayTimePattern dayTimePattern;

    @Enumerated(EnumType.ORDINAL)
    private WeekTimePattern weekTimePattern;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "start_date", nullable = false)
    private DateTime startTime;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @Column(name = "end_time", nullable = false)
    private DateTime endTime;

    @Column
    private String dose;

    @Column(name = "prescription_name")
    private String prescriptionName;

    private boolean enabled;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Prescription that = (Prescription) o;
        return idPrescription == that.idPrescription
                && enabled == that.enabled
                && weekTimePattern == that.weekTimePattern
                && Objects.equals(startTime, that.startTime)
                && Objects.equals(endTime, that.endTime)
                && Objects.equals(dose, that.dose);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPrescription,
                dayTimePattern, weekTimePattern,
                startTime, endTime, dose, enabled);
    }

    @Override
    public String toString() {
        return "Prescription{"
                + "idPrescription=" + idPrescription
                + ", dayTimePattern=" + dayTimePattern
                + ", weekTimePattern=" + weekTimePattern
                + ", startDateTime=" + startTime
                + ", endDateTime=" + endTime
                + ", dose='" + dose + '\''
                + ", enabled=" + enabled
                + '}';
    }
}
