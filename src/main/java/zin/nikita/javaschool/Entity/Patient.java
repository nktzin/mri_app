package zin.nikita.javaschool.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import zin.nikita.javaschool.Entity.Enums.StatusOfPatient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Builder
@Entity
@Table(name = "patients")
@NoArgsConstructor
@AllArgsConstructor
public final class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idPatient;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(nullable = false, unique = true)
    private String insurance;

    @Size(max = 1200)
    private String diagnosis;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doctor_id")
    @JsonBackReference
    private Doctor doctor;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "patients")
    @JsonBackReference
    private List<Nurse> nurses = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "patient")
    @JsonManagedReference
    private Set<Prescription> prescriptions = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "patient")
    @JsonManagedReference
    private List<Treatment> treatments = new ArrayList<>();

    @Column(name = "patient_status")
    @Enumerated(EnumType.STRING)
    private StatusOfPatient statusOfPatient;

    private boolean enabled;

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Patient patient = (Patient) o;
        return idPatient == patient.idPatient
                && Objects.equals(insurance, patient.insurance)
                && enabled == patient.enabled
                && Objects.equals(firstName, patient.firstName)
                && Objects.equals(lastName, patient.lastName)
                && Objects.equals(diagnosis, patient.diagnosis)
                && statusOfPatient == patient.statusOfPatient;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPatient, firstName, lastName, insurance, diagnosis, statusOfPatient, enabled);
    }

    @Override
    public String toString() {
        return "Patient{"
                + "idPatient=" + idPatient
                + ", firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", insurance=" + insurance
                + ", diagnosis='" + diagnosis + '\''
                + ", statusOfPatient=" + statusOfPatient
                + ", enabled=" + enabled
                + '}';
    }
}
