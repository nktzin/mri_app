package zin.nikita.javaschool.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Builder
@Entity
@Table(name = "nurses")
@NoArgsConstructor
@AllArgsConstructor
public final class Nurse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idNurse;

    @Column(name = "login_user", nullable = false, unique = true)
    private String login;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "treatment_nurse",
            joinColumns = @JoinColumn(name = "nurse_id"),
            inverseJoinColumns = @JoinColumn(name = "treatment_id"))
    @JsonManagedReference
    private Set<Treatment> treatments = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "patient_nurse",
            joinColumns = @JoinColumn(name = "nurse_id"),
            inverseJoinColumns = @JoinColumn(name = "patient_id"))
    @JsonManagedReference
    private List<Patient> patients = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idAdmin")
    @JsonBackReference
    private Admin admin;

    private boolean enabled;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Nurse nurse = (Nurse) o;
        return idNurse == nurse.idNurse &&
                enabled == nurse.enabled &&
                Objects.equals(login, nurse.login) &&
                Objects.equals(firstName, nurse.firstName) &&
                Objects.equals(lastName, nurse.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idNurse, login, firstName, lastName, enabled);
    }

    @Override
    public String toString() {
        return "Nurse{"
                + "id=" + idNurse
                + ", login='" + login + '\''
                + ", firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", enabled=" + enabled
                + '}';
    }
}
