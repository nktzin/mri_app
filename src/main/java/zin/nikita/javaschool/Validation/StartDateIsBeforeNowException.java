package zin.nikita.javaschool.Validation;

public class StartDateIsBeforeNowException extends Throwable {
    public StartDateIsBeforeNowException(final String message) {
        super(message);
    }
}
