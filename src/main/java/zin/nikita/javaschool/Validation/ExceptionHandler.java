package zin.nikita.javaschool.Validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@ControllerAdvice
public class ExceptionHandler implements HandlerExceptionResolver {

    private static final Logger log = LoggerFactory.getLogger(ExceptionHandler.class);

    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        log.error("Error catching in ExceptionHandler class with message: {} and cause: {}", e.getMessage(), e.getCause());
        String msg = "Просим прощения, произошла ошибка на стороне сервера, попробуйте пожалуйста позже.";
        return new ModelAndView("error", "errorMsg", msg);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ModelAndView requestHandlingNoHandlerFound(HttpServletRequest req, NoHandlerFoundException ex) {
        log.info("Page not found for that url: {}", req.getRequestURI());
        return new ModelAndView("error404");
    }
}
