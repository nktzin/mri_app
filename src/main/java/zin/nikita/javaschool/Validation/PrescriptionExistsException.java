package zin.nikita.javaschool.Validation;

@SuppressWarnings("serial")
public class PrescriptionExistsException extends Throwable{
    public PrescriptionExistsException(final String message) {
        super(message);
    }
}
