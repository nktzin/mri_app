package zin.nikita.javaschool.Validation;

@SuppressWarnings("serial")
public class LoginExistsException extends Throwable {
    public LoginExistsException(final String message) {
        super(message);
    }
}
