package zin.nikita.javaschool.Validation;

public class InsuranceExistsException extends Throwable {
    public InsuranceExistsException(final String message) {
        super(message);
    }
}
