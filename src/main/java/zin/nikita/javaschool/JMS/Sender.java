package zin.nikita.javaschool.JMS;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Hashtable;
import java.util.Objects;
import java.util.Properties;

@Component
public class Sender {

    private static final Logger log = LoggerFactory.getLogger(Sender.class);

    private Environment env;

    @Autowired
    private void setEnvironment(Environment env) {
        this.env = env;
    }

    public void sendMessage(String msg) {

        final Properties prop = new Properties();
        prop.put(Context.INITIAL_CONTEXT_FACTORY, Objects.requireNonNull(env.getProperty("jms.initial.context.factory")));
        prop.put(Context.PROVIDER_URL, Objects.requireNonNull(env.getProperty("jms.wildfly.remote.url")));
        prop.put(Context.SECURITY_PRINCIPAL, Objects.requireNonNull(env.getProperty("jms.username")));
        prop.put(Context.SECURITY_CREDENTIALS, Objects.requireNonNull(env.getProperty("jms.password")));

        Context context = null;
        QueueConnection connection = null;
        QueueSession session = null;
        QueueSender sender = null;

        try {
            context = new InitialContext(prop);
            QueueConnectionFactory connectionFactory = (QueueConnectionFactory) context.lookup(env.getProperty("jms.connection.factory.jndi"));
            Queue queue = (Queue) context.lookup(env.getProperty("jms.queue.jndi"));

            connection = connectionFactory.createQueueConnection(env.getProperty("jms.username"), env.getProperty("jms.password"));
            connection.start();

            session = connection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
            sender = session.createSender(queue);
            TextMessage message = session.createTextMessage(msg);

            sender.send(message);
            log.info("Message sent to elPanel app");
            sender.close();
            session.close();
            connection.close();

        } catch (NamingException | JMSException ex) {
            log.error("Exception in method sendMessage(): {}", ex);
        } finally {
            String exceptionMsg = "Exception in method sendMessage() in finally clause: {}";
            if (context != null) {
                try {
                    context.close();
                } catch (NamingException ex) {
                    log.error(exceptionMsg, ex);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException ex) {
                    log.error(exceptionMsg, ex);
                }
            }
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException ex) {
                    log.error(exceptionMsg, ex);
                }
            }
            if (sender != null) {
                try {
                    sender.close();
                } catch (JMSException ex) {
                    log.error(exceptionMsg, ex);
                }
            }
        }
    }
}
