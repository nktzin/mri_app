package zin.nikita.javaschool.Config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.XmlViewResolver;
import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.DTO.DoctorDTO;
import zin.nikita.javaschool.DTO.NurseDTO;
import zin.nikita.javaschool.DTO.PatientDTO;

import java.util.List;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = {"zin.nikita.javaschool"})
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/")
        .addResourceLocations("/test/resources");
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        GsonHttpMessageConverter msgConverter = new GsonHttpMessageConverter();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        msgConverter.setGson(gson);
        converters.add(msgConverter);
    }

    @Bean
    public ViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass(JstlView.class);
        bean.setOrder(1);
        bean.setPrefix("/WEB-INF/view/");
        bean.setSuffix(".jsp");

        return bean;
    }

    @Bean(name = "xmlViewResolver")
    public XmlViewResolver getXmlViewResolver() {
        XmlViewResolver xmlViewResolver = new XmlViewResolver();
        Resource resource = new ClassPathResource("excel-pdf-config.xml");
        xmlViewResolver.setOrder(0);
        xmlViewResolver.setLocation(resource);
        return xmlViewResolver;
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public DoctorDTO sessionDoctorDTO() {
        return new DoctorDTO();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public NurseDTO sessionNurseDTO() {
        return new NurseDTO();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public PatientDTO requestPatientDTO() {
        return new PatientDTO();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public PatientDTO sessionPatientDTO() {
        return new PatientDTO();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public AdminDTO sessionAdminDTO() {
        return new AdminDTO();
    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        //General views
        registry.addViewController("/").setViewName("forward:/login");
        registry.addViewController("/index.html").setViewName("index");
        registry.addViewController("/login.html").setViewName("login");
        registry.addViewController("about.html").setViewName("about");

        //Doctor's view
        registry.addViewController("/doctor.html").setViewName("doctor/doctor");
        registry.addViewController("/registerPatient.html").setViewName("doctor/registerPatient");
        registry.addViewController("/patientDischarge.html").setViewName("doctor/patientDischarge");
        registry.addViewController("/prescription.html").setViewName("doctor/prescription");
        registry.addViewController("/editTreatment.html").setViewName("doctor/patientPrescriptions");
        registry.addViewController("/findPatient.html").setViewName("doctor/findPatient");
    }
}
