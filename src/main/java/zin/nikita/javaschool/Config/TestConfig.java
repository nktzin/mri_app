package zin.nikita.javaschool.Config;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Profile("test")
@Configuration
@EnableTransactionManagement
@PropertySource(value = "classpath:util.properties")
public class TestConfig {

    @Value("${jdbc.h2.driverClass}")
    private String driverClass;
    @Value("${jdbc.h2.url}")
    private String jdbcUrl;
    @Value("${jdbc.h2.user}")
    private String jdbcUsername;
    @Value("${jdbc.h2.pass}")
    private String jdbcPassword;

    @Value("${hibernate.h2.dialect}")
    private String hibernateDialect;
    @Value("${hibernate.h2.hbm2ddl.auto}")
    private String hibernateHBM;
    @Value("${hibernate.show_sql}")
    private String showSql;
    @Value("${hibernate.cache.use_second_level_cache}")
    private String secondLevelCache;
    @Value("${hibernate.cache.use_query_cache}")
    private String queryCache;

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("zin.nikita.javaschool.Entity");
        sessionFactory.setHibernateProperties(hibernateProperties());

        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driverClass);
        dataSource.setUrl(jdbcUrl);
        dataSource.setUsername(jdbcUsername);
        dataSource.setPassword(jdbcPassword);

        return dataSource;
    }

    @Bean
    public PlatformTransactionManager hibernateTransactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }

    private Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", hibernateHBM);
        hibernateProperties.setProperty("hibernate.dialect", hibernateDialect);
        hibernateProperties.setProperty("hibernate.show_sql", showSql);
        hibernateProperties.setProperty("hibernate.cache.use_second_level", secondLevelCache);
        hibernateProperties.setProperty("hibernate.cache.use_query_cache", queryCache);

        return hibernateProperties;
    }
}
