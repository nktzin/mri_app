package zin.nikita.javaschool.Service;

import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.Entity.Admin;

public interface AdminService {

    void createAdmin(Admin admin);

    AdminDTO findAdminDTOByLogin(String login);

    void updateAdmin(Admin admin);

    void deleteAdminByLogin(String login);
}
