package zin.nikita.javaschool.Service.Impl;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.DAO.NurseDAO;
import zin.nikita.javaschool.DAO.PatientDAO;
import zin.nikita.javaschool.DAO.PrescriptionDAO;
import zin.nikita.javaschool.DAO.ProcAndMedDAO;
import zin.nikita.javaschool.DAO.TreatmentDAO;
import zin.nikita.javaschool.DTO.PrescriptionDTO;
import zin.nikita.javaschool.DTO.TreatmentDTO;
import zin.nikita.javaschool.Entity.Enums.StatusOfTreatment;
import zin.nikita.javaschool.Entity.Enums.WeekTimePattern;
import zin.nikita.javaschool.Entity.Nurse;
import zin.nikita.javaschool.Entity.Patient;
import zin.nikita.javaschool.Entity.Prescription;
import zin.nikita.javaschool.Entity.ProcAndMed;
import zin.nikita.javaschool.Entity.Treatment;
import zin.nikita.javaschool.GSON.TreatmentGSON;
import zin.nikita.javaschool.Service.TreatmentService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TreatmentServiceImpl implements TreatmentService {

    private TreatmentDAO treatmentDAO;

    private ProcAndMedDAO procAndMedDAO;

    private PatientDAO patientDAO;

    private NurseDAO nurseDAO;

    private PrescriptionDAO prescriptionDAO;

    @Autowired
    private void setTreatmentDAO(final TreatmentDAO treatmentDAO) {
        this.treatmentDAO = treatmentDAO;
    }

    @Autowired
    private void setProcAndMedDAO(final ProcAndMedDAO procAndMedDAO) {
        this.procAndMedDAO = procAndMedDAO;
    }

    @Autowired
    private void setPatientDAO(final PatientDAO patientDAO) {
        this.patientDAO = patientDAO;
    }

    @Autowired
    private void setNurseDAO(final NurseDAO nurseDAO) {
        this.nurseDAO = nurseDAO;
    }

    @Autowired
    private void setPrescriptionDAO(final PrescriptionDAO prescriptionDAO) {
        this.prescriptionDAO = prescriptionDAO;
    }

    /**
     * This method creates treatments with a daily pattern of equal ONCE_A_DAY, or null.
     *
     * @param numberOfTreatments number of Treatments required
     * @param prescriptionDTO    which contains information about the period of treatment, procedure, medications.
     * @return the number of treatmentsId to be added to the patient, nurse, and prescription
     */
    @Override
    @Transactional
    public Set<Long> createTreatment(final int numberOfTreatments,
                                     final PrescriptionDTO prescriptionDTO) {
        int countOfWeeks = 0;
        int numberOfGaps = findNumberOfGaps(prescriptionDTO);
        int treatmentsCounter = getTreatmentsCounter(numberOfTreatments, prescriptionDTO);
        List<Integer> intervals = findIntervalsForDays(numberOfGaps);
        int firstInterval = intervals.get(0);
        int secondInterval = intervals.get(1);

        DateTime dateTime = buildDate(prescriptionDTO);
        DateTime tempDate;
        ProcAndMed procAndMed = getProcedureOrMedicament(prescriptionDTO);
        Set<Long> treatmentsId = new HashSet<>();

        for (int i = 0; i < treatmentsCounter; i++) {
            if (dateTime.isAfter(prescriptionDTO.getEndTime())) {
                break;
            } else {
                if (prescriptionDTO.getWeekTimePattern().equals(WeekTimePattern.ONCE_A_WEEK)) {
                    makeTreatment(dateTime, treatmentsId, procAndMed, prescriptionDTO.getDose());
                    tempDate = dateTime.plusWeeks(1);
                    dateTime = tempDate;
                } else {
                    if (countOfWeeks < numberOfGaps) {
                        makeTreatment(dateTime, treatmentsId, procAndMed, prescriptionDTO.getDose());
                        tempDate = dateTime.plusDays(firstInterval);
                        dateTime = tempDate;
                        countOfWeeks++;
                    } else {
                        makeTreatment(dateTime, treatmentsId, procAndMed, prescriptionDTO.getDose());
                        tempDate = dateTime.plusDays(secondInterval);
                        dateTime = tempDate;
                        countOfWeeks = 0;
                    }
                }
            }
        }
        return treatmentsId;
    }

    @Override
    @Transactional
    public Set<Long> createTreatmentWithTwoDayPattern(final int numberOfTreatments,
                                                      final PrescriptionDTO prescriptionDTO) {
        int flag = 2;
        int numberOfLoops = 1;
        int localCounter = 0;
        int countOfWeeks = 0;
        int treatmentsCounter = getTreatmentsCounter(numberOfTreatments, prescriptionDTO);
        int numberOfGaps = findNumberOfGaps(prescriptionDTO);
        List<Integer> intervals = findIntervalForDaysAndWeeks(numberOfGaps);

        DateTime dateTime = buildDate(prescriptionDTO);
        ProcAndMed procAndMed = getProcedureOrMedicament(prescriptionDTO);
        Set<Long> treatmentsId = new HashSet<>();

        return getTreatments(localCounter, treatmentsCounter, numberOfLoops, countOfWeeks,
                numberOfGaps, dateTime, treatmentsId, procAndMed, prescriptionDTO,
                intervals, flag);
    }

    @Override
    @Transactional
    public Set<Long> createTreatmentWithThreeDayPattern(final int numberOfTreatments,
                                                        final PrescriptionDTO prescriptionDTO) {
        int flag = 3;
        int numberOfLoops = 2;
        int localCounter = 0;
        int countOfWeeks = 0;
        int treatmentsCounter = getTreatmentsCounter(numberOfTreatments, prescriptionDTO);
        int numberOfGaps = findNumberOfGaps(prescriptionDTO);
        List<Integer> intervals = findIntervalForDaysAndWeeks(numberOfGaps);

        DateTime dateTime = buildDate(prescriptionDTO);
        ProcAndMed procAndMed = getProcedureOrMedicament(prescriptionDTO);
        Set<Long> treatmentsId = new HashSet<>();

        return getTreatments(localCounter, treatmentsCounter, numberOfLoops, countOfWeeks,
                numberOfGaps, dateTime, treatmentsId, procAndMed, prescriptionDTO, intervals, flag);
    }

    @Override
    @Transactional
    public Set<Long> createTreatmentWithFourDayPattern(final int numberOfTreatments,
                                                       final PrescriptionDTO prescriptionDTO) {
        int flag = 4;
        int numberOfLoops = 3;
        int localCounter = 0;
        int countOfWeeks = 0;
        int treatmentsCounter = getTreatmentsCounter(numberOfTreatments, prescriptionDTO);
        int numberOfGaps = findNumberOfGaps(prescriptionDTO);
        List<Integer> intervals = findIntervalForDaysAndWeeks(numberOfGaps);

        DateTime dateTime = buildDate(prescriptionDTO);
        ProcAndMed procAndMed = getProcedureOrMedicament(prescriptionDTO);
        Set<Long> treatmentsId = new HashSet<>();

        return getTreatments(localCounter, treatmentsCounter, numberOfLoops, countOfWeeks,
                numberOfGaps, dateTime, treatmentsId, procAndMed, prescriptionDTO, intervals, flag);
    }

    @Override
    @Transactional
    public Set<Long> createTreatmentWithFiveDayPattern(final int numberOfTreatments,
                                                       final PrescriptionDTO prescriptionDTO) {
        int flag = 5;
        int numberOfLoops = 4;
        int localCounter = 0;
        int countOfWeeks = 0;
        int treatmentsCounter = getTreatmentsCounter(numberOfTreatments, prescriptionDTO);
        int numberOfGaps = findNumberOfGaps(prescriptionDTO);
        List<Integer> intervals = findIntervalForDaysAndWeeks(numberOfGaps);

        DateTime dateTime = buildDate(prescriptionDTO);
        ProcAndMed procAndMed = getProcedureOrMedicament(prescriptionDTO);
        Set<Long> treatmentsId = new HashSet<>();

        return getTreatments(localCounter, treatmentsCounter, numberOfLoops, countOfWeeks,
                numberOfGaps, dateTime, treatmentsId, procAndMed, prescriptionDTO, intervals, flag);
    }

    @Override
    @Transactional
    public void addPatient(final Set<Long> treatmentsId, final PrescriptionDTO prescriptionDTO) {
        Patient patient = patientDAO.findPatientByInsurance(prescriptionDTO.getPatientInsurance());
        List<Treatment> treatmentList = treatmentDAO.findTreatmentsBySetId(treatmentsId);
        for (Treatment treatment : treatmentList) {
            treatment.setPatient(patient);
            patient.getTreatments().add(treatment);
        }
        patientDAO.updatePatient(patient);
        for (Treatment treatment : treatmentList) {
            treatmentDAO.updateTreatment(treatment);
        }
    }

    @Override
    @Transactional
    public void addNurse(final Set<Long> treatmentsId) {
        List<Nurse> nurseList = nurseDAO.findAllNurses();
        List<Treatment> treatmentList = treatmentDAO.findTreatmentsBySetId(treatmentsId);
        for (Nurse nurse : nurseList) {
            for (Treatment treatment : treatmentList) {
                nurse.getTreatments().add(treatment);
                treatment.setNurseList(nurseList);
            }
        }
        for (Nurse nurse : nurseList) {
            nurseDAO.updateNurse(nurse);
        }
        for (Treatment treatment : treatmentList) {
            treatmentDAO.updateTreatment(treatment);
        }
    }

    @Override
    @Transactional
    public void addPrescription(final Set<Long> treatmentsId, final PrescriptionDTO prescriptionDTO) {
        Prescription prescription = prescriptionDAO
                .findPrescriptionById(prescriptionDTO.getIdPrescription());
        List<Treatment> treatmentList = treatmentDAO.findTreatmentsBySetId(treatmentsId);
        for (Treatment treatment : treatmentList) {
            treatment.setPrescription(prescription);
            prescription.getTreatments().add(treatment);
        }
        for (Treatment treatment : treatmentList) {
            treatmentDAO.updateTreatment(treatment);
        }
        prescriptionDAO.updatePrescription(prescription);
    }

    @Override
    @Transactional
    public List<TreatmentGSON> findTreatmentsGsonOnCurrentDay() {
        List<TreatmentGSON> treatmentGSONList = new ArrayList<>();
        List<Treatment> treatments = treatmentDAO.findAllTreatmentsOnCurrentDay();
        if (treatments != null) {
            for (Treatment treatment : treatments) {
                treatmentGSONList.add(getTreatmentGSON(treatment));
            }
        }
        return treatmentGSONList;
    }

    @Override
    @Transactional
    public Integer findAllPlannedTreatments(final int flag, final DateTime localTime) {
        return treatmentDAO.findAllPlannedTreatments(flag, localTime);
    }

    @Override
    @Transactional
    public Integer findAllCanceledTreatments() {
        return treatmentDAO.findAllCanceledOrDoneTreatments(0);
    }

    @Override
    @Transactional
    public Integer findAllDoneTreatments() {
        return treatmentDAO.findAllCanceledOrDoneTreatments(1);
    }

    @Override
    @Transactional
    public List<TreatmentDTO> findAllTreatmentsOnCurrentDay(final Integer pageNumber,
                                                            final int pageSize,
                                                            final DateTime localDate) {
        int firstResult = 0;
        int maxResult = 9;
        if (pageNumber == null || pageNumber == 1) {
            return getTreatmentDTOList(treatmentDAO.findAllTreatmentsOnCurrentDay(firstResult, maxResult, localDate));
        } else {
            firstResult = pageSize * (pageNumber - 1);
            maxResult = firstResult + 9;
            return getTreatmentDTOList(treatmentDAO.findAllTreatmentsOnCurrentDay(firstResult, maxResult, localDate));
        }
    }

    @Override
    @Transactional
    public List<TreatmentDTO> findAllTreatmentsOnNextHour(final Integer pageNumber,
                                                          final int pageSize,
                                                          final DateTime localDate) {
        int firstResult = 0;
        int maxResult = 9;
        if (pageNumber == null || pageNumber == 1) {
            return getTreatmentDTOList(treatmentDAO.findAllTreatmentsOnNextHour(firstResult, maxResult, localDate));
        } else {
            firstResult = pageSize * (pageNumber - 1);
            maxResult = firstResult + 9;
            return getTreatmentDTOList(treatmentDAO.findAllTreatmentsOnNextHour(firstResult, maxResult, localDate));
        }
    }

    @Override
    @Transactional
    public List<TreatmentDTO> findAllTreatmentsByPage(final Integer pageNumber, final int pageSize) {
        int firstSearchingResult = 1;
        if (pageNumber == null || pageNumber == 1) {
            return getTreatmentDTOList(treatmentDAO.findAllTreatmentsByPage(firstSearchingResult, pageSize));
        } else {
            firstSearchingResult = (pageNumber - 1) * pageSize + 1;
            return getTreatmentDTOList(treatmentDAO.findAllTreatmentsByPage(firstSearchingResult, pageSize));
        }
    }

    @Override
    @Transactional
    public List<TreatmentDTO> findAllCanceledOrDoneTreatmentsByPage(final int flag, final Integer pageNumber, final int pageSize) {
        int firstSearchResult = 1;
        if (pageNumber == null || pageNumber == 1) {
            return getTreatmentDTOList(treatmentDAO.findAllCanceledOrDoneTreatmentsByPage(flag, firstSearchResult, pageSize));
        } else {
            firstSearchResult = (pageNumber - 1) * pageSize + 1;
            return getTreatmentDTOList(treatmentDAO.findAllCanceledOrDoneTreatmentsByPage(flag, firstSearchResult, pageSize));
        }
    }

    @Override
    @Transactional
    public void moveToCompleted(final long treatmentId) {
        Treatment treatment = treatmentDAO.findTreatmentById(treatmentId);
        treatment.setStatusOfTreatment(StatusOfTreatment.DONE);
        treatmentDAO.updateTreatment(treatment);
    }

    @Override
    @Transactional
    public void moveToCanceled(final TreatmentDTO treatmentDTO) {
        Treatment treatment = treatmentDAO.findTreatmentById(treatmentDTO.getIdTreatment());
        treatment.setStatusOfTreatment(StatusOfTreatment.CANCELED);
        treatment.setCause(treatmentDTO.getCause());
        treatmentDAO.updateTreatment(treatment);
    }

    ///////////////////////////////////////////Auxiliary methods area///////////////////////////////////////////////////

    private DateTime buildDate(final PrescriptionDTO prescriptionDTO) {
        return new DateTime(prescriptionDTO.getStartTime().toLocalDate().year().get(),
                prescriptionDTO.getStartTime().toLocalDate().monthOfYear().get(),
                prescriptionDTO.getStartTime().toLocalDate().dayOfMonth().get(),
                8, 0, 0, 0);
    }

    private ProcAndMed getProcedureOrMedicament(final PrescriptionDTO prescriptionDTO) {
        if (prescriptionDTO.getProcedureName() != null) {
            return procAndMedDAO.findProcedureByName(prescriptionDTO.getProcedureName());
        } else {
            return procAndMedDAO.findMedicamentByName(prescriptionDTO.getMedicamentName());
        }
    }

    private Treatment buildTreatment(final DateTime dateTime, final String dose) {
        Treatment treatment = new Treatment();
        treatment.setEnabled(true);
        treatment.setStatusOfTreatment(StatusOfTreatment.PLANNED);
        treatment.setDateTime(dateTime);
        treatment.setDose(dose);
        return treatment;
    }

    private int getTreatmentsCounter(final int numberOfTreatments,
                                     final PrescriptionDTO prescriptionDTO) {
        int treatmentsCounter;
        if (prescriptionDTO.getWeekTimePattern() != null) {
            if (numberOfTreatments <= 7) {
                treatmentsCounter = Integer.parseInt(prescriptionDTO.getToWeekTimePatternConvert())
                        * Integer.parseInt(prescriptionDTO.getToDayTimePatternConvert());
            } else if (numberOfTreatments % 7 == 0) {
                treatmentsCounter = numberOfTreatments / 7
                        * Integer.parseInt(prescriptionDTO.getToWeekTimePatternConvert())
                        * Integer.parseInt(prescriptionDTO.getToDayTimePatternConvert());
            } else {
                treatmentsCounter = numberOfTreatments / 7
                        * Integer.parseInt(prescriptionDTO.getToWeekTimePatternConvert())
                        * Integer.parseInt(prescriptionDTO.getToDayTimePatternConvert())
                        + ((numberOfTreatments - 7) * Integer.parseInt(prescriptionDTO.getToDayTimePatternConvert()));
            }
        } else {
            treatmentsCounter = numberOfTreatments;
        }
        return treatmentsCounter;
    }

    private void makeTreatment(final DateTime dateTime,
                               final Set<Long> treatmentsId,
                               final ProcAndMed procAndMed,
                               final String dose) {
        if (dateTime.isBeforeNow()) {
            return;
        }
        Treatment treatment = buildTreatment(dateTime, dose);
        long id = treatmentDAO.createTreatment(treatment);
        treatmentsId.add(id);
        treatment.getProcAndMeds().add(procAndMed);
        procAndMed.getTreatment().add(treatment);
        treatmentDAO.updateTreatment(treatment);
        procAndMedDAO.updateProcAndMed(procAndMed);
    }

    private List<Integer> findIntervalsForDays(final int numberOfGaps) {
        List<Integer> intervals = new ArrayList<>();
        switch (numberOfGaps) {
            case 0:
                intervals.add(1);
                intervals.add(1);
                break;
            case 1:
                intervals.add(4);
                intervals.add(3);
                break;
            case 2:
                intervals.add(2);
                intervals.add(3);
                break;
            case 3:
                intervals.add(2);
                intervals.add(1);
                break;
            case 4:
                intervals.add(1);
                intervals.add(3);
                break;
            case 5:
                intervals.add(1);
                intervals.add(2);
                break;
            case 6:
                intervals.add(1);
                intervals.add(1);
        }
        return intervals;
    }

    private int findNumberOfGaps(final PrescriptionDTO prescriptionDTO) {
        int numberOfGaps = 0;
        if (prescriptionDTO.getWeekTimePattern().equals(WeekTimePattern.ONCE_A_WEEK)) {
            return numberOfGaps;
        } else {
            switch (prescriptionDTO.getWeekTimePattern()) {
                case TWICE_A_WEEK:
                    numberOfGaps = 1;
                    break;
                case THREE_TIMES_A_WEEK:
                    numberOfGaps = 2;
                    break;
                case FOUR_TIMES_A_WEEK:
                    numberOfGaps = 3;
                    break;
                case FIVE_TIMES_A_WEEK:
                    numberOfGaps = 4;
                    break;
                case SIX_TIMES_A_WEEK:
                    numberOfGaps = 5;
                    break;
                case SEVEN_TIMES_A_WEEK:
                    numberOfGaps = 6;
                    break;
            }
        }
        return numberOfGaps;
    }

    private List<Integer> findIntervalForDaysAndWeeks(final int numberOfGaps) {
        List<Integer> intervals = new ArrayList<>();
        switch (numberOfGaps) {
            case 0:
                intervals.add(6);
                intervals.add(0);
                intervals.add(14);
                break;
            case 1:
                intervals.add(3);
                intervals.add(2);
                intervals.add(14);
                break;
            case 2:
                intervals.add(1);
                intervals.add(2);
                intervals.add(14);
                break;
            case 3:
                intervals.add(1);
                intervals.add(0);
                intervals.add(14);
                break;
            case 4:
                intervals.add(0);
                intervals.add(2);
                intervals.add(14);
                break;
            case 5:
                intervals.add(0);
                intervals.add(1);
                intervals.add(14);
                break;
            case 6:
                intervals.add(0);
                intervals.add(0);
                intervals.add(14);
        }
        return intervals;
    }

    private Set<Long> getTreatments(int localCounter, final int treatmentsCounter,
                                    final int numberOfLoops, int countOfWeeks,
                                    final int numberOfGaps, DateTime dateTime,
                                    final Set<Long> treatmentsId, final ProcAndMed procAndMed,
                                    final PrescriptionDTO prescriptionDTO, final List<Integer> intervals,
                                    final int flag) {
        DateTime tempDate;
        int firstDayInterval = intervals.get(0);
        int secondDayInterval = intervals.get(1);
        int firstHourInterval = intervals.get(2);
        int commonHours = 0;
        int secondaryCommonHours = 0;
        if (flag == 2) {
            commonHours = 10;
        }
        if (flag == 3) {
            commonHours = 5;
        }
        if (flag == 4) {
            commonHours = 3;
            secondaryCommonHours = 4;
        }
        if (flag == 5) {
            commonHours = 3;
            secondaryCommonHours = 2;
        }
        for (int i = 0; i < treatmentsCounter; i++) {
            if (dateTime.isAfter(prescriptionDTO.getEndTime())) {
                break;
            } else {
                if (localCounter < numberOfLoops) {
                    makeTreatment(dateTime, treatmentsId, procAndMed, prescriptionDTO.getDose());
                    if (flag == 4) {
                        if (localCounter < 2) {
                            tempDate = dateTime.plusHours(commonHours);
                            dateTime = tempDate;
                            localCounter++;
                        } else if (localCounter == 2) {
                            tempDate = dateTime.plusHours(secondaryCommonHours);
                            dateTime = tempDate;
                            localCounter++;
                        }
                    } else if (flag == 5) {
                        if (localCounter < 2) {
                            tempDate = dateTime.plusHours(commonHours);
                            dateTime = tempDate;
                            localCounter++;
                        } else if (localCounter < 4) {
                            tempDate = dateTime.plusHours(secondaryCommonHours);
                            dateTime = tempDate;
                            localCounter++;
                        } else if (localCounter == 4) {
                            tempDate = dateTime.plusHours(firstHourInterval);
                            dateTime = tempDate;
                            localCounter++;
                        }
                    } else {
                        tempDate = dateTime.plusHours(commonHours);
                        dateTime = tempDate;
                        localCounter++;
                    }
                } else {
                    if (prescriptionDTO.getWeekTimePattern().equals(WeekTimePattern.ONCE_A_WEEK)) {
                        makeTreatment(dateTime, treatmentsId, procAndMed, prescriptionDTO.getDose());
                        tempDate = dateTime.plusDays(firstDayInterval).plusHours(firstHourInterval);
                        dateTime = tempDate;
                        localCounter = 0;
                    } else {
                        makeTreatment(dateTime, treatmentsId, procAndMed, prescriptionDTO.getDose());
                        if (countOfWeeks < numberOfGaps) {
                            tempDate = dateTime.plusDays(firstDayInterval).plusHours(firstHourInterval);
                            countOfWeeks++;
                        } else {
                            tempDate = dateTime.plusDays(secondDayInterval).plusHours(firstHourInterval);
                            countOfWeeks = 0;
                        }
                        dateTime = tempDate;
                        localCounter = 0;
                    }
                }
            }
        }
        return treatmentsId;
    }

    private TreatmentGSON getTreatmentGSON(Treatment treatment) {
        TreatmentGSON treatmentGSON = new TreatmentGSON();
        treatmentGSON.setId(treatment.getIdTreatment());
        treatmentGSON.setPatientFirstName(treatment.getPatient().getFirstName());
        treatmentGSON.setPatientLastName(treatment.getPatient().getLastName());
        treatmentGSON.setStatus(convertStatusOfTreatmentToString(treatment.getStatusOfTreatment()));
        treatmentGSON.setDate(treatment.getDateTime().toString("dd.MM.yyyy HH:mm"));
        treatmentGSON.setCause(treatment.getCause());
        return treatmentGSON;
    }

    private List<TreatmentDTO> getTreatmentDTOList(final List<Treatment> treatments) {
        List<TreatmentDTO> treatmentDTOList = new ArrayList<>();
        for (Treatment treatment : treatments) {
            TreatmentDTO treatmentDTO = new TreatmentDTO();
            treatmentDTO.setIdTreatment(treatment.getIdTreatment());
            treatmentDTO.setFirstNamePatient(treatment.getPatient().getFirstName());
            treatmentDTO.setLastNamePatient(treatment.getPatient().getLastName());
            treatmentDTO.setDate(treatment.getDateTime().toString("dd.MM.yyyy HH:mm"));
            treatmentDTO.setStatus(convertStatusOfTreatmentToString(treatment.getStatusOfTreatment()));
            if (treatment.getDose() != null) {
                treatmentDTO.setDose(treatment.getDose());
            } else {
                treatmentDTO.setDose("-");
            }
            treatmentDTO.setTreatmentName(treatment.getPrescription().getPrescriptionName());
            treatmentDTOList.add(treatmentDTO);
        }
        return treatmentDTOList;
    }

    private String convertStatusOfTreatmentToString(final StatusOfTreatment statusOfTreatment) {
        String statusOfTreatmentString = "";
        switch (statusOfTreatment) {
            case CANCELED:
                statusOfTreatmentString = "Отменено";
                break;
            case PLANNED:
                statusOfTreatmentString = "Запланировано";
                break;
            case DONE:
                statusOfTreatmentString = "Выполнено";
                break;
        }
        return statusOfTreatmentString;
    }
}