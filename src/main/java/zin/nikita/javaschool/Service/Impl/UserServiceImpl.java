package zin.nikita.javaschool.Service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.DAO.UserDAO;
import zin.nikita.javaschool.DTO.UserDTO;
import zin.nikita.javaschool.Entity.User;
import zin.nikita.javaschool.Service.UserService;
import zin.nikita.javaschool.Validation.LoginExistsException;

@Service
public class UserServiceImpl implements UserService {

    private UserDAO userDAO;

    private PasswordEncoder passwordEncoder;

    @Autowired
    private void setPasswordEncoder(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    private void setUserDAO(final UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    @Transactional
    public UserDTO createUser(final UserDTO userDTO) throws LoginExistsException {
        if (loginExists(userDTO.getLogin())) {
            throw new LoginExistsException("User with login - " + userDTO.getLogin() + " already exist");
        }
        User user = new User();
        user.setLogin(userDTO.getLogin());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setRole(userDTO.getRole());
        user.setEnabled(true);
        if (userDAO.createUser(user)) {
            return userDTO;
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public UserDTO createFirstUser(final UserDTO userDTO) throws LoginExistsException {
        if (loginExists(userDTO.getLogin())) {
            throw new LoginExistsException("User with login - " + userDTO.getLogin() + " already exists");
        }
        User user = new User();
        user.setLogin(userDTO.getLogin());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setRole(userDTO.getRole());
        user.setEnabled(true);
        if (userDAO.createUser(user)) {
            return userDTO;
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public User findUserByLogin(final String login) {
        return userDAO.findUserByLogin(login);
    }

    @Override
    @Transactional
    public String deleteUserByLogin(final String login) {
        return userDAO.deleteUserByLogin(login);
    }

    private boolean loginExists(final String login) {
        User user = findUserByLogin(login);
        return user != null;
    }
}
