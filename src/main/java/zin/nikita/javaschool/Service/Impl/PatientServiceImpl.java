package zin.nikita.javaschool.Service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.DAO.DoctorDAO;
import zin.nikita.javaschool.DAO.NurseDAO;
import zin.nikita.javaschool.DAO.PatientDAO;
import zin.nikita.javaschool.DTO.DoctorDTO;
import zin.nikita.javaschool.DTO.PatientDTO;
import zin.nikita.javaschool.DTO.PrescriptionDTO;
import zin.nikita.javaschool.DTO.TreatmentDTO;
import zin.nikita.javaschool.Entity.Doctor;
import zin.nikita.javaschool.Entity.Enums.StatusOfPatient;
import zin.nikita.javaschool.Entity.Enums.StatusOfTreatment;
import zin.nikita.javaschool.Entity.Nurse;
import zin.nikita.javaschool.Entity.Patient;
import zin.nikita.javaschool.Entity.Prescription;
import zin.nikita.javaschool.Entity.Treatment;
import zin.nikita.javaschool.PDF.PatientPDF;
import zin.nikita.javaschool.PDF.PrescriptionPDF;
import zin.nikita.javaschool.PDF.TreatmentPDF;
import zin.nikita.javaschool.Service.PatientService;
import zin.nikita.javaschool.Validation.InsuranceExistsException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    private PatientDAO patientDAO;

    private DoctorDAO doctorDAO;

    private NurseDAO nurseDAO;

    @Autowired
    private void setPatientDAO(PatientDAO patientDAO) {
        this.patientDAO = patientDAO;
    }

    @Autowired
    private void setDoctorDAO(DoctorDAO doctorDAO) {
        this.doctorDAO = doctorDAO;
    }

    @Autowired
    private void setNurseDAO(NurseDAO nurseDAO) {
        this.nurseDAO = nurseDAO;
    }

    @Override
    @Transactional
    public PatientDTO createPatient(final PatientDTO patientDTO, final DoctorDTO doctorDTO)
            throws InsuranceExistsException {
        if (insuranceExists(patientDTO) != null) {
            throw new InsuranceExistsException("Patient with insurance: "
                    + patientDTO.getInsurance() + " already exists");
        }
        Patient patient = new Patient();
        patient.setFirstName(patientDTO.getFirstName());
        patient.setLastName(patientDTO.getLastName());
        patient.setInsurance(patientDTO.getInsurance());
        patient.setDiagnosis(patientDTO.getDiagnosis());
        patient.setStatusOfPatient(StatusOfPatient.TREATMENT);
        patient.setPrescriptions(null);
        patient.setEnabled(true);
        if (patientDAO.createPatient(patient)) {
            patient = patientDAO.findPatientByInsurance(patientDTO.getInsurance());
            Doctor doctor = doctorDAO.findDoctorByLogin(doctorDTO.getLogin());
            doctor.getPatients().add(patient);
            patient.setDoctor(doctor);
            patientDAO.updatePatient(patient);
            doctorDAO.updateDoctor(doctor);
            patientDTO.setIdPatient(patient.getIdPatient());
            patientDTO.setStatusOfPatient(patient.getStatusOfPatient());
            return patientDTO;
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public Patient findPatientByInsurance(final String insurance) {
        return patientDAO.findPatientByInsurance(insurance);
    }

    @Override
    @Transactional
    public List<Patient> findAllPatients() {
        return patientDAO.findAllPatients();
    }

    @Override
    @Transactional
    public PatientDTO findAllPatientTreatments(final String insurance) {
        Patient patient = patientDAO.findPatientByInsurance(insurance);
        PatientDTO patientDTO = new PatientDTO();
        for (Treatment treatment : patient.getTreatments()) {
            if (treatment.getStatusOfTreatment().equals(StatusOfTreatment.PLANNED)) {
                TreatmentDTO treatmentDTO = new TreatmentDTO();
                treatmentDTO.setIdTreatment(treatment.getIdTreatment());
                treatmentDTO.setFirstNamePatient(patient.getFirstName());
                treatmentDTO.setLastNamePatient(patient.getLastName());
                treatmentDTO.setTreatmentName(treatment.getPrescription().getPrescriptionName());
                if (treatment.getDose() != null) {
                    treatmentDTO.setDose(treatment.getDose());
                } else {
                    treatmentDTO.setDose("-");
                }
                treatmentDTO.setStatus(convertStatusOfTreatmentToString(treatment.getStatusOfTreatment()));
                treatmentDTO.setDate(treatment.getDateTime().toString("dd.MM.yyyy HH:mm"));
                patientDTO.getTreatments().add(treatmentDTO);
            }
        }
        patientDTO.setInsurance(patient.getInsurance());
        return patientDTO;
    }

    @Override
    @Transactional
    public List<PrescriptionDTO> findPatientPrescriptions(final String insurance) {
        Patient patient = patientDAO.findPatientByInsurance(insurance);
        PatientDTO patientDTO = new PatientDTO();
        for (Prescription prescription : patient.getPrescriptions()) {
            if (prescription.isEnabled()) {
                PrescriptionDTO prescriptionDTO = setPrescriptionDTO(prescription);
                patientDTO.getPrescriptionDTOS().add(prescriptionDTO);
            }
        }
        return patientDTO.getPrescriptionDTOS();
    }

    @Override
    @Transactional
    public PatientDTO findAllPatientPrescriptions(final String insurance) {
        Patient patient = patientDAO.findPatientByInsurance(insurance);
        PatientDTO patientDTO = new PatientDTO();
        for (Prescription prescription : patient.getPrescriptions()) {
            if (prescription.isEnabled()) {
                PrescriptionDTO prescriptionDTO = setPrescriptionDTO(prescription);
                prescriptionDTO.setPatientFirsName(patient.getFirstName());
                prescriptionDTO.setPatientLastName(patient.getLastName());
                patientDTO.getPrescriptionDTOS().add(prescriptionDTO);
            }
        }
        patientDTO.setInsurance(patient.getInsurance());
        patientDTO.setFirstName(patient.getFirstName());
        patientDTO.setLastName(patient.getLastName());
        return patientDTO;
    }

    @Override
    @Transactional
    public PatientPDF findPatientForPDFpage(String insurance) {
        Patient patient = patientDAO.findPatientByInsurance(insurance);
        return createPatientPDF(patient);
    }

    @Override
    @Transactional
    public void addNurses(final PatientDTO patientDTO) {
        Patient patient = patientDAO.findPatientByInsurance(patientDTO.getInsurance());
        List<Nurse> nurseList = nurseDAO.findAllNurses();
        for (Nurse nurse : nurseList) {
            nurse.getPatients().add(patient);
        }

        patient.setNurses(nurseList);
        for (Nurse nurse : nurseList) {
            nurseDAO.updateNurse(nurse);
        }
        patientDAO.updatePatient(patient);
    }

    @Override
    @Transactional
    public PatientDTO dischargePatient(final PatientDTO patientDTO) {
        Patient patient = patientDAO.findPatientByInsurance(patientDTO.getInsurance());
        for (Treatment treatment : patient.getTreatments()) {
            if (treatment.getStatusOfTreatment().equals(StatusOfTreatment.PLANNED)) {
                treatment.setStatusOfTreatment(StatusOfTreatment.CANCELED);
            }
        }
        patient.setStatusOfPatient(StatusOfPatient.DISCHARGED);
        patientDAO.updatePatient(patient);
        if (patientDAO.findPatientByInsurance(patientDTO.getInsurance())
                .getStatusOfPatient().equals(StatusOfPatient.DISCHARGED)) {
            patientDTO.setDischarged(true);
            return patientDTO;
        } else {
            return patientDTO;
        }
    }

    @Override
    @Transactional
    public void updateStatusOfPatient(final PatientDTO patientDTO) {
        Patient patient = patientDAO.findPatientByInsurance(patientDTO.getInsurance());
        patient.setStatusOfPatient(patientDTO.getStatusOfPatient());
        patient.setDiagnosis(patientDTO.getDiagnosis());
        patientDAO.updatePatient(patient);
    }

    ///////////////////////////////////////////Auxiliary methods area///////////////////////////////////////////////////

    private PatientDTO insuranceExists(final PatientDTO patientDTO) {
        Patient patient = findPatientByInsurance(patientDTO.getInsurance());
        if (patient == null) {
            return null;
        } else {
            patientDTO.setStatusOfPatient(patient.getStatusOfPatient());
            return patientDTO;
        }
    }

    private PatientPDF createPatientPDF(final Patient patient) {
        PatientPDF patientPDF = new PatientPDF();
        HashMap<Long, List<TreatmentPDF>> treatmentsMap = new HashMap<>();
        HashMap<Long, List<PrescriptionPDF>> prescriptionMap = new HashMap<>();
        patientPDF.setFirstName(patient.getFirstName());
        patientPDF.setLastName(patient.getLastName());
        patientPDF.setInsurance(patient.getInsurance());
        patientPDF.setDoctorFirstName(patient.getDoctor().getFirstName());
        patientPDF.setDoctorLastName(patient.getDoctor().getLastName());
        patientPDF.setDoctorSpecialization(patient.getDoctor().getSpecialization());
        createPrescriptionsAndTreatments(patientPDF, patient, treatmentsMap, prescriptionMap);
        patientPDF.setPrescriptionsMap(prescriptionMap);
        patientPDF.setTreatmentsMap(treatmentsMap);
        patientPDF.setStatusOfPatient(patient.getStatusOfPatient());
        return patientPDF;
    }

    private void createPrescriptionsAndTreatments(final PatientPDF patientPDF,
                                                  final Patient patient,
                                                  final HashMap<Long, List<TreatmentPDF>> treatmentsMap,
                                                  final HashMap<Long, List<PrescriptionPDF>> prescriptionMap) {
        for (Prescription prescription : patient.getPrescriptions()) {
            patientPDF.setPrescriptionId(prescription.getIdPrescription());
            List<TreatmentPDF> treatmentPDFList = new ArrayList<>();
            List<PrescriptionPDF> prescriptionPDFList = new ArrayList<>();
            PrescriptionPDF prescriptionPDF = new PrescriptionPDF();
            prescriptionPDF.setPrescriptionName(prescription.getPrescriptionName());
            prescriptionPDF.setStartDate(prescription.getStartTime().toString("dd.MM.yyyy"));
            prescriptionPDF.setEndDate(prescription.getEndTime().toString("dd.MM.yyyy"));
            prescriptionPDF.setDayTimePattern(prescription.getDayTimePattern());
            prescriptionPDF.setWeekTimePattern(prescription.getWeekTimePattern());
            prescriptionPDFList.add(prescriptionPDF);
            for (Treatment treatment : prescription.getTreatments()) {
                if (treatment.getCause() == null ||
                        !treatment.getCause().equals("Prescription changed")) {
                    TreatmentPDF treatmentPDF = new TreatmentPDF();
                    treatmentPDF.setId(treatment.getIdTreatment());
                    treatmentPDF.setDate(treatment.getDateTime().toString("dd.MM.yyyy HH:mm"));
                    treatmentPDF.setStatusOfTreatment(treatment.getStatusOfTreatment());
                    treatmentPDFList.add(treatmentPDF);
                }
            }
            prescriptionMap.put(prescription.getIdPrescription(), prescriptionPDFList);
            treatmentsMap.put(prescription.getIdPrescription(), treatmentPDFList);
        }
    }

    private PrescriptionDTO setPrescriptionDTO(final Prescription prescription) {
        PrescriptionDTO prescriptionDTO = new PrescriptionDTO();
        prescriptionDTO.setIdPrescription(prescription.getIdPrescription());
        prescriptionDTO.setDayTimePattern(prescription.getDayTimePattern());
        prescriptionDTO.setWeekTimePattern(prescription.getWeekTimePattern());
        prescriptionDTO.setStartTime(prescription.getStartTime());
        prescriptionDTO.setEndTime(prescription.getEndTime());
        prescriptionDTO.setDose(prescription.getDose());
        prescriptionDTO.setProcedureName(prescription.getPrescriptionName());
        return prescriptionDTO;
    }

    private String convertStatusOfTreatmentToString(final StatusOfTreatment statusOfTreatment) {
        String statusOfTreatmentString = "";
        switch (statusOfTreatment) {
            case CANCELED:
                statusOfTreatmentString = "Отменено";
                break;
            case PLANNED:
                statusOfTreatmentString = "Запланировано";
                break;
            case DONE:
                statusOfTreatmentString = "Выполнено";
                break;
        }
        return statusOfTreatmentString;
    }
}
