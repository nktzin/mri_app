package zin.nikita.javaschool.Service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.DAO.AdminDAO;
import zin.nikita.javaschool.DAO.DoctorDAO;
import zin.nikita.javaschool.DAO.PatientDAO;
import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.DTO.DoctorDTO;
import zin.nikita.javaschool.DTO.PatientDTO;
import zin.nikita.javaschool.DTO.PrescriptionDTO;
import zin.nikita.javaschool.Entity.Admin;
import zin.nikita.javaschool.Entity.Doctor;
import zin.nikita.javaschool.Entity.Enums.StatusOfPatient;
import zin.nikita.javaschool.Entity.Patient;
import zin.nikita.javaschool.Entity.Prescription;
import zin.nikita.javaschool.Service.DoctorService;

import java.util.ArrayList;
import java.util.List;

@Service
public class DoctorServiceImpl implements DoctorService {

    private DoctorDAO doctorDAO;

    private AdminDAO adminDAO;

    @Autowired
    private void setDoctorDAO(final DoctorDAO doctorDAO) {
        this.doctorDAO = doctorDAO;
    }

    @Autowired
    private void setAdminDAO(final AdminDAO adminDAO) {
        this.adminDAO = adminDAO;
    }

    @Override
    @Transactional
    public void createDoctor(final DoctorDTO doctorDTO, final AdminDTO adminDTO) {
        Doctor doctor = new Doctor();
        Admin admin = adminDAO.findAdminByLogin(adminDTO.getLogin());
        doctor.setFirstName(doctorDTO.getFirstName());
        doctor.setLastName(doctorDTO.getLastName());
        doctor.setLogin(doctorDTO.getLogin());
        doctor.setSpecialization(doctorDTO.getSpecialization());
        doctor.setEnabled(doctorDTO.isEnabled());
        doctor.setAdmin(admin);
        doctorDAO.createDoctor(doctor);
        doctor = doctorDAO.findDoctorByLogin(doctor.getLogin());
        admin.getDoctors().add(doctor);
        adminDAO.updateAdmin(admin);
    }

    @Override
    @Transactional
    public DoctorDTO findDoctorDTOByLogin(final String login) {
        Doctor doctor = doctorDAO.findDoctorByLogin(login);
        DoctorDTO doctorDTO = new DoctorDTO();
        doctorDTO.setIdDoctor(doctor.getIdDoctor());
        doctorDTO.setFirstName(doctor.getFirstName());
        doctorDTO.setLastName(doctor.getLastName());
        doctorDTO.setLogin(doctor.getLogin());
        doctorDTO.setSpecialization(doctor.getSpecialization());
        doctorDTO.setEnabled(doctor.isEnabled());

        return doctorDTO;
    }

    @Override
    @Transactional
    public Doctor findDoctorByLogin(final String login) {
        return doctorDAO.findDoctorByLogin(login);
    }

    @Override
    @Transactional
    public List<DoctorDTO> findAllDoctorsDTO() {
        List<Doctor> doctors = doctorDAO.findAllDoctors();
        List<DoctorDTO> doctorDTOList = new ArrayList<>();
        for (Doctor doctor : doctors) {
            DoctorDTO doctorDTO = new DoctorDTO();
            doctorDTO.setFirstName(doctor.getFirstName());
            doctorDTO.setLastName(doctor.getLastName());
            doctorDTO.setSpecialization(doctor.getSpecialization());
            doctorDTO.setLogin(doctor.getLogin());
            doctorDTOList.add(doctorDTO);
        }
        return doctorDTOList;
    }

    @Override
    @Transactional
    public int findNumberOfPatients(final String login) {
        Doctor doctor = doctorDAO.findDoctorByLogin(login);
        int numberOfPatients = 0;
        for (Patient patient : doctor.getPatients()) {
            if (patient.getStatusOfPatient().equals(StatusOfPatient.TREATMENT)) {
                numberOfPatients++;
            }
        }
        return numberOfPatients;
    }

    @Override
    @Transactional
    public List<PatientDTO> findInfoAboutPatients(final String login) {
        Doctor doctor = doctorDAO.findDoctorByLogin(login);
        List<PatientDTO> patientDTOS = new ArrayList<>();
        for (Patient patient : doctor.getPatients()) {
            if (patient.getStatusOfPatient().equals(StatusOfPatient.TREATMENT)) {
                PatientDTO patientDTO = new PatientDTO();
                patientDTO.setFirstName(patient.getFirstName());
                patientDTO.setLastName(patient.getLastName());
                patientDTO.setDiagnosis(patient.getDiagnosis());
                for (Prescription prescription : patient.getPrescriptions()) {
                    if (prescription.isEnabled()) {
                        PrescriptionDTO prescriptionDTO = new PrescriptionDTO();
                        prescriptionDTO.setProcedureName(prescription.getPrescriptionName());
                        patientDTO.getPrescriptionDTOS().add(prescriptionDTO);
                    }
                }
                patientDTOS.add(patientDTO);
            }
        }
        return patientDTOS;
    }

    @Override
    @Transactional
    public void updateDoctor(final Doctor doctor) {
        doctorDAO.updateDoctor(doctor);
    }

    @Override
    @Transactional
    public void deleteDoctorByLogin(final String login) {
        doctorDAO.deleteDoctorByLogin(login);
    }
}