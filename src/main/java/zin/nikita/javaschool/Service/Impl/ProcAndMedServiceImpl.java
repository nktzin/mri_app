package zin.nikita.javaschool.Service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.DAO.ProcAndMedDAO;
import zin.nikita.javaschool.Entity.ProcAndMed;
import zin.nikita.javaschool.Service.ProcAndMedService;

import java.util.List;

@Service
public class ProcAndMedServiceImpl implements ProcAndMedService {

    private ProcAndMedDAO procAndMedDAO;

    @Autowired
    private void setProcAndMedDAO(final ProcAndMedDAO procAndMedDAO) {
        this.procAndMedDAO = procAndMedDAO;
    }

    @Override
    @Transactional
    public List<ProcAndMed> findAllProcedures() {
        return procAndMedDAO.findAllProcedures();
    }

    @Override
    @Transactional
    public List<ProcAndMed> findAllMedicaments() {
        return procAndMedDAO.findAllMedicaments();
    }
}
