package zin.nikita.javaschool.Service.Impl;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.DAO.DoctorDAO;
import zin.nikita.javaschool.DAO.PatientDAO;
import zin.nikita.javaschool.DAO.PrescriptionDAO;
import zin.nikita.javaschool.DAO.ProcAndMedDAO;
import zin.nikita.javaschool.DTO.PrescriptionDTO;
import zin.nikita.javaschool.Entity.Doctor;
import zin.nikita.javaschool.Entity.Enums.ProcedureOrMedicament;
import zin.nikita.javaschool.Entity.Enums.StatusOfTreatment;
import zin.nikita.javaschool.Entity.Patient;
import zin.nikita.javaschool.Entity.Prescription;
import zin.nikita.javaschool.Entity.ProcAndMed;
import zin.nikita.javaschool.Entity.Treatment;
import zin.nikita.javaschool.JMS.Sender;
import zin.nikita.javaschool.Service.PrescriptionService;

@Service
public class PrescriptionServiceImpl implements PrescriptionService {

    private static String dd_MM_yyyy = "dd.MM.yyyy";

    private PrescriptionDAO prescriptionDAO;

    private DoctorDAO doctorDAO;

    private PatientDAO patientDAO;

    private ProcAndMedDAO procAndMedDAO;

    private Sender sender;

    @Autowired
    private void setPrescriptionDAO(PrescriptionDAO prescriptionDAO) {
        this.prescriptionDAO = prescriptionDAO;
    }

    @Autowired
    private void setDoctorDAO(DoctorDAO doctorDAO) {
        this.doctorDAO = doctorDAO;
    }

    @Autowired
    private void setPatientDAO(PatientDAO patientDAO) {
        this.patientDAO = patientDAO;
    }

    @Autowired
    private void setProcAndMedDAO(ProcAndMedDAO procAndMedDAO) {
        this.procAndMedDAO = procAndMedDAO;
    }

    @Autowired
    private void setSender(Sender sender) {
        this.sender = sender;
    }

    @Override
    @Transactional
    public PrescriptionDTO createPrescription(final PrescriptionDTO prescriptionDTO) {
        Prescription prescription = new Prescription();
        prescription.setDayTimePattern(prescriptionDTO.getDayTimePattern());
        prescription.setWeekTimePattern(prescriptionDTO.getWeekTimePattern());
        prescription.setStartTime(prescriptionDTO.getStartTime());
        prescription.setEndTime(prescriptionDTO.getEndTime());
        prescription.setEnabled(true);
        if (prescriptionDTO.getProcedureName() != null) {
            prescription.setPrescriptionName(prescriptionDTO.getProcedureName());
        } else {
            prescription.setPrescriptionName(prescriptionDTO.getMedicamentName());
        }
        if (prescriptionDTO.getDose() != null) {
            prescription.setDose(prescriptionDTO.getDose());
        }
        long id = prescriptionDAO.createPrescription(prescription);
        if (id != 0) {
            prescriptionDTO.setIdPrescription(id);
            if (prescriptionDTO.getProcedureName() != null) {
                ProcAndMed procAndMed = procAndMedDAO.findProcedureByName(prescriptionDTO.getProcedureName());
                prescription = prescriptionDAO.findPrescriptionById(id);
                prescription.getProcAndMeds().add(procAndMed);
                procAndMed.getPrescription().add(prescription);
                prescriptionDAO.updatePrescription(prescription);
                procAndMedDAO.updateProcAndMed(procAndMed);
            } else {
                ProcAndMed procAndMed = procAndMedDAO.findMedicamentByName(prescriptionDTO.getMedicamentName());
                prescription = findPrescriptionById(id);
                prescription.getProcAndMeds().add(procAndMed);
                procAndMed.getPrescription().add(prescription);
                prescriptionDAO.updatePrescription(prescription);
                procAndMedDAO.updateProcAndMed(procAndMed);
            }
            return prescriptionDTO;
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public PrescriptionDTO findPrescriptionDTOById(final long id) {
        Prescription prescription = prescriptionDAO.findPrescriptionById(id);
        PrescriptionDTO prescriptionDTO = new PrescriptionDTO();
        prescriptionDTO.setProcedureName(prescription.getPrescriptionName());
        if (prescription.getDose() != null) {
            prescriptionDTO.setDose(prescription.getDose());
        } else {
            prescriptionDTO.setDose("-");
        }
        prescriptionDTO.setIdPrescription(prescription.getIdPrescription());
        prescriptionDTO.setStartTime(prescription.getStartTime());
        prescriptionDTO.setEndTime(prescription.getEndTime());
        prescriptionDTO.setDayTimePattern(prescription.getDayTimePattern());
        prescriptionDTO.setWeekTimePattern(prescription.getWeekTimePattern());
        return prescriptionDTO;
    }

    @Override
    @Transactional
    public Prescription findPrescriptionById(final long id) {
        return prescriptionDAO.findPrescriptionById(id);
    }

    @Override
    @Transactional
    public void addDoctor(final PrescriptionDTO prescriptionDTO) {
        Doctor doctor = doctorDAO.findDoctorById(prescriptionDTO.getIdDoctor());
        Prescription prescription = prescriptionDAO.findPrescriptionById(prescriptionDTO.getIdPrescription());
        doctor.getPrescriptions().add(prescription);
        prescription.setDoctor(doctor);
        doctorDAO.updateDoctor(doctor);
        prescriptionDAO.updatePrescription(prescription);
    }

    @Override
    @Transactional
    public void addPatient(final PrescriptionDTO prescriptionDTO) {
        Patient patient = patientDAO.findPatientByInsurance(prescriptionDTO.getPatientInsurance());
        Prescription prescription = prescriptionDAO.findPrescriptionById(prescriptionDTO.getIdPrescription());
        patient.getPrescriptions().add(prescription);
        prescription.setPatient(patient);
        patientDAO.updatePatient(patient);
        prescriptionDAO.updatePrescription(prescription);
    }

    @Override
    @Transactional
    public void canceledPrescription(final long prescriptionId) {
        Prescription prescription = prescriptionDAO.findPrescriptionById(prescriptionId);
        DateTime now = DateTime.now();
        for (Treatment treatment : prescription.getTreatments()) {
            if (treatment.getStatusOfTreatment().equals(StatusOfTreatment.PLANNED)) {
                if (treatment.getDateTime().toString(dd_MM_yyyy).equals(now.toString(dd_MM_yyyy))) {
                    sender.sendMessage("Treatments was canceled");
                }
                treatment.setCause("Назначение отменено");
                treatment.setStatusOfTreatment(StatusOfTreatment.CANCELED);
            }
        }
        prescription.setEnabled(false);
        prescriptionDAO.updatePrescription(prescription);
    }

    @Override
    @Transactional
    public void editDose(final long prescriptionId, final String dose) {
        Prescription prescription = prescriptionDAO.findPrescriptionById(prescriptionId);
        DateTime now = DateTime.now();
        for (Treatment treatment : prescription.getTreatments()) {
            if (treatment.getStatusOfTreatment().equals(StatusOfTreatment.PLANNED)) {
                treatment.setDose(dose);
                if (treatment.getDateTime().toString(dd_MM_yyyy).equals(now.toString(dd_MM_yyyy))) {
                    sender.sendMessage("Dose was changed");
                }
            }
        }
        prescriptionDAO.updatePrescription(prescription);
    }

    @Override
    @Transactional
    public void changePrescription(final PrescriptionDTO prescriptionDTO,
                                   final DateTime dateTime) {
        Prescription prescription = findPrescriptionById(prescriptionDTO.getIdPrescription());
        for (Treatment treatment : prescription.getTreatments()) {
            if (treatment.getStatusOfTreatment().equals(StatusOfTreatment.PLANNED)
                    && treatment.getDateTime().isAfter(dateTime)) {
                treatment.setStatusOfTreatment(StatusOfTreatment.CANCELED);
                treatment.setCause("Prescription changed");
            }
        }
        setDayTimePattern(prescriptionDTO, prescription);
        setWeekTimePattern(prescriptionDTO, prescription);
        prescription.setStartTime(prescriptionDTO.getStartTime());
        prescription.setEndTime(prescriptionDTO.getEndTime());
        for (ProcAndMed procAndMed : prescription.getProcAndMeds()) {
            if (procAndMed.getProcedureOrMedicament().equals(ProcedureOrMedicament.MEDICAMENT)) {
                prescriptionDTO.setMedicamentName(procAndMed.getName());
            } else {
                prescriptionDTO.setProcedureName(procAndMed.getName());
            }
        }
        prescriptionDAO.updatePrescription(prescription);
    }

    private void setDayTimePattern(final PrescriptionDTO prescriptionDTO,
                                   final Prescription prescription) {
        if (prescription.getDayTimePattern() == null && prescriptionDTO.getDayTimePattern() == null) {
            return;
        }
        if (prescription.getDayTimePattern() == null && prescriptionDTO.getDayTimePattern() != null) {
            prescription.setDayTimePattern(prescriptionDTO.getDayTimePattern());
            return;
        }
        if (prescription.getDayTimePattern() != null && prescriptionDTO.getDayTimePattern() == null) {
            prescription.setDayTimePattern(null);
            return;
        }
        if (prescription.getDayTimePattern() != null && prescriptionDTO.getDayTimePattern() != null
                && !prescription.getDayTimePattern().equals(prescriptionDTO.getDayTimePattern())) {
            prescription.setDayTimePattern(prescriptionDTO.getDayTimePattern());
        }
    }

    private void setWeekTimePattern(final PrescriptionDTO prescriptionDTO,
                                    final Prescription prescription) {
        if (prescription.getWeekTimePattern() == null && prescriptionDTO.getWeekTimePattern() == null) {
            return;
        }
        if (prescription.getWeekTimePattern() == null && prescriptionDTO.getWeekTimePattern() != null) {
            prescription.setWeekTimePattern(prescriptionDTO.getWeekTimePattern());
            return;
        }
        if (prescription.getWeekTimePattern() != null && prescriptionDTO.getWeekTimePattern() == null) {
            prescription.setWeekTimePattern(null);
            return;
        }
        if (prescription.getWeekTimePattern() != null && prescriptionDTO.getWeekTimePattern() != null
                && !prescription.getWeekTimePattern().equals(prescriptionDTO.getWeekTimePattern())) {
            prescription.setWeekTimePattern(prescriptionDTO.getWeekTimePattern());
        }
    }
}
