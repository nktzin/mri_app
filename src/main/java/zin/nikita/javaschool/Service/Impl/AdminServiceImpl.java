package zin.nikita.javaschool.Service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.DAO.AdminDAO;
import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.Entity.Admin;
import zin.nikita.javaschool.Service.AdminService;

@Service
public class AdminServiceImpl implements AdminService {

    private AdminDAO adminDAO;

    @Autowired
    private void setAdminDao(final AdminDAO adminDAO) {
        this.adminDAO = adminDAO;
    }

    @Override
    @Transactional
    public void createAdmin(final Admin admin) {
        adminDAO.createAdmin(admin);
    }

    @Override
    @Transactional
    public AdminDTO findAdminDTOByLogin(final String login) {
        Admin admin = adminDAO.findAdminByLogin(login);
        AdminDTO adminDTO = new AdminDTO();
        adminDTO.setIdAdmin(admin.getIdAdmin());
        adminDTO.setFirstName(admin.getFirstName());
        adminDTO.setLastName(admin.getLastName());
        adminDTO.setLogin(admin.getLogin());

        return adminDTO;
    }

    @Override
    @Transactional
    public void updateAdmin(final Admin admin) {
        adminDAO.updateAdmin(admin);
    }

    @Override
    @Transactional
    public void deleteAdminByLogin(final String login) {
        adminDAO.deleteAdminByLogin(login);
    }
}
