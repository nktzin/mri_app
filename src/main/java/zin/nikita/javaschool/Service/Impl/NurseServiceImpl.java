package zin.nikita.javaschool.Service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.DAO.AdminDAO;
import zin.nikita.javaschool.DAO.NurseDAO;
import zin.nikita.javaschool.DAO.PatientDAO;
import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.DTO.NurseDTO;
import zin.nikita.javaschool.Entity.Admin;
import zin.nikita.javaschool.Entity.Nurse;
import zin.nikita.javaschool.Entity.Patient;
import zin.nikita.javaschool.Service.NurseService;

import java.util.ArrayList;
import java.util.List;

@Service
public class NurseServiceImpl implements NurseService {

    private NurseDAO nurseDAO;

    private PatientDAO patientDAO;

    private AdminDAO adminDAO;

    @Autowired
    private void setNurseDAO(final NurseDAO nurseDAO) {
        this.nurseDAO = nurseDAO;
    }

    @Autowired
    private void setPatientDAO(final PatientDAO patientDAO) {
        this.patientDAO = patientDAO;
    }

    @Autowired
    private void setAdminDAO(final AdminDAO adminDAO) {
        this.adminDAO = adminDAO;
    }

    @Override
    @Transactional
    public void createNurse(final NurseDTO nurseDTO, final AdminDTO adminDTO) {
        Nurse nurse = new Nurse();
        Admin admin = adminDAO.findAdminByLogin(adminDTO.getLogin());
        nurse.setFirstName(nurseDTO.getFirstName());
        nurse.setLastName(nurseDTO.getLastName());
        nurse.setEnabled(nurseDTO.isEnabled());
        nurse.setLogin(nurseDTO.getLogin());
        nurse.setAdmin(admin);
        nurseDAO.createNurse(nurse);
        nurse = nurseDAO.findNurseByLogin(nurse.getLogin());
        admin.getNurses().add(nurse);
        adminDAO.updateAdmin(admin);
        List<Patient> patients = patientDAO.findAllPatients();
        for (Patient patient : patients) {
            patient.getNurses().add(nurse);
        }
        nurse.setPatients(patients);
        nurseDAO.updateNurse(nurse);
        for (Patient patient : patients) {
            patientDAO.updatePatient(patient);
        }
    }

    @Override
    @Transactional
    public Nurse findNurseById(final long id) {
        return nurseDAO.findNurseById(id);
    }

    @Override
    @Transactional
    public NurseDTO findNurseByLogin(final String login) {
        Nurse nurse = nurseDAO.findNurseByLogin(login);
        NurseDTO nurseDTO = new NurseDTO();
        nurseDTO.setIdNurse(nurse.getIdNurse());
        nurseDTO.setFirstName(nurse.getFirstName());
        nurseDTO.setLastName(nurse.getLastName());
        nurseDTO.setLogin(nurse.getLogin());
        nurseDTO.setEnabled(nurse.isEnabled());
        return nurseDTO;
    }

    @Override
    @Transactional
    public List<NurseDTO> findAllNursesDTO() {
        List<Nurse> nurses = nurseDAO.findAllNurses();
        List<NurseDTO> nurseDTOList = new ArrayList<>();
        for (Nurse nurse : nurses) {
            NurseDTO nurseDTO = new NurseDTO();
            nurseDTO.setFirstName(nurse.getFirstName());
            nurseDTO.setLastName(nurse.getLastName());
            nurseDTO.setLogin(nurse.getLogin());
            nurseDTOList.add(nurseDTO);
        }
        return nurseDTOList;
    }

    @Override
    @Transactional
    public void updateNurse(final Nurse nurse) {
        nurseDAO.updateNurse(nurse);
    }

    @Override
    @Transactional
    public void deleteNurseByLogin(final String login) {
        nurseDAO.deleteNurseByLogin(login);
    }
}
