package zin.nikita.javaschool.Service;

import zin.nikita.javaschool.DTO.UserDTO;
import zin.nikita.javaschool.Entity.User;
import zin.nikita.javaschool.Validation.LoginExistsException;

public interface UserService {

    UserDTO createUser(UserDTO userDTO) throws LoginExistsException;

    UserDTO createFirstUser(UserDTO userDTO) throws LoginExistsException;

    User findUserByLogin(String login);

    String deleteUserByLogin(String login);
}
