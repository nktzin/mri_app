package zin.nikita.javaschool.Service;

import zin.nikita.javaschool.Entity.ProcAndMed;

import java.util.List;

public interface ProcAndMedService {

    List<ProcAndMed> findAllProcedures();

    List<ProcAndMed> findAllMedicaments();
}
