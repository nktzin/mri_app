package zin.nikita.javaschool.Service;

import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.DTO.NurseDTO;
import zin.nikita.javaschool.Entity.Nurse;

import java.util.List;

public interface NurseService {

    void createNurse(NurseDTO nurse, AdminDTO adminDTO);

    Nurse findNurseById(long id);

    NurseDTO findNurseByLogin(String login);

    List<NurseDTO> findAllNursesDTO();

    void updateNurse(Nurse nurse);

    void deleteNurseByLogin(String login);
}
