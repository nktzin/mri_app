package zin.nikita.javaschool.Service;

import org.joda.time.DateTime;
import zin.nikita.javaschool.DTO.PrescriptionDTO;
import zin.nikita.javaschool.DTO.TreatmentDTO;
import zin.nikita.javaschool.GSON.TreatmentGSON;

import java.util.List;
import java.util.Set;

public interface TreatmentService {

    Set<Long> createTreatment(int numberOfTreatments, PrescriptionDTO prescriptionDTO);

    Set<Long> createTreatmentWithTwoDayPattern(int numberOfTreatments, PrescriptionDTO prescriptionDTO);

    Set<Long> createTreatmentWithThreeDayPattern(int numberOfTreatments, PrescriptionDTO prescriptionDTO);

    Set<Long> createTreatmentWithFourDayPattern(int numberOfTreatments, PrescriptionDTO prescriptionDTO);

    Set<Long> createTreatmentWithFiveDayPattern(int numberOfTreatments, PrescriptionDTO prescriptionDTO);

    void addPatient(Set<Long> treatmentsId, PrescriptionDTO prescriptionDTO);

    void addNurse(Set<Long> treatmentsId);

    void addPrescription(Set<Long> treatmentsId, PrescriptionDTO prescriptionDTO);

    List<TreatmentGSON> findTreatmentsGsonOnCurrentDay();

    Integer findAllPlannedTreatments(int flag, DateTime localTime);

    Integer findAllCanceledTreatments();

    Integer findAllDoneTreatments();

    List<TreatmentDTO> findAllTreatmentsOnCurrentDay(Integer numberOfPage, int pageSize, DateTime localDate);

    List<TreatmentDTO> findAllTreatmentsOnNextHour(Integer numberOfPage, int pageSize, DateTime localDate);

    List<TreatmentDTO> findAllTreatmentsByPage(Integer numberOfPage, int pageSize);

    List<TreatmentDTO> findAllCanceledOrDoneTreatmentsByPage(int flag, Integer numberOfPage, int pageSize);

    void moveToCompleted(long treatmentId);

    void moveToCanceled(TreatmentDTO treatmentDTO);
}
