package zin.nikita.javaschool.Service;

import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.DTO.DoctorDTO;
import zin.nikita.javaschool.DTO.PatientDTO;
import zin.nikita.javaschool.Entity.Doctor;

import java.util.List;

public interface DoctorService {

    void createDoctor(DoctorDTO doctor, AdminDTO adminDTO);

    Doctor findDoctorByLogin(String login);

    DoctorDTO findDoctorDTOByLogin(String login);

    List<DoctorDTO> findAllDoctorsDTO();

    int findNumberOfPatients(String login);

    List<PatientDTO> findInfoAboutPatients(String login);

    void updateDoctor(Doctor doctor);

    void deleteDoctorByLogin(String login);
}
