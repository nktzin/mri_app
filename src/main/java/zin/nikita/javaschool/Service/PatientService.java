package zin.nikita.javaschool.Service;

import zin.nikita.javaschool.DTO.DoctorDTO;
import zin.nikita.javaschool.DTO.PatientDTO;
import zin.nikita.javaschool.DTO.PrescriptionDTO;
import zin.nikita.javaschool.Entity.Patient;
import zin.nikita.javaschool.PDF.PatientPDF;
import zin.nikita.javaschool.Validation.InsuranceExistsException;

import java.util.List;

public interface PatientService {

    PatientDTO createPatient(PatientDTO patientDTO, DoctorDTO doctorDTO) throws InsuranceExistsException;

    Patient findPatientByInsurance(String insurance);

    PatientDTO findAllPatientTreatments(String insurance);

    PatientDTO findAllPatientPrescriptions(String insurance);

    PatientPDF findPatientForPDFpage(String insurance);

    List<PrescriptionDTO> findPatientPrescriptions(String insurance);

    List<Patient> findAllPatients();

    void addNurses(PatientDTO patientDTO);

    PatientDTO dischargePatient(PatientDTO patientDTO);

    void updateStatusOfPatient(PatientDTO patientDTO);
}
