package zin.nikita.javaschool.Service;

import org.joda.time.DateTime;
import zin.nikita.javaschool.DTO.PrescriptionDTO;
import zin.nikita.javaschool.Entity.Prescription;

public interface PrescriptionService {

    PrescriptionDTO createPrescription(PrescriptionDTO prescriptionDTO);

    Prescription findPrescriptionById(long id);

    PrescriptionDTO findPrescriptionDTOById(long id);

    void addDoctor(PrescriptionDTO prescriptionDTO);

    void addPatient(PrescriptionDTO prescriptionDTO);

    void canceledPrescription(long prescriptionId);

    void editDose(long prescriptionId, String dose);

    void changePrescription(PrescriptionDTO prescriptionDTO, DateTime dateTime);
}
