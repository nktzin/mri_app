<!DOCTYPE html>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="page" tagdir="/WEB-INF/tags" %>

<page:template>

    <jsp:attribute name="title">Nurse</jsp:attribute>
    <jsp:body>
        <spring:url value="/resources/css/modalWindow.css" var="modal"/>
        <link href="${modal}" rel="stylesheet"/>

        <c:url value="/nurse.html" var="nurse"/>
        <c:url value="/nurse/today" var="onTodayTreatments"/>
        <c:url value="/nurse/nexthour" var="onNextHour"/>
        <c:url value="/nurse/patient" var="findPatient"/>
        <c:url value="/nurse/patient/treatments" var="toTreatment"/>
        <c:url value="/nurse/patient/treatments" var="getTreatments"/>
        <c:url value="/nurse/doneOrCanceled" var="doneOrCanceled"/>
        <c:url value="/nurse/canceled" var="canceled"/>
        <c:url value="/nurse/done" var="done"/>
        <c:url value="http://localhost:8080/elPanel/index.jsf?employee=nurse" var="elPanel"/>

        <%--Page Content--%>
        <div class="container">
                <%--Content Row--%>
            <div class="row">
                    <%--Sidebar Columb--%>
                <div class="col-lg-3" style="margin-top: 15px">
                    <h4>Добрый день, <c:out value="${nurseDTO.firstName} ${nurseDTO.lastName}."/></h4>
                    <div class="list-group">
                        <a href="${nurse}" class="list-group-item">Все события</a>
                        <a href="${onTodayTreatments}" class="list-group-item">События запланированные на сегодня</a>
                        <a href="${onNextHour}" class="list-group-item">События запланированные на ближайший час</a>
                        <a href="${findPatient}" class="list-group-item">Просмотр событий пациента</a>
                        <a href="${elPanel}" class="list-group-item">Электронное табло</a>
                        <a href="${canceled}" class="list-group-item">Отменённые события</a>
                        <a href="${done}" class="list-group-item">Выполненные события</a>
                    </div>
                </div>
                    <%--Content Column--%>
                <c:if test="${empty currentTreatments}">
                    <h4 style="margin-top: 150px">На сегодня нет запланированных событий</h4>
                </c:if>
                <c:if test="${not empty currentTreatments}">
                    <div class="col-lg-9" style="margin-top: 15px">
                        <div id="wrapper">
                            <h2>События на сегодня</h2>
                            <div class="scroll-arrow" style="width: 900px;height:400px;overflow-y:scroll">
                                <table id="keywords" cellspacing="0" cellpadding="0">
                                    <thead>
                                    <tr>
                                        <th id="idTreatment">№</th>
                                        <th>Имя</th>
                                        <th>Фамилия</th>
                                        <th>Дата</th>
                                        <th>Назначение</th>
                                        <th>Статус</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="treatment" items="${currentTreatments}">
                                        <tr>
                                            <td class="lalign">${treatment.idTreatment}</td>
                                            <td>${treatment.firstNamePatient}</td>
                                            <td>${treatment.lastNamePatient}</td>
                                            <td>${treatment.date}</td>
                                            <td>${treatment.treatmentName}</td>
                                            <td>${treatment.status}</td>
                                            <td>
                                                <form:form id="doneForm" method="post" action="${doneOrCanceled}"
                                                           modelAttribute="treatmentDTO">
                                                <input type="hidden" name="idTreatment"
                                                       value="${treatment.idTreatment}"/>
                                                <input type="hidden" name="date" value="${treatment.date}">
                                                <input type="hidden" name="page" value="today">
                                                <button name="done" value="done" class="btn btn-outline-success"
                                                        type="submit">
                                                    Выполнено
                                                </button>
                                            </td>
                                            <td>
                                                <input type="hidden" name="idTreatment"
                                                       value="${treatment.idTreatment}}"/>
                                                <a class="btn btn-outline-danger" href="#cancel">Отменено</a>
                                                <div class="cssmodal dialog" style="z-index: 1" id="cancel">
                                                    <a href="#/" class="veil"></a>
                                                    <figure>
                                                        <h2>Укажите причину отмены: <a href="#/" class="smallclose"
                                                                                       aria-label="Close Modal Box">×</a>
                                                        </h2>
                                                        <input class="text-input-dialog" type="text" name="cause">
                                                        <button name="cancel" value="1"
                                                                class="btn btn-outline-danger ml-3" type="submit">
                                                            Отменить
                                                        </button>
                                                    </figure>
                                                </div>
                                                </form:form>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            <%--Pagination--%>
                        <c:if test="${pages.size() > 1}">
                            <ul class="pagination justify-content-center mt-4">
                                <c:forEach var="numberOfPage" items="${pages}">
                                    <li class="page-item">
                                        <a class="page-link" style="z-index: 0"
                                           href="${onTodayTreatments}?page=${numberOfPage}">${numberOfPage}</a>
                                    </li>
                                </c:forEach>
                            </ul>
                        </c:if>
                    </div>
                </c:if>
            </div>
        </div>
    </jsp:body>
</page:template>
