<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="page" tagdir="/WEB-INF/tags" %>

<page:template>

    <jsp:attribute name="title">Admin</jsp:attribute>

    <jsp:body>

        <c:url value="/admin" var="admin"/>
        <c:url value="/admin/registration" var="create"/>

        <%--Page Content--%>
        <div class="container">
        <%--Page Heading/Breadcrumbs--%>
        <h1 class="mt-4 mb-3">Регистрация сотрудника</h1>

        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="${admin}">Домой</a>
            </li>
            <li class="breadcrumb-item active">Регистрация сотрудника</li>
        </ol>

        <%--Patient Form--%>
        <div class="row">
            <div class="col-lg-8 mb-4">
                <h3>Заполните информацию о новом сотруднике</h3>
                <form:form name="userRegistration" id="registrationForm" action="${create}"
                           method="post" modelAttribute="userDTO">
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Имя:</label>
                            <input type="text" class="form-control" name="firstName"
                                   required data-validation-required-message="Пожалуйста, укажите имя пациента">
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Фамилия:</label>
                            <input type="text" class="form-control" name="lastName"
                                   required
                                   data-validation-required-message="Пожалуйста, укажите фамилию пациента">
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label id="accordion">Профессия</label>
                            <div>
                                <p id="headingOne" class="mb-2">
                                    <spring:hasBindErrors name="user">
                                    <c:forEach var="error" items="${errors.fieldErrors}">
                                    <c:if test="${error.field.contains('specialization')}">
                                <p style="color: red">Укажите специализацию</p>
                                </c:if>
                                </c:forEach>
                                </spring:hasBindErrors>
                                <input type="radio" class="radio" name="role" value="DOCTOR"
                                       data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                       required data-validation-required-message="Выберите профессию">
                                Врач
                                </p>
                                <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <p>Укажите специализацию врача:</p>
                                    <input type="text" class="mb-3" name="specialization">
                                </div>
                            </div>
                            <p>
                                <input type="radio" class="radio" name="role" value="NURSE"
                                       required data-validation-required-message="Выберите профессию">
                                Мед.сестра
                            </p>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Логин</label>
                            <input type="text" class="form-control" name="login"
                                   required data-validation-required-message="Пожалуйста, укажите логин">
                            <spring:hasBindErrors name="user">
                                <c:forEach var="error" items="${errors.fieldErrors}">
                                    <c:if test="${error.field.contains('login')}">
                                        <p style="color: red">Логин уже существует</p>
                                    </c:if>
                                </c:forEach>
                            </spring:hasBindErrors>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Пароль:</label>
                            <input type="password" class="form-control" name="password"
                                   required
                                   data-validation-required-message="Пожалуйста, укажите пароль">
                            <spring:hasBindErrors name="user">
                                <c:forEach var="error" items="${errors.fieldErrors}">
                                    <c:if test="${error.field.contains('password')}">
                                        <p style="color: red">Пароли не совпадают</p>
                                    </c:if>
                                </c:forEach>
                            </spring:hasBindErrors>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <label>Подтвердите пароль:</label>
                            <input type="password" class="form-control" name="confirmPassword"
                                   required
                                   data-validation-required-message="Пожалуйста, подтвердите пароль">
                            <spring:hasBindErrors name="user">
                                <c:forEach var="error" items="${errors.fieldErrors}">
                                    <c:if test="${error.field.contains('password')}">
                                        <p style="color: red">Пароли не совпадают</p>
                                    </c:if>
                                </c:forEach>
                            </spring:hasBindErrors>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="registerUser">
                        Зарегистрировать
                    </button>
                </form:form>
            </div>
        </div>
    </jsp:body>
</page:template>