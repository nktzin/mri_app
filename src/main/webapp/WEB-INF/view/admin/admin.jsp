<!DOCTYPE html>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="page" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<page:template>

    <jsp:attribute name="title">Admin</jsp:attribute>

    <jsp:body>

        <c:url value="/admin" var="admin"/>
        <c:url value="/admin/registration" var="registerUser"/>
        <c:url value="/admin/employees" var="findEmployee"/>
        <c:url value="/admin/fireEmployee" var="fireEmployee"/>

        <%--Page Content--%>
        <div class="container">
                <%--Content Row--%>
            <div class="row">
                    <%--Sidebar Column--%>
                <div class="col-lg-3 mb-4" style="margin-top: 15px">
                    <div class="list-group">
                        <a href="${registerUser}" class="list-group-item">Добавить сотрудника</a>
                        <a href="${findEmployee}" class="list-group-item">Перевести сотрудника в статус уволившегося</a>
                        <%--<a href="" class="list-group-item">Изменить пароль для сотрудника</a>--%>
                    </div>
                </div>
                    <%--Content Column--%>
                <c:if test="${not empty doctorDTOList}">
                    <form:form name="fireDoctor" modelAttribute="employeyDTO" action="${fireEmployee}"
                               method="post">
                        <div class="row">
                            <div class="card ml-3" style="width: 425px; margin-top: 15px">
                                <h5 class="card-header m-0">Выберите врача из списка</h5>
                                <div class="card card-body">
                                    <div class="row-fluid">
                                        <input class="col-lg-10 mb-4 rounded" list="listOfDoctors" name="employeeInfo"
                                               value="${employeeDTO.employeeInfo}">
                                        <datalist id="listOfDoctors" class="selection-handle">
                                            <c:forEach var="doctor" items="${doctorDTOList}">
                                                <option value="${doctor.lastName} ${doctor.firstName}. Специализация: ${doctor.specialization}. Логин: ${doctor.login}">
                                                </option>
                                            </c:forEach>
                                        </datalist>
                                    </div>
                                    <div>
                                        <button class="btn btn-outline-success" type="submit">
                                            Изменить статус
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form:form>
                </c:if>
                <c:if test="${not empty nurseDTOList}">
                    <form:form name="fireNurse" modelAttribute="employeyDTO" action="${fireEmployee}"
                               method="post">
                        <div class="row">
                            <div class="card ml-5" style="width: 425px; margin-top: 15px">
                                <h5 class="card-header m-0">Выберите медсестру из списка</h5>
                                <div class="card card-body">
                                    <div class="row-fluid">
                                        <input class="col-lg-10 mb-4 rounded" list="listOfNurses" name="employeeInfo"
                                               value="${employeeDTO.employeeInfo}">
                                        <datalist id="listOfNurses" class="selection-handle">
                                            <c:forEach var="nurse" items="${nurseDTOList}">
                                                <option
                                                        value="${nurse.lastName} ${nurse.firstName}. Логин: ${nurse.login}">
                                                </option>
                                            </c:forEach>
                                        </datalist>
                                    </div>
                                    <div>
                                        <button class="btn btn-outline-success" type="submit">
                                            Изменить статус
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form:form>
                </c:if>
                <c:if test="${empty doctorDTOList}">
                    <c:if test="${empty nurseDTOList}">
                        <div class="col-lg-9" style="margin-top: 15px">
                            <div id="information">
                                <h4>Добрый день, <c:out value="${adminDTO.firstName} ${adminDTO.lastName}."/> </h4>
                            </div>
                        </div>
                    </c:if>
                </c:if>
            </div>
        </div>
    </jsp:body>
</page:template>