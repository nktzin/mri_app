<!DOCTYPE html>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Sign in</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <spring:url value="/resources/login/images/icons/favicon.ico" var="images"/>
    <link href="${images}" rel="icon"/>

    <spring:url value="/resources/login/vendor/bootstrap/css/bootstrap.min.css" var="bootstrap"/>
    <link href="${bootstrap}" rel="stylesheet"/>

    <spring:url value="/resources/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css" var="fontawesome"/>
    <link href="${fontawesome}" rel="stylesheet"/>

    <spring:url value="/resources/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css" var="iconFonts"/>
    <link href="${iconFonts}" rel="stylesheet"/>

    <spring:url value="/resources/login/vendor/animate/animate.css" var="animate"/>
    <link href="${animate}" rel="stylesheet"/>

    <spring:url value="/resources/login/vendor/css-hamburgers/hamburgers.min.css" var="hamburgers"/>
    <link href="${hamburgers}" rel="stylesheet"/>

    <spring:url value="/resources/login/vendor/animsition/css/animsition.min.css" var="animsition"/>
    <link href="${animsition}" rel="stylesheet"/>

    <spring:url value="/resources/login/vendor/select2/select2.min.css" var="select2"/>
    <link href="${select2}" rel="stylesheet"/>

    <spring:url value="/resources/login/vendor/daterangepicker/daterangepicker.css" var="daterangepicker"/>
    <link href="${daterangepicker}" rel="stylesheet"/>

    <spring:url value="/resources/login/css/util.css" var="util"/>
    <link href="${util}" rel="stylesheet"/>

    <spring:url value="/resources/login/css/main.css" var="main"/>
    <link href="${main}" rel="stylesheet"/>

    <%--Favicon--%>
    <spring:url value="/resources/favicon.ico" var="favicon"/>
    <link rel="shortcut icon" href="${favicon}" type="image/x-icon">
    <link rel="icon" href="${favicon}" type="image/x-icon">

</head>
<body>

<c:url value="/login" var="loginUser"/>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
            <form class="login100-form validate-form flex-sb flex-w" action="${loginUser}" method="post">
					<span class="login100-form-title p-b-32">
						Войдите в систему
					</span>
                <span class="txt1 p-b-11">
						Имя пользователя
					</span>
                <c:if test="${SPRING_SECURITY_LAST_EXCEPTION.message eq 'Bad credentials'}">
                    <span class="txt1 p-b-11" style="color: red">
                        Неверный логин или пароль
                    </span>
                </c:if>
                <div class="wrap-input100 validate-input m-b-36" data-validate="Введите имя пользователя">
                    <input class="input100 input" type="text" name="username">
                    <span class="focus-input100"></span>
                </div>

                <span class="txt1 p-b-11">
						Пароль
					</span>
                <c:if test="${SPRING_SECURITY_LAST_EXCEPTION.message eq 'Bad credentials'}">
                    <span class="txt1 p-b-11" style="color: red">
                        Неверный логин или пароль
                    </span>
                </c:if>
                <div class="wrap-input100 validate-input m-b-12" data-validate="Введите пароль">
						<span class="btn-show-pass">
							<em class="fa fa-eye"></em>
						</span>
                    <input class="input100" type="password" name="password">
                    <span class="focus-input100"></span>
                </div>

                <div class="flex-sb-m w-full p-b-48">
                    <div class="contact100-form-checkbox">
                        <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                        <label class="label-checkbox100" for="ckb1">
                            Запомни меня
                        </label>
                    </div>

                    <%--<div>
                        <a href="#" class="txt3">
                            Забыли пароль?
                        </a>
                    </div>--%>
                </div>

                <div class="container-login100-form-btn">
                    <button type="submit" class="login100-form-btn">
                        Войти
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="dropDownSelect1"></div>

<spring:url value="/resources/login/vendor/jquery/jquery-3.2.1.min.js" var="jquery"/>
<script src="${jquery}"></script>

<spring:url value="/resources/login/vendor/animsition/js/animsition.min.js" var="animisitionJS"/>
<script src="${animisitionJS}"></script>

<spring:url value="/resources/login/vendor/bootstrap/js/popper.js" var="popper"/>
<script src="${popper}"></script>

<spring:url value="/resources/login/vendor/bootstrap/js/bootstrap.min.js" var="bootstrapJS"/>
<script src="${bootstrapJS}"></script>

<spring:url value="/resources/login/vendor/select2/select2.min.js" var="select2JS"/>
<script src="${select2JS}"></script>

<spring:url value="/resources/login/vendor/daterangepicker/moment.min.js" var="momentJS"/>
<script src="${momentJS}"></script>

<spring:url value="/resources/login/vendor/daterangepicker/daterangepicker.js" var="daterangepickerJS"/>
<script src="${daterangepickerJS}"></script>

<spring:url value="/resources/login/vendor/countdowntime/countdowntime.js" var="countdowntime"/>
<script src="${countdowntime}"></script>

<spring:url value="/resources/login/js/main.js" var="mainJS"/>
<script src="${mainJS}"></script>

</body>
</html>