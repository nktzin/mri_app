<%--
  Created by IntelliJ IDEA.
  User: nervous
  Date: 16.03.2019
  Time: 19:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="page" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<page:template>
    <jsp:attribute name="title">404</jsp:attribute>
    <jsp:body>

        <c:url value="/home" var="home"/>

        <%--Page Content--%>
        <div class="container">
            <%--Page Heading/Breadcrumbs--%>
            <h1 class="mt-4 mb-3">404
            <small>Страница не найдена</small>
            </h1>

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="${home}">Домой</a>
                </li>
                <li class="breadcrumb-item active">404</li>
            </ol>

            <div class="jumbotron">
                <h1 class="display-1">404</h1>
                <p>Страница которую вы ищете не найдена.
                Несколько полезных ссылок, которые помогут вам вернуться к работе:</p>
                <ul>
                    <li>
                        <a href="${home}">Домой</a>
                    </li>
                    <li>
                        <a class="#about">Общее</a>
                    </li>
                    <li>
                        <a class="#services">Тех. помощь</a>
                    </li>
                    <li>
                        <a class="#contact">Контакты</a>
                    </li>
                </ul>
            </div>
        </div>
    </jsp:body>
</page:template>
