<!DOCTYPE html>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="page" tagdir="/WEB-INF/tags" %>

<page:template>

    <jsp:attribute name="title">Patient prescriptions</jsp:attribute>

    <jsp:body>

        <c:url value="/doctor.html" var="doctor"/>
        <c:url value="/doctor/patient" var="findPatientForEditPrescription"/>
        <c:url value="/doctor/patient/patientPrescriptions" var="toEditPrescription"/>
        <c:url value="/doctor/editOrCanceled" var="editOrCanceled"/>

        <%--Page Content--%>
        <div class="container">
                <%--Page Heading/Breadcrumbs--%>
            <h1 class="mt-4 mb-3">Список назначений</h1>

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="${doctor}">Домой</a>
                </li>
                <li class="breadcrumb-item active">Список назначений</li>
            </ol>

                <%--Content Row--%>
            <div class="row">
                <c:if test="${not empty patientPrescriptions}">
                    <div class="col-lg-12">
                        <div id="wrapper">
                            <h2>Выберите требуемую опцию</h2>
                            <div class="scroll-arrow">
                                <table id="keywords" cellspacing="0" cellpadding="0">
                                    <thead>
                                    <tr>
                                        <th id="idTreatment">№</th>
                                        <th>Имя</th>
                                        <th>Фамилия</th>
                                        <th>Назначение</th>
                                        <th>Дата начала</th>
                                        <th>Дата окончания</th>
                                        <th>Дневной период</th>
                                        <th>Недельный период</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="prescription" items="${patientPrescriptions}">
                                        <tr>
                                            <td class="lalign">${prescription.idPrescription}</td>
                                            <td>${prescription.patientFirsName}</td>
                                            <td>${prescription.patientLastName}</td>
                                            <td>${prescription.procedureName}</td>
                                            <td>${prescription.startTime.toString("dd.MM.yyyy")}</td>
                                            <td>${prescription.endTime.toString("dd.MM.yyyy")}</td>
                                            <td>
                                                <c:if test="${prescription.dayTimePattern == null}">
                                                    -
                                                </c:if>
                                                <c:if test="${prescription.dayTimePattern.name().equals('ONCE_A_DAY')}">
                                                    Один раз в день
                                                </c:if>
                                                <c:if test="${prescription.dayTimePattern.name().equals('TWICE_A_DAY')}">
                                                    Два раза в день
                                                </c:if>
                                                <c:if test="${prescription.dayTimePattern.name().equals('THREE_TIMES_A_DAY')}">
                                                    Три раза в день
                                                </c:if>
                                                <c:if test="${prescription.dayTimePattern.name().equals('FOUR_TIMES_A_DAY')}">
                                                    Четыре раза в день
                                                </c:if>
                                                <c:if test="${prescription.dayTimePattern.name().equals('FIVE_TIMES_A_DAY')}">
                                                    Пять раз в день
                                                </c:if>
                                            </td>
                                            <td>
                                                <c:if test="${prescription.weekTimePattern == null}">
                                                    -
                                                </c:if>
                                                <c:if test="${prescription.weekTimePattern.name().equals('ONCE_A_WEEK')}">
                                                    Один раз в неделю
                                                </c:if>
                                                <c:if test="${prescription.weekTimePattern.name().equals('TWICE_A_WEEK')}">
                                                    Два раза в неделю
                                                </c:if>
                                                <c:if test="${prescription.weekTimePattern.name().equals('THREE_TIMES_A_WEEK')}">
                                                    Три раза в неделю
                                                </c:if>
                                                <c:if test="${prescription.weekTimePattern.name().equals('FOUR_TIMES_A_WEEK')}">
                                                    Четыре раза в неделю
                                                </c:if>
                                                <c:if test="${prescription.weekTimePattern.name().equals('FIVE_TIMES_A_WEEK')}">
                                                    Пять раз в неделю
                                                </c:if>
                                                <c:if test="${prescription.weekTimePattern.name().equals('SIX_TIMES_A_WEEK')}">
                                                    Шесть раз в неделю
                                                </c:if>
                                                <c:if test="${prescription.weekTimePattern.name().equals('SEVEN_TIMES_A_WEEK')}">
                                                    Каждый день
                                                </c:if>
                                            </td>
                                            <td>
                                                <form:form id="doneForm" method="post" action="${editOrCanceled}"
                                                           modelAttribute="prescriptionDTO">
                                                <input type="hidden" name="idPrescription"
                                                       value="${prescription.idPrescription}"/>
                                                <button name="edit" value="edit" class="btn btn-outline-success"
                                                        type="submit">
                                                    Редактировать
                                                </button>
                                            </td>
                                            <td>
                                                <input type="hidden" name="dose" value="${prescription.dose}"/>
                                                <input type="hidden" name="idPrescription"
                                                       value="${prescription.idPrescription}"/>
                                                <button name="cancel" value="cancel" class="btn btn-outline-danger"
                                                        type="submit">Отменить
                                                </button>
                                                </form:form>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            <%--Pagination--%>
                        <ul class="pagination justify-content-center mt-4">
                            <c:if test="${pages.size() == 1}">
                            </c:if>
                            <c:if test="${pages.size() > 1}">
                                <c:forEach var="pageNumber" items="${pages}">
                                    <li class="page-item">
                                        <a class="page-link"
                                           href="${toEditPrescription}?page=${pageNumber}">${pageNumber}</a>
                                    </li>
                                </c:forEach>
                            </c:if>
                        </ul>
                    </div>
                </c:if>
            </div>
        </div>
        </div>
    </jsp:body>
</page:template>