<!DOCTYPE html>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>

    <%--Calendar--%>
    <spring:url value="/resources/css/daterangepicker.css" var="daterangepicker"/>
    <link href="${daterangepicker}" rel="stylesheet"/>

    <%--Bootstrap core CSS--%>
    <spring:url value="/resources/vendor/bootstrap/css/bootstrap.min.css" var="bootstrap"/>
    <link href="${bootstrap}" rel="stylesheet">

    <%--Custom styles for this template--%>
    <spring:url value="/resources/css/modern-business.css" var="startertemplate"/>
    <link href="${startertemplate}" rel="stylesheet">

    <%--Favicon--%>
    <spring:url value="/resources/favicon.ico" var="favicon"/>
    <link rel="shortcut icon" href="${favicon}" type="image/x-icon">
    <link rel="icon" href="${favicon}" type="image/x-icon">

    <title>Prescription</title>
</head>
<body>

<c:url value="/doctor.html" var="doctor"/>
<c:url value="/about.html" var="about"/>
<c:url value="/logout" var="logout"/>

<%--Navigaton--%>
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#index.html">Super-duper medical center</a>
        <button class="navbar-toggler navbar-toggler-right" type="button"
                data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav m1-auto">
                <li class="nav-item">
                    <a class="nav-link" href="${about}">Общее</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#Services">Тех. помощь</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#Contacts">Контакты</a>
                </li>
                <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Другие страницы
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                        <a class="dropdown-item" href="#">FAQ</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="btn btn-outline-warning" href="${logout} ">Выйти</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<%--Page Content--%>
<div class="container">
    <%--Page Heading/Breadcrumbs--%>
    <h1 class="mt-4 mb-3">Назначение</h1>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="${doctor}">Домой</a>
        </li>
        <li class="breadcrumb-item active">Назначение</li>
    </ol>

    <c:url value="/doctor/prescription/create" var="createPrescription"/>
    <c:url value="/doctor/findAllProcedures" var="findAllProcedures"/>
    <c:url value="/doctor/findAllMedicaments" var="findAllMedicaments"/>

    <%--Prescription form--%>
    <div class="row">
        <div class="col-lg-6 mb-4">
            <h3 class="mb-4">Пациент:
                <br/>
                <small><c:out
                        value="${patientDTO.firstName} ${patientDTO.lastName}, № страховки ${patientDTO.insurance}"/></small>
            </h3>
            <form:form name="prescriptionInfo" action="${createPrescription}"
                       method="post" modelAttribute="prescriptionDTO">
                <div class="control-group form-group">
                    <div class="controls">
                        <label class="mr-2">Выберите тип:</label>
                        <a class="btn btn-outline-secondary mr-2" href="${findAllMedicaments}">Лекарство</a>
                        <a class="btn btn-outline-secondary mr-5" href="${findAllProcedures}">Процедура</a>
                        <spring:hasBindErrors name="prescriptionDTO">
                            <c:forEach var="error" items="${errors.fieldErrors}">
                                <c:if test="${error.field.contains('procedureName')}">
                                    <div class="help-block">
                                        <p style="color: red">
                                            Пожалуйста, выберите процедуру или лекарство
                                        </p>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </spring:hasBindErrors>
                        <c:if test="${not empty resultAllMedicaments}">
                            <div class="card mt-4 scroll-bar">
                                <input class="col-lg-12 rounded" list="listOfMedicaments" name="medicamentName"
                                       value="${medicament.name}" required
                                       data-validation-required-message="Please, choose medicament">
                                <datalist id="listOfMedicaments" class="selection-handle">
                                    <c:forEach var="medicament" items="${resultAllMedicaments}">
                                        <option name="medicamentName" value="${medicament.name}"><c:out
                                                value="${medicament.name}"/></option>
                                    </c:forEach>
                                </datalist>
                            </div>
                        </c:if>
                        <c:if test="${not empty resultAllProcedures}">
                            <div class="card mt-4 scroll-bar">
                                <input class="rounded col-lg-12" list="listOfProcedures" name="procedureName"
                                       value="${procedure.name}" required
                                       data-validate="Please, choose procedure">
                                <datalist id="listOfProcedures" class="selection-handle">
                                    <c:forEach var="procedure" items="${resultAllProcedures}">
                                        <option name="procedureName" value="${procedure.name}"><c:out
                                                value="${procedure.name}"/></option>
                                    </c:forEach>
                                </datalist>
                            </div>
                        </c:if>
                        <c:if test="${not empty resultAllMedicaments}">
                            <label>Доза:</label>
                            <input class="col-lg-3 mt-3 rounded" name="dose" type="text" required>
                            <br/>
                        </c:if>
                        <label class="mt-2">Укажите частоту приёма:</label>
                        <div class="col card-columns">
                            <label style="width: 100px">в день:</label>
                        </div>
                        <div class="col card-columns">
                            <select name="toDayTimePatternConvert" class="simple-select rounded" size="1">
                                <option name="toDayTimePatternConvert" value="1">Один раз в день</option>
                                <option name="toDayTimePatternConvert" value="2">Два раза в день</option>
                                <option name="toDayTimePatternConvert" value="3">Три раза в день</option>
                                <option name="toDayTimePatternConvert" value="4">Четыре раза в день</option>
                                <option name="toDayTimePatternConvert" value="5">Пять раз в день</option>
                            </select>
                        </div>
                        <div class="col mt-1 card-columns">
                            <label style="width: 100px">в неделю:</label>
                        </div>
                        <div class="col card-columns">
                            <select name="toWeekTimePatternConvert" class="simple-select rounded" size="1">
                                <option name="toWeekTimePatternConvert" value="1">Один раз в неделю</option>
                                <option name="toWeekTimePatternConvert" value="2">Два раза в неделю</option>
                                <option name="toWeekTimePatternConvert" value="3">Три раза в неделю</option>
                                <option name="toWeekTimePatternConvert" value="4">Четыре раза в неделю</option>
                                <option name="toWeekTimePatternConvert" value="5">Пять раз в неделю</option>
                                <option name="toWeekTimePatternConvert" value="6">Шесть раз в неделю</option>
                                <option name="toWeekTimePatternConvert" value="7">Каждый день</option>
                            </select>
                        </div>
                        <label>Период:</label>
                        <input class="col-lg-6 mt-4 mb-4 rounded" name="datefilter" type="text" required value=""/>
                        <spring:hasBindErrors name="prescriptionDTO">
                            <c:forEach var="error" items="${errors.fieldErrors}">
                                <c:if test="${error.field.contains('startTime')}">
                                    <div class="help-block">
                                        <p style="color: red">
                                            Введите начальную дату c сегодняшнего дня
                                        </p>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </spring:hasBindErrors>
                    </div>
                </div>
                <button type="submit" class="btn btn-dark" name="sendPrescriptionForm">
                    Создать назначение
                </button>
            </form:form>
        </div>
        <div class="col-lg-6">
            <div id="information">
                <c:if test="${not empty patientDTO.prescriptionDTOS}">
                    На данный момент пациенту назначено:
                    <ul>
                        <c:forEach var="prescriptionName" items="${patientDTO.prescriptionDTOS}">
                            <li><c:out
                                    value="${prescriptionName.procedureName}. Период: ${prescriptionName.startTime.toString('dd.MM.yyyy')} - ${prescriptionName.endTime.toString('dd.MM.yyyy')}"/></li>
                        </c:forEach>
                    </ul>
                </c:if>
                <h5>Будьте внимательны!</h5>
                <p class="mb-2">Все поля должны быть заполены!</p>
                <p class="mb-2">Укажите начало лечения c ${date}.</p>
                <p>Проверьте правильность введённых данных перед отправкой формы.</p>
            </div>
        </div>
    </div>

    <%--Bootstrap core JavaScript--%>
    <spring:url value="/resources/vendor/jquery/jquery.min.js" var="jquery"/>
    <script src="${jquery}"></script>

    <spring:url value="/resources/vendor/bootstrap/js/bootstrap.bundle.min.js" var="bootstrapJS"/>
    <script src="${bootstrapJS}"></script>

    <spring:url value="/resources/js/jqBootstrapValidation.js" var="jsBootstrapValidation"/>
    <script src="${jsBootstrapValidation}"></script>

    <spring:url value="/resources/js/contact_me.js" var="contact_me"/>
    <script src="${contact_me}"></script>

    <%--Calendar--%>

    <script type="text/javascript">
        $(function () {

            $('input[name="datefilter"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('input[name="datefilter"]').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('DD.MM.YYYY') + ' - ' + picker.endDate.format('DD.MM.YYYY'));
            });

            $('input[name="datefilter"]').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

        });
    </script>

    <spring:url value="/resources/calendar/jquery.min.js" var="calendarJQuery"/>
    <script type="text/javascript" src="${calendarJQuery}"></script>
    <spring:url value="/resources/calendar/moment.min.js" var="calendarMoment"/>
    <script type="text/javascript" src="${calendarMoment}"></script>
    <spring:url value="/resources/calendar/daterangepicker.min.js" var="calendarDateRangePicker"/>
    <script type="text/javascript" src="${calendarDateRangePicker}"></script>

</body>
</html>