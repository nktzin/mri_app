<jsp:useBean id="now" class="java.util.Date" scope="page"/>
<!DOCTYPE html>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="page" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<page:template>

    <jsp:attribute name="title">Doctor</jsp:attribute>

    <jsp:body>

        <c:url value="/doctor.html" var="doctor"/>
        <c:url value="/doctor/registerPatient" var="registerPatient"/>
        <c:url value="/doctor/findPatient" var="findPatient"/>
        <c:url value="/doctor/patientDischarge.html" var="patientDischarge"/>
        <c:url value="/doctor/patient" var="findPatientForEditPrescription"/>
        <c:url value="/doctor/patient/patientPrescriptions" var="toPatientPrescriptions"/>
        <c:url value="/doctor/treatments" var="allTreatments"/>
        <c:url value="/doctor/createPDF" var="createPDF"/>
        <c:url value="http://localhost:8080/elPanel/index.jsf?employee=doctor" var="elPanel"/>
        <c:url value="/doctorError" var="doctorError"/>

        <%--Page Content--%>
        <div class="container">
                <%--Content Row--%>
            <div class="row mt-4 mt-4">
                    <%--Sidebar Column--%>
                <div class="col-lg-3 mb-4">
                    <div class="list-group">
                        <a href="${registerPatient}" class="list-group-item">Зарегистрировать пациента</a>
                        <a href="${patientDischarge}" class="list-group-item">Выписать пациента</a>
                        <a href="${findPatient}" class="list-group-item">Назначение процедуры или лекарства</a>
                        <a href="${findPatientForEditPrescription}" class="list-group-item">Редактирование
                            назначений</a>
                        <a href="${allTreatments}" class="list-group-item">Список событий</a>
                        <a href="${createPDF}" class="list-group-item">Создать PDF</a>
                        <a href="${elPanel}" class="list-group-item">Электронное табло</a>
                    </div>
                </div>
                    <%--Content Column--%>
                <c:if test="${not empty patientList}">
                    <form:form name="findPatientForTreatment" modelAttribute="patientDTO"
                               action="${toPatientPrescriptions}"
                               method="post">
                        <div class="row">
                            <div class="card ml-3" style="width: 500px">
                                <h5 class="card-header m-0">Выберите пациента из списка</h5>
                                <div class="card card-body">
                                    <div class="row-fluid">
                                        <input class="col-lg-12 mb-4 rounded" list="listOfPatients" name="insurance"
                                               value="${patient.insurance}">
                                        <datalist id="listOfPatients" class="selection-handle">
                                            <c:forEach var="patient" items="${patientList}">
                                                <option value="${patient.lastName} ${patient.firstName}. № Страховки: ${patient.insurance}">
                                                </option>
                                            </c:forEach>
                                        </datalist>
                                    </div>
                                    <div class="col-lg-12">
                                        <button class="col-lg-5 btn btn-outline-success" type="submit">Найти
                                            назначения
                                        </button>
                                        <a href="${doctor}" class="btn btn-outline-danger">Назад</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form:form>
                    <div class="col-lg-3 ml-3">
                        <div id="patientListInfo">
                            <h5>Проверьте правильность введённых данных:</h5>
                            <ul>
                                <li><h5><small>Имя</small></h5></li>
                                <li><h5><small>Фамилия</small></h5></li>
                                <li><h5><small>№ Страховки</small></h5></li>
                            </ul>
                        </div>
                    </div>
                </c:if>
                <c:if test="${not empty treatmentList}">
                    <div class="col-lg-9">
                        <div id="wrapper">
                            <h2>Список пациентов</h2>
                            <div class="scroll-arrow" style="height:300px;overflow-y:scroll">
                                <table id="keywords" cellspacing="0" cellpadding="0">
                                    <thead>
                                    <tr>
                                        <th id="idTreatment">№</th>
                                        <th>Имя</th>
                                        <th>Фамилия</th>
                                        <th>Дата</th>
                                        <th>Назначение</th>
                                        <th>Статус</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="treatment" items="${treatmentList}">
                                        <tr>
                                            <td class="lalign">${treatment.idTreatment}</td>
                                            <td>${treatment.firstNamePatient}</td>
                                            <td>${treatment.lastNamePatient}</td>
                                            <td>${treatment.date}</td>
                                            <td>${treatment.treatmentName}</td>
                                            <td>${treatment.status}</td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                            <%--Pagination--%>
                        <ul class="pagination justify-content-center mt-4">
                            <c:forEach var="numberOfPage" items="${pages}">
                                <c:if test="${pages.size() > 6}">

                                </c:if>
                                <li class="page-item">
                                    <a class="page-link"
                                       href="${allTreatments}?page=${numberOfPage}">${numberOfPage}</a>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </c:if>
                <c:if test="${empty treatmentList}">
                    <c:if test="${empty patientList}">
                        <div class="col-lg-9">
                            <div id="information">
                                <h4>Добрый день, <c:out value="${doctorDTO.firstName} ${doctorDTO.lastName}."
                                                        default=""/></h4>
                                <h5>Сегодня <fmt:formatDate type="time" value="${now}" pattern="dd MMM yyyy"/>
                                    года.</h5>
                                <p style="color: green">${successMsg}</p>
                                <c:if test="${not empty numberOfPatients}">
                                    <p>
                                        Количество пациентов, проходящих лечение: <c:out value="${numberOfPatients}"/>
                                    </p>
                                    <p>Краткая информация:</p>
                                    <ul>
                                        <c:forEach var="patient" items="${infoAboutPatients}">
                                            <li>
                                                <c:out value="${patient.firstName} ${patient.lastName}. Диагноз: ${patient.diagnosis}."/>
                                                <ul>
                                                    <c:forEach var="prescriptions" items="${patient.prescriptionDTOS}">
                                                        <li><c:out value="${prescriptions.procedureName}"/></li>
                                                    </c:forEach>
                                                </ul>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </c:if>
                            </div>
                        </div>
                    </c:if>
                </c:if>
            </div>
        </div>
    </jsp:body>
</page:template>