<!DOCTYPE html>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>

    <%--Calendar--%>
    <spring:url value="/resources/css/daterangepicker.css" var="daterangepicker"/>
    <link href="${daterangepicker}" rel="stylesheet"/>

    <%--Bootstrap core CSS--%>
    <spring:url value="/resources/vendor/bootstrap/css/bootstrap.min.css" var="bootstrap"/>
    <link href="${bootstrap}" rel="stylesheet">

    <%--Custom styles for this template--%>
    <spring:url value="/resources/css/modern-business.css" var="startertemplate"/>
    <link href="${startertemplate}" rel="stylesheet">

    <%--Favicon--%>
    <spring:url value="/resources/favicon.ico" var="favicon"/>
    <link rel="shortcut icon" href="${favicon}" type="image/x-icon">
    <link rel="icon" href="${favicon}" type="image/x-icon">

    <title>Prescription</title>
</head>
<body>

<c:url value="/doctor.html" var="doctor"/>
<c:url value="/doctor/patient/patientPrescriptions" var="patientPrescriptions"/>
<c:url value="/doctor/editPrescription" var="editPrescription"/>
<c:url value="/logout" var="logout"/>

<%--Navigaton--%>
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#index.html">Super-duper medical center</a>
        <button class="navbar-toggler navbar-toggler-right" type="button"
                data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav m1-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#about">Общее</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#Services">Тех. помощь</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#Contacts">Контакты</a>
                </li>
                <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Другие страницы
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                        <a class="dropdown-item" href="#">FAQ</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="btn btn-outline-warning" href="${logout} ">Выйти</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<%--Page Content--%>
<div class="container">
    <%--Page Heading/Breadcrumbs--%>
    <h1 class="mt-4 mb-3">Назначение</h1>

    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="${doctor}">Домой</a>
        </li>
        <li class="breadcrumb-item">
            <a href="${patientPrescriptions}">Список назначений</a></li>
        <li class="breadcrumb-item active">Редактирование назначение</li>
    </ol>

    <%--Prescription form--%>
    <div class="row">
        <div class="col-lg-6 mb-4">
            <h3 class="mb-2">Назначение:
                <br/>
                <small><c:out
                        value="${patientDTO.firstName} ${patientDTO.lastName}, № страховки ${patientDTO.insurance}"/></small>
            </h3>
            <form:form name="prescriptionInfo" action="${editPrescription}"
                       method="post" modelAttribute="prescriptionDTO">
                <input type="hidden" value="${patientDTO.insurance}" name="patientInsurance">
                <div class="control-group form-group">
                    <div class="controls">
                        <input type="hidden" name="idPrescription" value="${prescriptionDTO.idPrescription}"/>
                        <c:if test="${not empty prescriptionDTO.dose}">
                            <label>Доза:</label>
                            <input class="col-lg-3 mt-3 rounded" name="dose" type="text">
                            <br/>
                        </c:if>
                        <label class="mt-2">Укажите частоту приёма:</label>
                        <div class="col card-columns">
                            <label style="width: 100px">в день:</label>
                        </div>
                        <div class="col card-columns">
                            <select name="toDayTimePatternConvert" class="simple-select rounded" size="1">
                                <option name="toDayTimePatternConvert" value="1">Один раз в день</option>
                                <option name="toDayTimePatternConvert" value="2">Два раза в день</option>
                                <option name="toDayTimePatternConvert" value="3">Три раза в день</option>
                                <option name="toDayTimePatternConvert" value="4">Четыре раза в день</option>
                                <option name="toDayTimePatternConvert" value="5">Пять раз в день</option>
                            </select>
                        </div>
                        <div class="col mt-1 card-columns">
                            <label style="width: 100px">в неделю:</label>
                        </div>
                        <div class="col card-columns">
                            <select name="toWeekTimePatternConvert" class="simple-select rounded" size="1">
                                <option name="toWeekTimePatternConvert" value="1">Один раз в неделю</option>
                                <option name="toWeekTimePatternConvert" value="2">Два раза в неделю</option>
                                <option name="toWeekTimePatternConvert" value="3">Три раза в неделю</option>
                                <option name="toWeekTimePatternConvert" value="4">Четыре раза в неделю</option>
                                <option name="toWeekTimePatternConvert" value="5">Пять раз в неделю</option>
                                <option name="toWeekTimePatternConvert" value="6">Шесть раз в неделю</option>
                                <option name="toWeekTimePatternConvert" value="7">Каждый день</option>
                            </select>
                        </div>
                        <label>Период:</label>
                        <input class="col-lg-6 mt-4 mb-4 rounded" name="datefilter" type="text" value="" required/>
                        <spring:hasBindErrors name="prescriptionDTO">
                            <c:forEach var="error" items="${errors.fieldErrors}">
                                <c:if test="${error.field.contains('startTime')}">
                                    <div class="help-block">
                                        <p style="color: red">
                                            Начальная дата должна совпадать с начала назначения или начинаться с
                                            сегодняшнего дня
                                        </p>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </spring:hasBindErrors>
                    </div>
                </div>
                <button type="submit" class="btn btn-dark" name="sendPrescriptionForm">
                    Изменить назначение
                </button>
            </form:form>
        </div>
        <div class="col-lg-6">
            <div id="information">
                <h4>Информация о назначении</h4>
                <ul>
                    <li>Назначение: ${prescriptionDTO.procedureName}</li>
                    <li>Доза: ${prescriptionDTO.dose}</li>
                    <li>Дата начала: ${prescriptionDTO.startTime.toString("dd.MM.yyyy")}</li>
                    <li>Дата окончания: ${prescriptionDTO.endTime.toString("dd.MM.yyyy")}</li>
                    <li>Частота приема в день: <c:if test="${prescriptionDTO.dayTimePattern == null}">
                        -
                    </c:if>
                        <c:if test="${prescriptionDTO.dayTimePattern.name().equals('ONCE_A_DAY')}">
                            Один раз в день
                        </c:if>
                        <c:if test="${prescriptionDTO.dayTimePattern.name().equals('TWICE_A_DAY')}">
                            Два раза в день
                        </c:if>
                        <c:if test="${prescriptionDTO.dayTimePattern.name().equals('THREE_TIMES_A_DAY')}">
                            Три раза в день
                        </c:if>
                        <c:if test="${prescriptionDTO.dayTimePattern.name().equals('FOUR_TIMES_A_DAY')}">
                            Четыре раза в день
                        </c:if>
                        <c:if test="${prescriptionDTO.dayTimePattern.name().equals('FIVE_TIMES_A_DAY')}">
                            Пять раз в день
                        </c:if></li>
                    <li>Частота приема в неделю: <c:if test="${prescriptionDTO.weekTimePattern == null}">
                        -
                    </c:if>
                        <c:if test="${prescriptionDTO.weekTimePattern.name().equals('ONCE_A_WEEK')}">
                            Один раз в неделю
                        </c:if>
                        <c:if test="${prescriptionDTO.weekTimePattern.name().equals('TWICE_A_WEEK')}">
                            Два раза в неделю
                        </c:if>
                        <c:if test="${prescriptionDTO.weekTimePattern.name().equals('THREE_TIMES_A_WEEK')}">
                            Три раза в неделю
                        </c:if>
                        <c:if test="${prescriptionDTO.weekTimePattern.name().equals('FOUR_TIMES_A_WEEK')}">
                            Четыре раза в неделю
                        </c:if>
                        <c:if test="${prescriptionDTO.weekTimePattern.name().equals('FIVE_TIMES_A_WEEK')}">
                            Пять раз в неделю
                        </c:if>
                        <c:if test="${prescriptionDTO.weekTimePattern.name().equals('SIX_TIMES_A_WEEK')}">
                            Шесть раз в неделю
                        </c:if>
                        <c:if test="${prescriptionDTO.weekTimePattern.name().equals('SEVEN_TIMES_A_WEEK')}">
                            Каждый день
                        </c:if></li>
                </ul>
                <h5>Будьте внимательны!</h5>
                <p class="mb-2">Все поля должны быть заполены!</p>
                <p class="mb-2">Если лечение уже началось, укажите дату начала назначения.
                    В ином случае, укажите начало лечения c ${date}.</p>
                <p>Проверьте правильность введённых данных перед отправкой формы.</p>
            </div>
        </div>
    </div>
</div>

<%--Bootstrap core JavaScript--%>
<spring:url value="/resources/vendor/jquery/jquery.min.js" var="jquery"/>
<script src="${jquery}"></script>

<spring:url value="/resources/vendor/bootstrap/js/bootstrap.bundle.min.js" var="bootstrapJS"/>
<script src="${bootstrapJS}"></script>

<spring:url value="/resources/js/jqBootstrapValidation.js" var="jsBootstrapValidation"/>
<script src="${jsBootstrapValidation}"></script>

<spring:url value="/resources/js/contact_me.js" var="contact_me"/>
<script src="${contact_me}"></script>

<%--Calendar--%>

<script type="text/javascript">
    $(function () {

        $('input[name="datefilter"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="datefilter"]').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD.MM.YYYY') + ' - ' + picker.endDate.format('DD.MM.YYYY'));
        });

        $('input[name="datefilter"]').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

    });
</script>

<spring:url value="/resources/calendar/jquery.min.js" var="calendarJQuery"/>
<script type="text/javascript" src="${calendarJQuery}"></script>

<spring:url value="/resources/calendar/moment.min.js" var="calendarMoment"/>
<script type="text/javascript" src="${calendarMoment}"></script>

<spring:url value="/resources/calendar/daterangepicker.min.js" var="calendarDateRangePicker"/>
<script type="text/javascript" src="${calendarDateRangePicker}"></script>
</body>
</html>