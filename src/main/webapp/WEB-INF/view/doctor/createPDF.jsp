<!DOCTYPE html>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="page" tagdir="/WEB-INF/tags" %>

<page:template>
    <jsp:attribute name="title">Doctor</jsp:attribute>
    <jsp:body>

        <c:url value="/doctor.html" var="doctor"/>
        <c:url value="/doctor/pdf" var="createPDF"/>

        <%--Page Content--%>
        <div class="container">
                <%--Page Heading/Breadcrumbs--%>
            <h1 class="mt-4 mb-3">Поиск</h1>

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="${doctor}">Домой</a>
                </li>
                <li class="breadcrumb-item active">Поиск</li>
            </ol>


            <form:form name="findPatientForPrescription" modelAttribute="patientDTO" action="${createPDF}"
                       method="post">
                <div class="row">
                    <div class="card ml-3" style="width: 500px">
                        <h5 class="card-header m-0">Выберите пациента из списка</h5>
                        <div class="card card-body">
                            <div class="row-fluid">
                                <input class="col-lg-12 mb-4 rounded" list="listOfPatients" name="insurance"
                                       value="${patient.insurance}">
                                <datalist id="listOfPatients" class="selection-handle">
                                    <c:forEach var="patient" items="${patientList}">
                                        <option value="${patient.lastName} ${patient.firstName}. № Страховки: ${patient.insurance}">
                                        </option>
                                    </c:forEach>
                                </datalist>
                            </div>
                            <div class="col-lg-12">
                                <button class="col-lg-5 btn btn-outline-success ml-4" type="submit">Далее</button>
                                <a class="col-lg-5 btn btn-outline-warning ml-8" href="${doctor}">Назад</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div id="information">
                            <h4>Проверьте правильность введённых данных:</h4>
                            <ul>
                                <li><h4><small>Имя</small></h4></li>
                                <li><h4><small>Фамилия</small></h4></li>
                                <li><h4><small>№ Страховки</small></h4></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </jsp:body>
</page:template>