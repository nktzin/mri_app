<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="page" tagdir="/WEB-INF/tags" %>

<page:template>
    <jsp:attribute name="title">Doctor</jsp:attribute>

    <jsp:body>

        <c:url value="/doctor" var="doctor"/>
        <c:url value="/doctor/prescription" var="getPrescription"/>
        <c:url value="/doctor/prescription/afterDischarge" var="getPrescriptionAfterDischarge"/>
        <c:url value="/doctor/registerPatient" var="create"/>

        <%--Page Content--%>
        <div class="container">
                <%--Page Heading/Breadcrumbs--%>
            <h1 class="mt-4 mb-3">Карточка пациента</h1>

            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="${doctor}">Домой</a>
                </li>
                <li class="breadcrumb-item active">Зарегистрировать пациента</li>
            </ol>

                <%--Patient Form--%>
            <div class="row">
                <div class="col-lg-8 mb-4">
                    <h3>Заполните информацию о пациенте</h3>
                        <p style="color: green">${successMsg}</p>
                    <form:form name="patientInfo" id="InfoForm" action="${create}"
                               method="post" modelAttribute="patientDTO">
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Имя:</label>
                                <input type="text" class="form-control" name="firstName"
                                       required data-validation-required-message="Пожалуйста, укажите имя пациента">
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Фамилия:</label>
                                <input type="text" class="form-control" name="lastName"
                                       required
                                       data-validation-required-message="Пожалуйста, укажите фамилию пациента">
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Страховка:</label>
                                <input type="text" class="form-control" name="insurance"
                                       required
                                       data-validation-required-message="Пожалуйста, укажите данные страховки"
                                       minlength="16" maxlength="16">
                                <spring:hasBindErrors name="patientDTO">
                                    <c:forEach var="error" items="${errors.fieldErrors}">
                                        <c:if test="${error.field.contains('insurance')}">
                                            <div class="help-block">
                                                <p style="color: red">
                                                    Пользователь с данным номером страховки зарегестрирован
                                                </p>
                                            </div>
                                        </c:if>
                                        <c:if test="${error.field.contains('statusOfPatient')}">
                                            <div class="help-block">
                                                <p style="color: red">
                                                    Пользователь с данным номером страховки выписан
                                                </p>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </spring:hasBindErrors>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Диагноз:</label>
                                <textarea rows="5" cols="100" class="form-control" name="diagnosis"
                                          required data-validation-required-message="Пожалуйста, укажите диагноз"
                                          maxlength="999" style="resize:none" aria-invalid="false"></textarea>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div id="success"></div>
                        <%--For success/fail--%>
                        <button type="submit" class="btn btn-primary" id="registerPatient">
                            Зарегистрировать
                        </button>
                    </form:form>

                </div>
                <div class="col-lg-4 mb-2">
                    <spring:hasBindErrors name="patientDTO">
                        <c:forEach var="error" items="${errors.fieldErrors}">
                            <c:if test="${error.field.contains('statusOfPatient')}">
                                <h3>Перевести пациента в статус лечения и создать назначение?</h3>
                                <a class="btn btn-outline-success" style="width: 75px"
                                   href="${getPrescriptionAfterDischarge}">Да </a>
                                <a class="btn btn-outline-warning" style="width: 75px" href="${doctor}">Нет</a>
                            </c:if>
                        </c:forEach>
                    </spring:hasBindErrors>
                    <c:if test="${patientDTO.firstName != null and patientDTO.hasErrors == 0}">
                        <h3>Хотите создать назначение?</h3>
                        <a class="btn btn-outline-success" style="width: 75px" href="${getPrescription}">Да </a>
                        <a class="btn btn-outline-warning" style="width: 75px" href="${doctor}">Нет</a>
                    </c:if>
                </div>
            </div>

        </div>
    </jsp:body>
</page:template>