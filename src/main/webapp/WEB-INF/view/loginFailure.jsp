<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<c:if test="${sessionScope.get(SPRING_SECURITY_LAST_EXCEPTION.toString()) eq 'Bad credentials'}">
    <h1>Username or password incorrect</h1>
</c:if>
<p>Такого пользователя не существует</p>

</body>
</html>
