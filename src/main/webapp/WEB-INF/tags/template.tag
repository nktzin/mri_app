<!DOCTYPE html>

<%@tag description="Template Site tag" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<%@attribute name="title" fragment="true" %>

<html>
<head>
    <title>
        <jsp:invoke fragment="title"/>
    </title>

    <%--Favicon--%>
    <spring:url value="/resources/favicon.ico" var="favicon"/>
    <link rel="shortcut icon" href="${favicon}" type="image/x-icon">
    <link rel="icon" href="${favicon}" type="image/x-icon">

    <%--Calendar--%>
    <spring:url value="/resources/css/daterangepicker.css" var="daterangepicker"/>
    <link href="${daterangepicker}" rel="stylesheet"/>

    <%--Bootstrap core CSS--%>
    <spring:url value="/resources/vendor/bootstrap/css/bootstrap.min.css" var="bootstrap"/>
    <link href="${bootstrap}" rel="stylesheet">

    <%--Custom styles for this template--%>
    <spring:url value="/resources/css/modern-business.css" var="startertemplate"/>
    <link href="${startertemplate}" rel="stylesheet">

    <%--Table styles--%>
    <spring:url value="/resources/css/table.css" var="table"/>
    <link href="${table}" rel="stylesheet">
</head>
<body>

<c:url value="/about" var="about"/>
<c:url value="/logout" var="logout"/>
<c:url value="/home" var="home"/>

<%--Navigaton--%>
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="${home}">Super-duper medical center</a>
        <button class="navbar-toggler navbar-toggler-right" type="button"
                data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav m1-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#about">Общее</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#Services">Тех. помощь</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#Contacts">Контакты</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-outline-warning" href="${logout}">Выйти</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<jsp:doBody/>

<%--Bootstrap core JavaScript--%>
<spring:url value="/resources/vendor/jquery/jquery.min.js" var="jquery"/>
<script src="${jquery}"></script>

<spring:url value="/resources/vendor/bootstrap/js/bootstrap.bundle.min.js" var="bootstrapJS"/>
<script src="${bootstrapJS}"></script>

<spring:url value="/resources/js/jqBootstrapValidation.js" var="jsBootstrapValidation"/>
<script src="${jsBootstrapValidation}"></script>
</body>
</html>