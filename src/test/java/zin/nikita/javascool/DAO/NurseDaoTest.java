package zin.nikita.javascool.DAO;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.Config.MvcConfig;
import zin.nikita.javaschool.DAO.AdminDAO;
import zin.nikita.javaschool.DAO.NurseDAO;
import zin.nikita.javaschool.Entity.Admin;
import zin.nikita.javaschool.Entity.Nurse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MvcConfig.class})
@WebAppConfiguration
@DirtiesContext
@Transactional
public class NurseDaoTest {

    @Autowired
    private NurseDAO nurseDAO;

    @Autowired
    private AdminDAO adminDAO;

    private Admin admin;

    @Before
    public void init() {
        admin = Admin.builder().idAdmin(1)
                .login("admin")
                .firstName("FirstName")
                .lastName("LastName").build();
        adminDAO.createAdmin(admin);
    }

    @Test
    public void createNurseTest() {
        Nurse nurse = createNurse("createNurse", "Create", "Nurse");

        nurseDAO.createNurse(nurse);

        assertNotNull(nurseDAO.findNurseByLogin(nurse.getLogin()));
        assertTrue(nurse.getIdNurse() != 0);
    }

    @Test
    public void findNurseByIdTest() {
        Nurse nurse = createNurse("findNurseById", "Find", "Nurse");

        nurseDAO.createNurse(nurse);

        assertNotNull(nurseDAO.findNurseById(nurse.getIdNurse()));
        assertNull(nurseDAO.findNurseById(999));
    }

    @Test
    public void findNurseByLoginTest() {
        Nurse nurse = createNurse("findNurseByLogin", "Find", "Nurse");

        nurseDAO.createNurse(nurse);

        assertNotNull(nurseDAO.findNurseByLogin(nurse.getLogin()));
        assertEquals(nurse.getLastName(), nurseDAO.findNurseByLogin(nurse.getLogin()).getLastName());
        assertNull(nurseDAO.findNurseByLogin("NotInDB"));
    }

    @Test
    public void findAllNursesTest() {
        Nurse nurse = createNurse("findAllNurses", "Find", "AllNurses");
        nurseDAO.createNurse(nurse);
        assertEquals(1,nurseDAO.findAllNurses().size());
        nurseDAO.findAllNurses().forEach(nurseFound -> {
            nurseFound.setEnabled(false);
            nurseDAO.updateNurse(nurseFound);
        });
        assertEquals(0, nurseDAO.findAllNurses().size());
    }

    @Test
    public void updateNurseTest() {
        Nurse nurse = createNurse("updateNurse", "Nurse", "Update");

        nurseDAO.createNurse(nurse);
        nurse.setFirstName("Marina");
        nurse.setLastName("Ivanova");
        nurseDAO.updateNurse(nurse);
        assertEquals("Marina", nurseDAO.findNurseByLogin(nurse.getLogin()).getFirstName());
        assertEquals("Ivanova", nurseDAO.findNurseByLogin(nurse.getLogin()).getLastName());
    }

    @Test
    public void deleteNurseByLoginTest() {
        Nurse nurse = createNurse("deleteNurse", "Delte", "Nurse");
        nurseDAO.createNurse(nurse);
        nurseDAO.deleteNurseByLogin(nurse.getLogin());
        assertNull(nurseDAO.findNurseByLogin(nurse.getLogin()));
    }

    private Nurse createNurse(String login, String firstName, String lastName) {
        return Nurse.builder()
                .login(login)
                .firstName(firstName)
                .lastName(lastName)
                .admin(admin)
                .enabled(true).build();
    }
}
