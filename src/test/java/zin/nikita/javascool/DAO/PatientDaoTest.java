package zin.nikita.javascool.DAO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.Config.MvcConfig;
import zin.nikita.javaschool.DAO.PatientDAO;
import zin.nikita.javaschool.Entity.Enums.StatusOfPatient;
import zin.nikita.javaschool.Entity.Patient;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MvcConfig.class})
@WebAppConfiguration
@DirtiesContext
@Transactional
public class PatientDaoTest {

    private static final String PATIENT_INSURANCE = "1234567891234567";

    @Autowired
    private PatientDAO patientDAO;

    @Test
    public void createPatientTest() {
        Patient patient = createPatient("create", "patient");

        patientDAO.createPatient(patient);

        assertNotNull(patientDAO.findPatientByInsurance(PATIENT_INSURANCE));
        assertTrue(patient.getIdPatient() != 0);
    }

    @Test
    public void findPatientBuInsuranceTest() {
        Patient patient = createPatient("find", "patientByInsurance");

        patientDAO.createPatient(patient);

        assertNotNull(patientDAO.findPatientByInsurance(PATIENT_INSURANCE));
        assertEquals("find", patientDAO.findPatientByInsurance(PATIENT_INSURANCE).getFirstName());
    }

    @Test
    public void findAllPatientsTest() {
        Patient patient = createPatient("find", "patientByInsurance");
        patientDAO.createPatient(patient);
        assertEquals(1,patientDAO.findAllPatients().size());
        patientDAO.findAllPatients().forEach(patientFound -> {
            patientFound.setStatusOfPatient(StatusOfPatient.DISCHARGED);
            patientDAO.updatePatient(patientFound);
        });
        assertEquals(0, patientDAO.findAllPatients().size());
    }

    private Patient createPatient(String firstName, String lastName) {
        return Patient.builder()
                .firstName(firstName)
                .lastName(lastName)
                .insurance(PATIENT_INSURANCE)
                .diagnosis("diagnosis")
                .statusOfPatient(StatusOfPatient.TREATMENT).build();
    }
}
