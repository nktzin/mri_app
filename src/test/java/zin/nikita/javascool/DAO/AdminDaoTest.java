package zin.nikita.javascool.DAO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.Config.MvcConfig;
import zin.nikita.javaschool.DAO.AdminDAO;
import zin.nikita.javaschool.Entity.Admin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MvcConfig.class})
@WebAppConfiguration
@DirtiesContext
@Transactional
public class AdminDaoTest {

    @Autowired
    private AdminDAO adminDAO;

    @Test
    public void createAdminTest() {

        Admin admin = createAdmin("admin", "Igor", "Ivanov");

        adminDAO.createAdmin(admin);

        assertNotNull(adminDAO.findAdminByLogin(admin.getLogin()));
        assertTrue(admin.getIdAdmin() != 0);
    }

    @Test
    public void findAdminByLoginTest() {
        Admin admin = createAdmin("adminTest", "Ivan", "Petrov");
        adminDAO.createAdmin(admin);

        Admin findedAdmin = adminDAO.findAdminByLogin(admin.getLogin());

        assertNotNull(findedAdmin);
        assertTrue(findedAdmin.getIdAdmin() != 0);
        assertEquals("adminTest", findedAdmin.getLogin());
    }

    @Test
    public void updateAdminTest() {
        Admin admin = createAdmin("updateAdmin", "Update", "Admin");
        adminDAO.createAdmin(admin);

        admin.setFirstName("Евгений");
        adminDAO.updateAdmin(admin);
        assertEquals("Евгений", adminDAO.findAdminByLogin("updateAdmin").getFirstName());
    }

    @Test
    public void deleteAdminByLoginTest() {
        Admin admin = createAdmin("deleteAdmin", "Delete", "Admin");
        adminDAO.createAdmin(admin);

        adminDAO.deleteAdminByLogin(admin.getLogin());

        assertNull(adminDAO.findAdminByLogin("deleteAdmin"));
    }

    private Admin createAdmin(String login, String firstName, String lastName) {
        return Admin.builder()
                .login(login)
                .firstName(firstName)
                .lastName(lastName)
                .enabled(true).build();
    }
}
