package zin.nikita.javascool.DAO;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.Config.MvcConfig;
import zin.nikita.javaschool.DAO.AdminDAO;
import zin.nikita.javaschool.DAO.DoctorDAO;
import zin.nikita.javaschool.Entity.Admin;
import zin.nikita.javaschool.Entity.Doctor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MvcConfig.class})
@WebAppConfiguration
@DirtiesContext
@Transactional
public class DoctorDaoTest {

    @Autowired
    private DoctorDAO doctorDAO;

    @Autowired
    private AdminDAO adminDAO;

    private Admin admin;

    @Before
    public void init() {
        admin = Admin.builder().idAdmin(1)
                .login("admin")
                .firstName("FirstName")
                .lastName("LastName").build();
        adminDAO.createAdmin(admin);
    }

    @Test
    public void createDoctorTest() {
        Doctor doctor = createDoctor("createDoctor", "Create", "Doctor");

        doctorDAO.createDoctor(doctor);

        assertNotNull(doctorDAO.findDoctorByLogin(doctor.getLogin()));
        assertTrue(doctor.getIdDoctor() != 0);
    }

    @Test
    public void findDoctorByIdTest() {
        Doctor doctor = createDoctor("findDoctor", "Find", "Doctor");

        doctorDAO.createDoctor(doctor);

        assertNotNull(doctorDAO.findDoctorById(doctor.getIdDoctor()));
        assertNull(doctorDAO.findDoctorById(999));
    }

    @Test
    public void findDoctorByLoginTest() {
        Doctor doctor = createDoctor("findByLogin", "Find", "ByLogin");

        doctorDAO.createDoctor(doctor);

        assertNotNull(doctorDAO.findDoctorByLogin(doctor.getLogin()));
        assertEquals(doctor.getLastName(), doctorDAO.findDoctorByLogin(doctor.getLogin()).getLastName());
        assertNull(doctorDAO.findDoctorByLogin("NotInDB"));
    }

    @Test
    public void findAllDoctorsTest() {
        Doctor doctor = createDoctor("findAllDoctors", "Find", "AllDoctors");
        doctorDAO.createDoctor(doctor);
        assertEquals(1, doctorDAO.findAllDoctors().size());
        doctorDAO.findAllDoctors().forEach(doctorFound -> {
            doctorFound.setEnabled(false);
            doctorDAO.updateDoctor(doctorFound);
        });
        assertEquals(0, doctorDAO.findAllDoctors().size());
    }

    @Test
    public void updateDoctorTest() {
        Doctor doctor = createDoctor("updateDoctor", "Update", "Doctor");

        doctorDAO.createDoctor(doctor);
        doctor.setFirstName("Ivan");
        doctor.setLastName("Ivanov");
        doctorDAO.updateDoctor(doctor);
        assertEquals("Ivan", doctorDAO.findDoctorByLogin(doctor.getLogin()).getFirstName());
        assertEquals("Ivanov", doctorDAO.findDoctorByLogin(doctor.getLogin()).getLastName());
    }

    @Test
    public void deleteDoctorByLoginTest() {
        Doctor doctor = createDoctor("deleteDoctor", "Delete", "Doctor");
        doctorDAO.createDoctor(doctor);
        doctorDAO.deleteDoctorByLogin(doctor.getLogin());
        assertNull(doctorDAO.findDoctorByLogin(doctor.getLogin()));
    }

    private Doctor createDoctor(String login, String firstName, String lastName) {
        return Doctor.builder()
                .login(login)
                .firstName(firstName)
                .lastName(lastName)
                .specialization("Specialization")
                .admin(admin)
                .enabled(true).build();
    }
}
