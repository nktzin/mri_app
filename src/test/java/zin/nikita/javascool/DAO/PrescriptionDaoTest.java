package zin.nikita.javascool.DAO;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.Config.MvcConfig;
import zin.nikita.javaschool.DAO.PrescriptionDAO;
import zin.nikita.javaschool.Entity.Prescription;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MvcConfig.class})
@WebAppConfiguration
@DirtiesContext
@Transactional
public class PrescriptionDaoTest {

    @Autowired
    private PrescriptionDAO prescriptionDAO;

    @Test
    public void createPrescriptionAndFindPrescriptionByIdTest() {
        Prescription prescription = Prescription.builder()
                .startTime(DateTime.now())
                .endTime(DateTime.now().plusDays(1))
                .prescriptionName("Анальгин").build();
        prescriptionDAO.createPrescription(prescription);

        assertEquals("Анальгин", prescriptionDAO.findPrescriptionById(prescription.getIdPrescription()).getPrescriptionName());
    }
}
