package zin.nikita.javascool.Service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.Config.MvcConfig;
import zin.nikita.javaschool.DTO.UserDTO;
import zin.nikita.javaschool.Entity.Enums.RoleEnum;
import zin.nikita.javaschool.Entity.User;
import zin.nikita.javaschool.Service.UserService;
import zin.nikita.javaschool.Validation.LoginExistsException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MvcConfig.class})
@WebAppConfiguration
@DirtiesContext
@Transactional
public class UserServiceTest {

    private UserDTO userDTO;

    @Autowired
    private UserService userService;

    @Before
    public void init() {
        userDTO = new UserDTO();
        userDTO.setLogin("testUser");
        userDTO.setPassword("12345");
        userDTO.setRole(RoleEnum.ADMIN);
    }

    @Test
    public void createUserTest() {
        try {
            UserDTO testUserDTO = userService.createUser(userDTO);
            assertNotNull(testUserDTO);
        } catch (LoginExistsException ex) {

        }
        try {
            UserDTO testUser = userService.createUser(userDTO);
        } catch (LoginExistsException ex) {
            assertNotNull(ex);
        }
        User user = userService.findUserByLogin(userDTO.getLogin());
        assertNotNull(user);
        assertNotEquals(user.getPassword(), userDTO.getPassword());
    }
}
