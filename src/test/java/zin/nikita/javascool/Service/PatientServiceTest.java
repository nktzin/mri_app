package zin.nikita.javascool.Service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.Config.MvcConfig;
import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.DTO.DoctorDTO;
import zin.nikita.javaschool.DTO.PatientDTO;
import zin.nikita.javaschool.Entity.Admin;
import zin.nikita.javaschool.Entity.Enums.StatusOfPatient;
import zin.nikita.javaschool.Entity.Patient;
import zin.nikita.javaschool.Service.AdminService;
import zin.nikita.javaschool.Service.DoctorService;
import zin.nikita.javaschool.Service.PatientService;
import zin.nikita.javaschool.Validation.InsuranceExistsException;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MvcConfig.class})
@WebAppConfiguration
@DirtiesContext
@Transactional
public class PatientServiceTest {

    private PatientDTO patientDTO;

    private DoctorDTO doctorDTO;

    @Autowired
    private PatientService patientService;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private AdminService adminService;

    @Before
    public void init() {
        Admin admin = createAdmin();
        patientDTO = createPatientDTO();
        doctorDTO = createDoctorDTO();

        AdminDTO adminDTO = new AdminDTO();
        adminDTO.setLogin("admin");
        adminService.createAdmin(admin);

        doctorService.createDoctor(doctorDTO, adminDTO);

    }

    @Test
    public void createPatientTest() {
        try {
            PatientDTO patient = patientService.createPatient(patientDTO, doctorDTO);
            assertNotNull(patient);
            assertEquals(patient.getInsurance(), patientDTO.getInsurance());
            assertTrue(patient.getIdPatient() != 0);
            assertEquals(patient.getStatusOfPatient(), StatusOfPatient.TREATMENT);
        } catch (InsuranceExistsException ex) {
            assertNull(ex);
        }
        try {
            PatientDTO patientTest = patientService.createPatient(patientDTO, doctorDTO);
            assertNull(patientTest);
        } catch (InsuranceExistsException ex) {
            assertNotNull(ex);
        }
        Patient patient = patientService.findPatientByInsurance(patientDTO.getInsurance());
        assertNotNull(patient);
        assertEquals("Иван", patient.getFirstName());
    }


    private Admin createAdmin() {
        Admin admin = new Admin();
        admin.setLogin("admin");
        admin.setFirstName("Иван");
        admin.setLastName("Петров");
        admin.setIdAdmin(1);
        admin.setEnabled(true);
        admin.setDoctors(new HashSet<>());
        return admin;
    }

    private PatientDTO createPatientDTO() {
        patientDTO = new PatientDTO();
        patientDTO.setFirstName("Иван");
        patientDTO.setLastName("Иванов");
        patientDTO.setInsurance("1234567891234567");
        patientDTO.setDiagnosis("Diagnoz");
        return patientDTO;
    }

    private DoctorDTO createDoctorDTO() {
        doctorDTO = new DoctorDTO();
        doctorDTO.setLogin("doctor");
        doctorDTO.setFirstName("Алексей");
        doctorDTO.setLastName("Иванов");
        doctorDTO.setSpecialization("Уролог");
        doctorDTO.setEnabled(true);
        return doctorDTO;
    }
}
