package zin.nikita.javascool.Service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.Config.MvcConfig;
import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.Entity.Admin;
import zin.nikita.javaschool.Service.AdminService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MvcConfig.class})
@WebAppConfiguration
@DirtiesContext
@Transactional
public class AdminServiceTest {

    @Autowired
    private AdminService adminService;

    private Admin admin;

    @Before
    public void init() {
        admin = Admin.builder()
                .lastName("admin")
                .firstName("Иван")
                .lastName("Петров")
                .enabled(true).build();
    }

    @Test
    public void createAdminTest() {

        adminService.createAdmin(admin);
        assertTrue(admin.getIdAdmin() != 0);
    }

    @Test
    public void findAdminDTOByLoginTest() {
        admin.setLogin("findAdminDTO");
        adminService.createAdmin(admin);
        AdminDTO adminDTO = adminService.findAdminDTOByLogin(admin.getLogin());
        assertNotNull(adminDTO);
        assertTrue(adminDTO.getIdAdmin() != 0);
        assertEquals("findAdminDTO", adminDTO.getLogin());
        assertEquals("Иван", adminDTO.getFirstName());
        assertEquals("Петров", adminDTO.getLastName());
    }
}
