package zin.nikita.javascool.Service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.Config.MvcConfig;
import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.DTO.NurseDTO;
import zin.nikita.javaschool.Entity.Admin;
import zin.nikita.javaschool.Entity.Nurse;
import zin.nikita.javaschool.Service.AdminService;
import zin.nikita.javaschool.Service.NurseService;

import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MvcConfig.class})
@WebAppConfiguration
@DirtiesContext
@Transactional
public class NurseServiceTest {

    private NurseDTO nurseDTO;

    private AdminDTO adminDTO;

    @Autowired
    private NurseService nurseService;

    @Autowired
    private AdminService adminService;

    @Before
    public void init() {
        Admin admin = new Admin();
        admin.setLogin("admin");
        admin.setFirstName("Иван");
        admin.setLastName("Петров");
        admin.setIdAdmin(1);
        admin.setEnabled(true);
        admin.setNurses(new HashSet<>());

        nurseDTO = new NurseDTO();
        nurseDTO.setLogin("nurse");
        nurseDTO.setFirstName("Марина");
        nurseDTO.setLastName("Петрова");
        nurseDTO.setEnabled(true);

        adminDTO = new AdminDTO();
        adminDTO.setLogin("admin");
        adminService.createAdmin(admin);
    }

    @Test
    public void CRUDTest() {
        nurseService.createNurse(nurseDTO, adminDTO);
        NurseDTO nurseTest = nurseService.findNurseByLogin(nurseDTO.getLogin());
        assertNotNull(nurseTest);
        assertTrue(nurseTest.getIdNurse() != 0);
        assertEquals(nurseDTO.getFirstName(), nurseTest.getFirstName());
        nurseService.deleteNurseByLogin(nurseTest.getLogin());
        /*nurseTest = nurseService.findNurseByLogin(nurseTest.getLogin());
        assertFalse(nurseTest.isEnabled());*/
        Nurse nurse = nurseService.findNurseById(nurseTest.getIdNurse());
        assertNotNull(nurse);
        nurse.setEnabled(true);
        nurseService.updateNurse(nurse);
        assertTrue(nurseService.findNurseByLogin(nurse.getLogin()).isEnabled());
    }

    @Test
    public void findAllNursesDTOTest() {
        List<NurseDTO> nurseDTOList = nurseService.findAllNursesDTO();
        int startSize = nurseDTOList.size();
        nurseDTO.setLogin("nurseDtoFindAll");
        nurseService.createNurse(nurseDTO, adminDTO);
        nurseDTO.setLogin("nurseDtoFindAll2");
        nurseService.createNurse(nurseDTO, adminDTO);
        nurseDTOList = nurseService.findAllNursesDTO();
        int endSize = startSize + 2;
        assertEquals(endSize, nurseDTOList.size());
        assertEquals("nurseDtoFindAll2", nurseDTOList.get(endSize-1).getLogin());
    }
}
