package zin.nikita.javascool.Service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import zin.nikita.javaschool.Config.MvcConfig;
import zin.nikita.javaschool.DTO.AdminDTO;
import zin.nikita.javaschool.DTO.DoctorDTO;
import zin.nikita.javaschool.Entity.Admin;
import zin.nikita.javaschool.Entity.Doctor;
import zin.nikita.javaschool.Service.AdminService;
import zin.nikita.javaschool.Service.DoctorService;

import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MvcConfig.class})
@WebAppConfiguration
@DirtiesContext
@Transactional
public class DoctorServiceTest {

    private DoctorDTO doctorDTO;

    private AdminDTO adminDTO;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private AdminService adminService;

    @Before
    public void init() {
        Admin admin = new Admin();
        admin.setLogin("admin");
        admin.setFirstName("Иван");
        admin.setLastName("Петров");
        admin.setIdAdmin(1);
        admin.setEnabled(true);
        admin.setDoctors(new HashSet<>());

        doctorDTO = new DoctorDTO();
        doctorDTO.setLogin("doctor");
        doctorDTO.setFirstName("Алексей");
        doctorDTO.setLastName("Иванов");
        doctorDTO.setSpecialization("Уролог");
        doctorDTO.setEnabled(true);

        adminDTO = new AdminDTO();
        adminDTO.setLogin("admin");
        adminService.createAdmin(admin);
    }

    @Test
    public void CRUDTest() {
        doctorService.createDoctor(doctorDTO, adminDTO);
        Doctor doctor = doctorService.findDoctorByLogin(doctorDTO.getLogin());
        assertNotNull(doctor);
        assertTrue(doctor.getIdDoctor() != 0);
        assertEquals(doctorDTO.getFirstName(), doctor.getFirstName());
        doctorService.deleteDoctorByLogin(doctor.getLogin());
        assertFalse(doctor.isEnabled());
        doctor.setEnabled(true);
        doctorService.updateDoctor(doctor);
        assertTrue(doctor.isEnabled());
    }

    @Test
    public void findDoctorDTOByLoginTest() {
        doctorDTO.setLogin("findDoctorDTO");
        doctorService.createDoctor(doctorDTO, adminDTO);
        DoctorDTO doctor = doctorService.findDoctorDTOByLogin(doctorDTO.getLogin());
        assertNotNull(doctor);
        assertTrue(doctor.getIdDoctor() != 0);
        assertEquals("findDoctorDTO", doctor.getLogin());

    }

    @Test
    public void findAllDoctorsDTOTest() {
        List<DoctorDTO> doctorDTOList = doctorService.findAllDoctorsDTO();
        int startSize = doctorDTOList.size();
        doctorDTO.setLogin("doctorDto1");
        doctorService.createDoctor(doctorDTO, adminDTO);
        doctorDTO.setLogin("doctorDto2");
        doctorService.createDoctor(doctorDTO, adminDTO);
        doctorDTOList = doctorService.findAllDoctorsDTO();
        int endSize = startSize + 2;
        assertEquals(endSize, doctorDTOList.size());
        assertEquals("doctorDto2", doctorDTOList.get(endSize-1).getLogin());
    }

}
