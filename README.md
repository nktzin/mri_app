# MRI Application
Electronic document management system for medical institutions.

**DISCLAIMER:** this is a test application created only for educational purposes

**ABOUT:**

This application simulates the work of electronic medical document management.

There are two modules in the application. This is the first module in which the main
work takes place. _Registration of patients, change of treatments, change of their status._
The second module is an electronic panel that shows all the treatments for the current day
(completed, canceled or scheduled). The first module is deployed on tomcat, the second on wildfly.

Interactions between modules happens by means pf ActiveMQ Artemis embedded in WildFly AS.
The data transfer format is JSON. Serialization occurs via the Google library GSON, 
deserialization using Google GSON and Retrofit.

###How to build a project?
To build the first module, run the following command: `mvn tomcat7:deploy`

To build the second module, run the following command: `mvn wildfly:deploy`
